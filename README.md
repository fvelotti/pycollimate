# pycollimate

__Authors__: F.M. Velotti, L. Stoel, Y. Dutheil, J.P. Prieto

Module to enhance MADX tracking: 
 - Add possibility to track charged particles inside material
 - Add possibility to simulate real septum elements
 - Simulations of crystals at 30 and 400 GeV (data from UA9, for crystal installed in SPS)
 - include subroutine (pycollimate_fast) written in cyton to speed things up

Once cloned: 
```shell
git clone https://gitlab.cern.ch/fvelotti/pycollimate.git
```
install it via:
```shell
python setup.py build_ext --inplace
python setup.py install
```

Some additional files are:

 - **madxColl** patched version of MADX. When an element RCOLLIMATOR (or
COLLIMATOR) is found, it automatically uses the pycollimate tracking
routing to track protons inside the collimator jaw
 - **track_inside_coll.py** script example on how to use pycollimate. To
   be included in the working folder.
    - It implements now a full crystal 2D pdf from measurements in H8 provided by UA9 collaboration
 - **colldbmadx_ti2.tfs** collimator database for TI2. Also to be
   included in the working directory. 
 - **track_inside_coll_server.py** to avoid to write files on disk and connect to MADX via server port (Y. Dutheil)

---
### Email:
francesco.maria.velotti@cern.ch