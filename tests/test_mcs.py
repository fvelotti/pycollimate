from __future__ import print_function
from __future__ import division

import numpy as np
import matplotlib.pyplot as plt
from pycollimate import pycollimate as pc

chi_0 = {'c': 19.32e-2, 'w': 0.35e-2}


def mcs(s, material):

    theta_0 = 13.6e-3 / (400.) / np.sqrt(chi_0[material]) * (1. + 0.038 * np.log(s / chi_0[material]))

    return theta_0 * np.sqrt(s)


def mcs_bren(s, material):

    theta_0 = 14.1e-3 / (400.) / np.sqrt(chi_0[material]) * (1. + 1/9. * np.log10(s / chi_0[material]))

    return theta_0 * np.sqrt(s)


s = np.linspace(1e-4, 1, 1000)

plt.plot(s, mcs(s, 'c') * 1e6, label='pycollimate')
plt.plot(s, mcs_bren(s, 'c') * 1e6, '--', label='bren')

plt.legend()


# ===========================
# Test different mcs routins
# ===========================

n_part = 10000

pu = pc.make_pencil_beam(n_part, 400., 10e-3, 0.0)
septum_test = pc.Septum("Graphite", 0, 0.6, 0e-3, 0e-3, 1, 0.0, 0e-3, 'ZS.21633', 'left', 0.0, 0.0)

cs = pc.get_cross_sections(pu[0], septum_test)

print(cs)

print('int_len = ', septum_test.a / (6.02214129e23 * septum_test.d * cs['inelastic'] * 1e-24))
int_len = septum_test.a / (6.02214129e23 * septum_test.d * cs['inelastic'] * 1e-24) * 1e-2

print('survival from cs_in = ', np.exp(-1 * (septum_test.length / int_len)))

s = 2 * pu[0].mass * pu[0].p0

pptot = (41 - 2.33 * np.log(s) + 0.315 * np.log(s)**2) * 1e-3

cs_tot = septum_test.cs_tot_ref + septum_test.freep * (pptot - septum_test.pptref)
print(0.232 / 0.337 * cs_tot)

pu_col = pu = pc.make_pencil_beam(n_part, 400., 10e-3, 0.0)

# Test septum

for pp in pu:

    pc.get_impact_pos(septum_test, pp)

    # plt.plot(get_list_from_pu(pu, 's'), get_list_from_pu(pu, 'x'), 'r.')


pc.do_scattering_routine_septum(septum_test, pu)

xp = np.array([pp.x_p for pp in pu if pp.flag == 'outside'])

print('rms: ', np.std(xp) * 1e6, ' urad |  rms_t: ', mcs(septum_test.length, 'c') * 1e6, ' urad')

# Test collimator
new_coll = pc.Collimator('Graphite', 0, 0.6, 'h', 0.0, 0.4, 0.0, 0.0, 0.0, 0.0, 'collimator', 0.0)

for pp in pu_col:

    pc.get_impact_point_angle(new_coll, pp)

pc.do_scattering_routine(new_coll, pu_col)

xp_coll = np.array([pp.x_p for pp in pu_col if pp.flag == 'outside'])


plt.figure()
plt.hist(xp * 1e3, histtype='step', lw=3)
plt.hist(xp_coll * 1e3, histtype='step')

plt.axvline(mcs(septum_test.length, 'c') * 1e3, ls='--')

print(len(xp) / float(n_part))
plt.yscale('log')
# plt.xlim(-0.02, 0.02)


plt.show()
