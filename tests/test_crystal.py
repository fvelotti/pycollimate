from __future__ import print_function
from __future__ import division

from scipy.stats import norm, expon
import pickle
import matplotlib.pyplot as plt

def process_command_line(argv):
    """
    Process command line arguments
    """

    help_text = 'Script to use MADX tracking for active elements and the pycollimate module' \
                'for tracking inside matter.\n'

    if argv is None:

        argv = sys.argv[1:]

    # initialize the parser object:
    parser = optparse.OptionParser(
        formatter=optparse.TitledHelpFormatter(width=78),
        add_help_option=None)

    # define options here:
    parser.add_option(      # customized description; put --help last
                            '-h', '--help', action='help',
                            help=help_text)

    parser.set_defaults(black=False)

    parser.add_option('-b', '--black', action='store_true', dest='black', help='Black absorber switch')

    options, args = parser.parse_args(argv)

    return options, args


def rvs_double(n, mu1, mu2, s1, s2, c2):

    tot_len = int(n * (1 - c2)) + int(n * c2)

    if not tot_len == n:
        add_one = 1
    else:
        add_one = 0

    res1 = norm(loc=mu1, scale=s1).rvs(int(n * (1 - c2)))

    res2 = norm(loc=mu2, scale=s2).rvs(int(n * c2) + add_one)

    return np.r_[res1, res2]


def rvs_double_one(mu1, mu2, s1, s2, c2):

    choice = np.random.choice([1, 2], p=[1 - c2, c2])

    if choice == 1:
        return norm(loc=mu1, scale=s1).rvs(1)

    else:
        return norm(loc=mu2, scale=s2).rvs(1)


def crystal_pdf(n=1):

    n_tot = {1: 0, 2: 0, 3: 0}

    if n == 1:

        choice = np.random.choice([1, 2, 3], p=[0.4, 0.06, 0.54])
        n_tot[choice] = 1

        if n_tot[1] == 1:
            x1 = rvs_double_one(-14, 0.1, 8.03, 7.3, 0.5)
        else:
            x1 = []

    else:
        n_tot[1] = int(n * 0.4)
        n_tot[2] = int(n * 0.06)
        n_tot[3] = int(n * 0.54)

        if n_tot[1] + n_tot[2] + n_tot[3] < n:
            n_tot[3] += 1
        elif n_tot[3] + n_tot[2] + n_tot[3] > n:
            n_tot[3] -= 1

        x1 = rvs_double(int(n_tot[1]), -14, 0.1, 8.03, 7.3, 0.5)

    s, l = 88.3, 3.0044

    x2 = []
    while(len(x2) != n_tot[2]):
        temp = float(expon.rvs(size=1, loc=l, scale=s))
        if temp < 130:
            x2.append(temp)

    x3 = norm.rvs(size=n_tot[3], loc=143.8, scale=8.2)

    return np.r_[x1, x2, x3]


def crystal_thin_kick(x_min, x_max, kick, particle_universe):

    kicked = []
    for pp in particle_universe:

        if x_min < pp.x < x_max:
            pp.x_p += kick
            kicked.append(pp.id)

    return kicked


def crystal_rand_kick(x_min, x_max, kick, kick_std, particle_universe):

    kicked = []
    for pp in particle_universe:
        if x_min <= pp.x <= x_max:
            actual_kick = np.random.randn() * kick_std + kick
            pp.x_p += actual_kick
            kicked.append(pp.id)

    return kicked


def crystal_real_kick(x_min, x_max, kick, kick_std, particle_universe):

    kicked = []
    kick_given = []
    # the kick from crystal is now computed from measured
    # pdf from UA9 - the one from the DB is ignored!
    for pp in particle_universe:
        if x_min <= pp.x <= x_max:
            actual_kick = crystal_pdf() * 1e-6 * (kick / np.abs(kick))
            pp.x_p += actual_kick
            pp.update_energy(-2e-3)
            kicked.append(pp.id)
            kick_given.append(actual_kick)

    return kicked, kick_given


def crystal_real_kick_2d(x_min, x_max, angle_c, kick, particle_universe):
    
    p = pickle.load(open('../pycollimate_fast/cry_2d_pdf.p', 'r'))
    kicked = []
    kick_given = []
    # the kick from crystal is now computed from measured
    # pdf from UA9 - the one from the DB is ignored!
    for pp in particle_universe:
        if x_min <= pp.x <= x_max:
            try:
                actual_kick = p.sampleX(n_sample=1, Yini=pp.x_p * 1e6 - angle_c * 1e6) * 1e-6 * np.sign(kick)
            except ValueError:
                inp_angle = pp.x_p * 1e6 - angle_c * 1e6
                if 29 < inp_angle < 150:
                    actual_kick = p.sampleX(n_sample=1, Yini=29) * 1e-6 * np.sign(kick)
                else:
                    actual_kick = p.sampleX(n_sample=1, Yini=-29) * 1e-6 * np.sign(kick)
            pp.x_p += actual_kick
            pp.update_energy(-2e-3)
            kicked.append(pp.id)
            kick_given.append(actual_kick)

    return kicked, kick_given

def main(argv):

    options, args = process_command_line(argv)

    #seed = 100
    #np.random.seed(seed)

    files = os.listdir('.')
    coll_db_file = [a for a in files if 'coll_DB' in a or 'colldb' in a or 'crystal_db' in a]
    #name, angle, particle_universe = read_madx_dialogue_file('outputf.dat', 400.)
    name = 'crystal1'
    particle_universe = make_pencil_beam(10000, 400.0)
    angle = 100 * 1e-6
    for pp in particle_universe:
        pp.x_p = angle

    plt.figure()
    plt.axvline(angle * 1e6, label='initial angle of particles')
    # Get crystal info
    # Kick must be != 0 in order for the crystal to be used, even though 
    # the kick does not mean anything
    x_min, x_max, kick, kick_sd, angle_c = read_crystal_from_db(coll_db_file[0], name)
    kick *= -1

    if kick != 0:

        interacted, kicked = crystal_real_kick_2d(x_min, x_max, angle_c, kick, particle_universe)
        print('Channelling angle = ', angle_c)
        print('Interacted: ', len(interacted))
        print('Kicked: ', len(kicked))
        if len(interacted) > 0:
            write_cryst_interaction_madx('cryst_inter.tfs', interacted)
            write_cryst_interaction_madx('cryst_kick.tfs', kicked)

    plt.hist(list(map(lambda x: x.x_p * 1e6, particle_universe)), bins=100, histtype='step', label='After crystal interaction')
    px = np.array(list(map(lambda x: x.x_p * 1e6, particle_universe)))
    print(np.std(px[px > 50]))
    dist_mean = np.mean(px)
    print(dist_mean - (angle * 1e6))
    plt.axvline(dist_mean, ls='--', color='magenta', label='mean')
    #write_madx_dialogue_file('outputp.dat', particle_universe)
    #write_absfile_madx('outabsf.dat', particle_universe)
    plt.legend()

    plt.show()

if __name__ == '__main__':
    main(sys.argv)
