import numpy as np
import matplotlib.pyplot as plt


def test_crystal_interaction(xp, angle_c, kick):
    sign_cyr = np.sign(kick)
    angle_c_urad = angle_c * 1e6

    inp_angle = ((xp * 1e6) - angle_c_urad) * sign_cyr
    xp_VR_max = np.abs(kick * 1e6)

    actual_kick = np.where(
        (inp_angle > 10) & (inp_angle < xp_VR_max), -15, 0.0
    )
    actual_kick = np.where(
        (inp_angle <= 10) & (inp_angle >= -10),
        np.abs(kick) * 1e6,
        actual_kick,
    )

    actual_kick = actual_kick * sign_cyr
    return actual_kick


xp = np.linspace(-1, -3, 1000) * 1e-3

angle_c = -2e-3

kick = 170e-6

actual_kick = test_crystal_interaction(xp, angle_c, kick)

plt.figure()
plt.plot(xp, actual_kick)
plt.show()
