from pycollimate_fast import crystal_real_kick_2d_vectors, read_pdf
import numpy as np
import matplotlib.pyplot as plt

# Make particle distribution
x = np.linspace(1, 2, 1000) * 1e-3
px = np.linspace(1, 2, 1000) * 1e-3
y = np.zeros_like(x)
py = np.zeros_like(x)
t = np.zeros_like(x)
pt = np.zeros_like(x)

particles = [x, px, y, py, t, pt]


# Make crystal
crystal = read_pdf()

particles_int = crystal_real_kick_2d_vectors(
    1.2e-3, 1.8e-3, 1.5e-3, 300e-6, particles, crystal
)
print(np.array(particles_int))


with plt.style.context("scientific_colors_style"):
    plt.figure(figsize=(4, 3))
    plt.plot(x, px, "o")
    plt.plot(np.array(particles_int[0, :]), np.array(particles_int[1, :]), "x")
plt.show()
