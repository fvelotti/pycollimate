from __future__ import print_function
from __future__ import division

import matplotlib.pyplot as plt
from pycollimate.pycollimate import *


def test_make_septum():
    septum = Septum("Tungsten", 0, 3.13, 0.068, 0.068, 100e-6, 0, 0, 'ZS.21633', 'left', 0.0, 1e-3)
    septum_from_file = make_septum_for_madx('septa_db_sps.tfs', 'ZS.21633')

    assert septum == septum_from_file


septum = Septum("Tungsten", 0, 3.13, 0.068, 0.068, 100e-6, 0.0, 0e-3, 'ZS.21633', 'left', 0.0, 20e-3)

pp = Proton(s=0, x=68.2e-3, x_p=-0.5e-3, y=0, y_p=0, pt=0, p0=400, t=0, flag='outside', id=1)

# Test field of the septum
for s in np.linspace(0, septum.length, 30):
    pp_test = Proton(s=0, x=68.2e-3, x_p=-0.5e-3, y=0, y_p=0, pt=0, p0=400, t=0, flag='outside', id=1)
    pp_test.track_in_field(septum, s)
    plt.plot(pp_test.s, pp_test.x, 'ro')

print('p1 (black)', pp.flag)

plt.plot(pp.s, pp.x, 'ko')

septum_hull = septum.get_hull()

for simplex in septum_hull.simplices:
    plt.plot(septum.get_points_hull()[simplex, 0], septum.get_points_hull()[simplex, 1], 'k')

get_impact_pos(septum, pp)

plt.plot(pp.s, pp.x, 'ko')
print('p1', pp.flag)

do_scattering_routine_septum(septum, [pp])

plt.plot(pp.s, pp.x, 'ko')
print('p1', pp.flag)

# Test hit from inside
# plt.figure()

pp_2 = Proton(s=0, x=67.9e-3, x_p=0.5e-3, y=0, y_p=0, pt=0, p0=400, t=0, flag='outside', id=1)


for s in np.linspace(0, septum.length, 30):
    pp_test = Proton(s=0, x=67.9e-3, x_p=0.5e-3, y=0, y_p=0, pt=0, p0=400, t=0, flag='outside', id=1)
    pp_test.track_drift(s)
    plt.plot(pp_test.s, pp_test.x, 'ro')

print('p2 (green)', pp_2.flag)

plt.plot(pp_2.s, pp_2.x, 'go')


get_impact_pos(septum, pp_2)

plt.plot(pp_2.s, pp_2.x, 'go')
print('p2', pp_2.flag)


# ==============================================
# Check update_s0
# ==============================================

plt.figure()
print('new s0')
pp_3 = Proton(s=1, x=68.2e-3, x_p=-0.5e-3, y=0, y_p=0, pt=0, p0=400, t=0, flag='outside', id=1)

septum.update_s0(1)

# Test field of the septum
for s in np.linspace(septum.s0, septum.s0 + septum.length, 30):
    pp_test = Proton(s=1, x=68.2e-3, x_p=-0.5e-3, y=0, y_p=0, pt=0, p0=400, t=0, flag='outside', id=1)
    pp_test.track_in_field(septum, s - septum.s0)
    plt.plot(pp_test.s, pp_test.x, 'go')

print('p1', pp_3.flag)

plt.plot(pp_3.s, pp_3.x, 'o')

septum_hull = septum.get_hull()

for simplex in septum_hull.simplices:
    plt.plot(septum.get_points_hull()[simplex, 0], septum.get_points_hull()[simplex, 1], 'b')

get_impact_pos(septum, pp_3)

plt.plot(pp_3.s, pp_3.x, 'o')
print('p1', pp_3.flag)

# Test hit from inside

pp_4 = Proton(s=1, x=67.9e-3, x_p=0.5e-3, y=0, y_p=0, pt=0, p0=400, t=0, flag='outside', id=1)


for s in np.linspace(septum.s0, septum.s0 + septum.length, 30):
    pp_test = Proton(s=1, x=67.9e-3, x_p=0.5e-3, y=0, y_p=0, pt=0, p0=400, t=0, flag='outside', id=1)
    pp_test.track_drift(s - septum.s0)
    plt.plot(pp_test.s, pp_test.x, 'go')

print('p2', pp_4.flag)

plt.plot(pp_4.s, pp_4.x, 'o')


get_impact_pos(septum, pp_4)

plt.plot(pp_4.s, pp_4.x, 'ko')
print('p2', pp_4.flag)

benchmark = 'zs'

if benchmark == 'zs':

    # ============================
    # Benchmark with Bren
    # ============================

    plt.figure(10)

    septum_test = Septum("W75RE25", 0, 3.15, 0e-3, 0e-3, 0.2e-3, 0.0, 0e-3, 'ZS.21633', 'left', 0.0, 0.0866e-3, 1.66e-2)

    septum_hull = septum_test.get_hull()

    for simplex in septum_hull.simplices:
        plt.plot(septum_test.get_points_hull()[simplex, 0], septum_test.get_points_hull()[simplex, 1], 'k')


    x, xp = np.genfromtxt('input_points.txt', delimiter=',', unpack=True)

    # x = x[:1000]
    # xp = xp[:1000]

    n = len(x)

    print('number of particles: ', n)

    # x = 1e-3 * np.random.randn(n)
    # xp = 0.01e-3 * np.random.randn(n)

    y = 1e-3 * np.random.randn(n)
    yp = 0.01e-3 * np.random.randn(n)

    t = np.zeros(n)
    pt = np.zeros(n)


    pu = make_particle_universe_from_vector(x * 1e-3, xp * 1e-3, y, yp, t ,pt, 400.)

    cs = get_cross_sections(pu[0], septum_test)


    plt.plot(get_list_from_pu(pu, 's'), get_list_from_pu(pu, 'x'), 'k.')

    # plt.figure(4)
    # plt.plot(get_list_from_pu(pu, 'x'), get_list_from_pu(pu, 'x_p'), 'k.')
    # plt.figure(3)


    for pp in pu:

        get_impact_pos(septum_test, pp)

    plt.plot(get_list_from_pu(pu, 's'), get_list_from_pu(pu, 'x'), 'r.')

    impacted = [ppp.x for ppp in pu if ppp.flag == 'inside']

    do_scattering_routine_septum(septum_test, pu)


    plt.plot(get_list_from_pu(pu, 's'), get_list_from_pu(pu, 'x'), 'g.')

    x_bren, xp_bren = np.genfromtxt('output_points.txt', delimiter=',', unpack=True)

    # x_bren = x_bren[:1000]
    # xp_bren = xp_bren[:1000]
    s_after = [ppp.s for ppp in pu if ppp.flag == 'outside']
    x_after = [ppp.x for ppp in pu if ppp.flag == 'outside']
    xp_after = [ppp.x_p for ppp in pu if ppp.flag == 'outside']

    plt.plot(s_after, x_after, 'r.')
    # plt.plot(x_bren * 1e-3, xp_bren * 1e-3, '.')
    plt.figure()
    plt.hist(np.array(xp_after) * 1e3, histtype='step', label='pycollimate')
    plt.hist(xp_bren, histtype='step', label="bren's")

    plt.yscale('log')
    plt.xlabel("x' / mrad")
    plt.ylabel('counts')
    #plt.xlim(-3, 3)
    plt.legend()
    plt.savefig('benchmark_xp.pdf')

    plt.figure()

    plt.axvspan(0, 0.2, alpha=0.3)
    plt.hist(np.array(x_after) * 1e3, histtype='step', label='pycollimate')
    plt.hist(x_bren, histtype='step', label="bren's")

    plt.yscale('log')
    plt.xlabel('x / mm')
    plt.ylabel('counts')
    #plt.xlim(-5, 5)
    plt.legend()

    plt.savefig('benchmark_x.pdf')

    x_abs = [ppp.x for ppp in pu if ppp.flag == 'absorbed']
    s_abs = [ppp.s for ppp in pu if ppp.flag == 'absorbed']
    print('losses: ', float(len(x_abs)) / len(x))

    print('ratio absorbed / impacted: ', float(len(x_abs)) / len(impacted))

    int_len = septum_test.a / (6.02214129e23 * septum_test.d * cs['inelastic'] * 1e-24) * 1e-2

    print('losses from cs_in = ', 1 - np.exp(-1 * (septum_test.length / int_len)))

    plt.figure()
    # plt.hist2d(np.array(x_after) * 1e3, np.array(xp_after) * 1e3, bins=100, norm=LogNorm())
    plt.hexbin(np.array(x_after) * 1e3, np.array(xp_after) * 1e3, bins='log')

    plt.figure()
    # plt.hist2d(np.array(x_after) * 1e3, np.array(xp_after) * 1e3, bins=100, norm=LogNorm())
    plt.plot(np.array(x_after) * 1e3, np.array(xp_after) * 1e3, '.')

    plt.figure()
    plt.plot(s_abs, x_abs, '.')

else:

    # ============================
    # Benchmark with Bren - Graphite
    # ============================

    plt.figure()

    septum_test = Septum("Graphite", 0, 1.0, 0e-3, 0e-3, 2e-3, 0.0, 0e-3, 'ZS.21633', 'left', 0.0, 0.0)

    septum_hull = septum_test.get_hull()

    # for simplex in septum_hull.simplices:
    #     plt.plot(septum_test.get_points_hull()[simplex, 0], septum_test.get_points_hull()[simplex, 1], 'k')


    x, xp = np.genfromtxt('input_points.txt', delimiter=',', unpack=True)

    # x = x[:2000]
    # xp = xp[:2000]

    n = len(x)

    print('number of particles: ', n)

    # x = 1e-3 * np.random.randn(n)
    # xp = 0.01e-3 * np.random.randn(n)

    y = 1e-3 * np.random.randn(n)
    yp = 0.01e-3 * np.random.randn(n)

    t = np.zeros(n)
    pt = np.zeros(n)


    pu = make_particle_universe_from_vector(x * 1e-3, xp * 1e-3, y, yp, t ,pt, 400)

    cs = get_cross_sections(pu[0], septum_test)

    # plt.plot(get_list_from_pu(pu, 's'), get_list_from_pu(pu, 'x'), 'k.')

    # plt.figure(4)
    # plt.plot(get_list_from_pu(pu, 'x'), get_list_from_pu(pu, 'x_p'), 'k.')
    # plt.figure(3)


    for pp in pu:

        get_impact_pos(septum_test, pp)

    # plt.plot(get_list_from_pu(pu, 's'), get_list_from_pu(pu, 'x'), 'r.')

    impacted = [ppp.x for ppp in pu if ppp.flag == 'inside']

    do_scattering_routine_septum(septum_test, pu)


    # plt.plot(get_list_from_pu(pu, 's'), get_list_from_pu(pu, 'x'), 'g.')

    x_bren, xp_bren = np.genfromtxt('output_points_graphite.txt', delimiter=',', unpack=True)

    # x_bren = x_bren[:2000]
    # xp_bren = xp_bren[:2000]



    x_after = [ppp.x for ppp in pu if ppp.flag == 'outside']
    xp_after = [ppp.x_p for ppp in pu if ppp.flag == 'outside']

    # plt.plot(x_after, xp_after, '.')
    # plt.plot(x_bren * 1e-3, xp_bren * 1e-3, '.')

    plt.title('Diffuser')
    plt.hist(np.array(xp_after) * 1e3, histtype='step', label='pycollimate')
    plt.hist(xp_bren, histtype='step', label="bren's")

    plt.yscale('log')
    plt.xlabel("x' / mrad")
    plt.ylabel('counts')
    plt.xlim(-1, 1)
    plt.legend()
    plt.savefig('benchmark_diffuser_xp.pdf')

    plt.figure()
    plt.title('Diffuser')

    plt.axvspan(0, 2, alpha=0.3)
    plt.hist(np.array(x_after) * 1e3, histtype='step', label='pycollimate')
    plt.hist(x_bren, histtype='step', label="bren's")

    plt.xlim(-5, 5)
    plt.yscale('log')
    plt.xlabel('x / mm')
    plt.ylabel('counts')
    plt.legend()

    plt.savefig('benchmark_diffuser_x.pdf')

    x_abs = [ppp.x for ppp in pu if ppp.flag == 'absorbed']
    print('losses: ', float(len(x_abs)) / len(x))

    print('ratio absorbed / impacted: ', float(len(x_abs)) / len(impacted))

    int_len = septum_test.a / (6.02214129e23 * septum_test.d * cs['inelastic'] * 1e-24) * 1e-2

    print('losses from cs_in = ', 1 - np.exp(-1 * (septum_test.length / int_len)))

    plt.figure()
    plt.hexbin(np.array(x_after) * 1e3, np.array(xp_after) * 1e3, bins='log')


plt.show()
