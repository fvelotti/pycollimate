# =======================
# J. P. Prieto test 
# =======================

from __future__ import print_function
from __future__ import division

import sys, time, copy
sys.dont_write_bytecode = True

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

BASEPATH = '/afs/cern.ch/project/sloex/'
PYCPATH = BASEPATH + 'code/pycollimate'
sys.path.insert(1, PYCPATH)

from pycollimate.pycollimate import Proton, Septum, get_impact_pos, do_scattering_routine_septum

# Define septum parameters
L = 3.13
kick = 4.16355e-4 / 5.
thicks = np.array([50.]) * 1e-6
thetas = np.array([1.]) * 1e-3

# Define particle distribution
N = 50
xmax = 1e-3
x0 = np.linspace(-xmax, xmax, N)

pu0 = [Proton(s = 0.,
              x = xx,
              x_p = 0.,
              y = 0.,
              y_p = 0.,
              pt = 0.,
              p0 = 400.,
              t = 0.,
              flag = 'outside',
              id = k)
              for k, xx in enumerate(x0)]

# Start scan

fig, ax = plt.subplots(len(thicks), len(thetas), figsize=(5, 3))

fdict = {'inside': 'red', 'outside': 'blue', 'absorbed': 'orange'}
totalIter = len(thicks) * len(thetas)
count = 0
r = []
t0 = time.time()

for i, thick in enumerate(thicks):
    for j, theta in enumerate(thetas):

        if totalIter > 1:
            plt.sca(ax[i,j])

        count += 1

        print('Simulation {} of {} --- Time elapsed: {:.2f}'.format(count, totalIter, time.time() - t0))

        pu = copy.deepcopy(pu0)

        septum = Septum('W75RE25', 0., L, -.5 * thick, np.tan(theta) - .5 * thick, thick, 0., 0., 'test', 'left', theta, kick, 1.)

        [get_impact_pos(septum, p) for p in pu]
        do_scattering_routine_septum(septum, pu)

        nAbsorbed = len([p for p in pu if p.flag == 'absorbed'])
        nBehind = len([p for p in pu if p.s < L])

        r.append((thick, theta, nAbsorbed, nBehind))

        # Plot septum
        (sa, xa), (sb, xb), (sc, xc), (sd, xd) = septum.get_points_hull()
        xa, xb, xc, xd = map(lambda x: x*1e3, (xa, xb, xc, xd))

        plt.plot([sa, sb], [xa, xb], ls = '--', c = 'k', alpha = 1.)
        plt.plot([sb, sc], [xb, xc], ls = '--', c = 'k', alpha = 1.)
        plt.plot([sc, sd], [xc, xd], ls = '--', c = 'k', alpha = 1.)
        plt.plot([sd, sa], [xd, xa], ls = '--', c = 'k', alpha = 1.)

        # Plot trajectories
        [plt.plot([p0.s, p.s], [1e3 * p0.x, 1e3 * p.x], c = fdict[p.flag]) for p0, p in zip(pu0, pu)]

        plt.title(r'$\theta$ = {} mrad  --  w = {} $\mu$m'.format(theta*1e3, thick*1e6) + r'  --  $\Delta N_{abs}$' + r' = {}'.format(nBehind - nAbsorbed))

        if i == len(thicks)-1 and j == 0:
            plt.xlabel('S [m]')
            plt.ylabel('X [mm]')

r = pd.DataFrame(r, columns = ['thick', 'theta', 'absorbed', 'behind'])

plt.savefig('traces_crossing_septum.pdf')
plt.tight_layout()
plt.show()

