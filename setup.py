"""
© Copyright F.M. Velotti 2017-2020. All rights not expressly granted are reserved’
"""

import os
import numpy
import setuptools
from Cython.Build import cythonize
from pathlib import Path

HERE = Path(__file__).parent.absolute()
with (HERE / "README.md").open("rt") as fh:
    LONG_DESCRIPTION = fh.read().strip()


exts = [
    setuptools.Extension(
        name="pycollimate_fast.pycollimate_fast",
        sources=["./pycollimate_fast/pycollimate_fast.pyx"],
        include_dirs=[numpy.get_include()],
    )
]

REQUIREMENTS = {"core": ["numpy", "prettytable", "pandas", "scipy", "cython==0.29.25"]}

setuptools.setup(
    name="pycollimate",
    version="3.0.1",
    author="Francesco Maria Velotti",
    author_email="francesco.maria.velotti@cern.ch",
    description="Scattering routine",
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    url="",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: ",
        "Operating System :: OS Independent",
    ],
    include_package_data=True,
    install_requires=REQUIREMENTS["core"],
    python_requires=">=2.7",
    ext_modules=cythonize(exts),
)
