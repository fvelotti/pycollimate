from pycollimate import pycollimate as pc

planes = pc.get_gauss_distribution(input='sequence_totrack_ti8.tfs', sigmas=4, beam_t='LIU', n_part=100000)

pc.writeDistributionFile(planes, 'initial_distribution_test')

