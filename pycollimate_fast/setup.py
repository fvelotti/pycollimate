"""
© Copyright F.M. Velotti 2017-2020. All rights not expressly granted are reserved’
"""
from distutils.core import setup
from Cython.Build import cythonize
setup(name='pycollimate_fast', ext_modules=cythonize('pycollimate_fast.pyx'),)
