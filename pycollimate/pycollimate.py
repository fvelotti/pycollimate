"""
© Copyright F.M. Velotti 2017-2020. All rights not expressly granted are reserved’
"""
from __future__ import division
from __future__ import print_function

import optparse
import re
import random
from math import *
import sys
import fileinput
import os
import collections
from scipy.spatial import ConvexHull
import copy
from datetime import datetime
import numpy as np
from prettytable import PrettyTable
from scipy.integrate import quad as quad
from scipy.stats import truncnorm as trandn
from scipy.constants import Avogadro as avogadro
import pandas as pd


def string_to_float(seq):
    for x in seq:
        try:
            yield float(x)
        except ValueError:
            yield x


def writeDistributionFile(planes, filename_out):

    x1, px1, y1, py1, t, pt = planes

    head = []
    head.append('@ TYPE          %04s "USER"\n')
    head.append('@ FileCreated   %28s "' + str(datetime.now()) + '"\n')
    head.append('@ Name          %007 "MYTABLE"\n')

    halo_tab = PrettyTable(
        ["*", "NUMBER", "TURN", "X", "PX", "Y", "PY", "T", "PT", "S", "E"]
    )
    halo_tab.align = "r"
    halo_tab.border = False
    halo_tab.add_row(
        [
            "$",
            "%d",
            "%d",
            "%le",
            "%le",
            "%le",
            "%le",
            "%le",
            "%le",
            "%le",
            "%le",
        ]
    )

    with open(filename_out + ".txt", "w") as fp:
        for i in range(len(head)):
            fp.write(head[i])
        for i in range(len(x1)):
            halo_tab.add_row(
                [
                    " ",
                    i + 1,
                    0,
                    x1[i],
                    px1[i],
                    y1[i],
                    py1[i],
                    0.0000000,
                    pt[i],
                    0.0000000,
                    450.000978170460,
                ]
            )
        fp.write(halo_tab.get_string())


def hist_cool(
    data,
    bins=10,
    range=None,
    color="black",
    linew=2.5,
    density=True,
    fitted=False,
    color_fit="red",
    norm=1.0,
    label=None,
):
    histo, bin_edge = np.histogram(
        data, bins=bins, range=range, density=density
    )
    bins_centre = []
    for k in range(1, len(bin_edge)):
        bins_centre.append((bin_edge[k] + bin_edge[k - 1]) / 2.0)
    count = 0
    for i in range(0, len(histo)):
        if histo[i] > 0:
            count += 1
            if count == 1:
                plt.plot(
                    [bins_centre[i], bins_centre[i]],
                    [0, histo[i] / float(norm)],
                    lw=linew,
                    color=color,
                    label=label,
                )
            plt.plot(
                [bins_centre[i], bins_centre[i]],
                [0, histo[i] / float(norm)],
                lw=linew,
                color=color,
            )

    if fitted:
        mu = np.mean(data)
        sigma = np.std(data)
        pdf_x_x = np.linspace(-5 * sigma, 5 * sigma, 1000)
        pdf_x = gauss.pdf(pdf_x_x, loc=mu, scale=sigma)
        plt.plot(pdf_x_x, pdf_x, lw=1, color=color_fit)

    return histo / float(norm), bins_centre


def get_sequencetwiss(twiss_file):
    with open(twiss_file, "r") as f:
        for i, s in enumerate(f):
            if i == 2:
                sequence = s.split()[3].strip('"')
    return sequence


def get_twiss_var(file_name, path, list_var):
    """
    Gets a twiss and a list of twiss columns as input and gives a matrix where each
    column represents a twiss column in the given order

    @param file_name: twiss file name
    @param path: path of the file
    @param list_var: list of strings of the required twiss columns
    @return: matrix of the asked twiss columns
    """
    list_var = [temp.upper() for temp in list_var]

    col_var = []
    variable = []

    with open(path + file_name, "r") as ff:
        for _ in range(1, 46):
            next(ff)

        names = next(ff).split()

        for nn in list_var:
            if nn in names:
                col_var.append(names.index(nn) - 1)
            else:
                print("Var %s not found!" % nn)
        for _ in range(len(col_var)):
            variable.append([])
        next(ff)

        if "KEYWORD" in list_var:
            i_kw = list_var.index("KEYWORD")
        else:
            i_kw = -1

        if "NAME" in list_var:
            i_n = list_var.index("NAME")
        else:
            i_n = -1

        for line in ff:
            col = line.split()
            for index in zip(col_var, range(0, len(col_var))):
                if index[1] == i_kw or index[1] == i_n:
                    variable[index[1]].append(col[index[0]])
                else:
                    variable[index[1]].append(float(col[index[0]]))
    return variable


def get_beam(beam_type):
    if beam_type == "LHC":
        beam = dict(
            type="LHC",
            momentum=450,
            emit_nom=3.5e-6,
            emit_n=3.5e-6,
            intensity=288 * 1.15e11,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=5,
        )
    elif beam_type == "LIU":
        beam = dict(
            type="LIU",
            momentum=450,
            emit_nom=3.5e-6,
            emit_n=1.37e-6,
            intensity=288 * 2.0e11,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=5,
        )
    elif beam_type == "LHC-meas":
        beam = dict(
            type="LHC-meas",
            momentum=450,
            emit_nom=3.5e-6,
            emit_n=2.0e-6,
            intensity=288 * 1.2e11,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=6.8,
        )
    else:
        beam = dict(
            type="HL-LHC",
            momentum=450,
            emit_nom=3.5e-6,
            emit_n=2.08e-6,
            intensity=288 * 2.32e11,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=5,
        )
    return beam


class Beam(object):
    def __init__(self, beam_type):
        bb = get_beam(beam_type)
        self.emit_n = bb["emit_n"]
        self.type = bb["type"]
        self.momentum = bb["momentum"]
        self.emit_nom = bb["emit_nom"]
        self.intensity = bb["intensity"]
        self.dpp = bb["dpp_t"]
        self.mass = 0.93827231
        self.e_tot = np.sqrt(self.momentum ** 2 + self.mass ** 2)
        self.gamma = self.e_tot / self.mass
        self.beta = np.sqrt(1 - 1 / self.gamma ** 2)

        def get_brho(self):
            p = 1.67e-27 * self.beta * self.gamma * 3e8
            return p / 1.6e-19


class Proton(object):
    """
    Class to create an object Proton
        s is the longitudinal coordinate of the particle
        x and y are in meters
        x_p and y_p are in rad
        p0 in GeV/c
        remarks: all equivalent energies are in GeV
        Difference of 1% between De = sqrt(p0^2 + mp^2) and De = dp_p * beta^2 * e_tot
    """

    def __init__(self, s, x, x_p, y, y_p, pt, p0, t, flag, id):
        c = 299792458.0
        m_p = 0.93827231
        self.s = s
        self.x = x
        self.x_p = x_p
        self.y = y
        self.y_p = y_p
        self.pt = pt
        self.p0 = float(p0)
        self.t = t
        self.flag = flag
        self.id = id
        self.de = self.pt * self.p0
        self.mass = m_p  # from Accelerator Physics - S.Y. Lee
        self.e0 = np.sqrt(
            self.p0 ** 2 + self.mass ** 2
        )  # e0 is the total energy of the synchrotron particle
        self.beta0 = self.p0 / self.e0
        self.dp_p = (1 / self.beta0 ** 2) * (self.de / self.e0)
        self.norm_mom = np.sqrt(
            (1 + self.dp_p) ** 2 - self.x_p ** 2 - self.y_p ** 2
        )  # p_z to obtain x' from p_x
        self.mom_p = self.p0 * (1.0 + self.dp_p)
        self.e_tot = np.sqrt(self.mom_p ** 2 + self.mass ** 2)
        self.kinetic = self.e_tot - self.mass
        self.gamma = self.e_tot / self.mass
        self.beta = self.mom_p / self.e_tot
        self.velocity = self.beta * c

    def update_energy(self, de):
        """
        Update self proton energy of a positive quantity de
        @param de: energy to be added to the self proton energy
        @return: updated proton object
        """
        # if de < -60:
        #    print 'DE = ', de
        delta = self.de + de
        self.pt = delta / self.p0
        self.de = self.pt * self.p0
        self.dp_p = (1.0 / self.beta0 ** 2) * (self.de / self.e0)
        self.norm_mom = np.sqrt(
            (1.0 + self.dp_p) ** 2 - self.x_p ** 2 - self.y_p ** 2
        )
        self.mom_p = self.p0 * (1.0 + self.dp_p)
        self.e_tot = np.sqrt(self.mom_p ** 2 + self.mass ** 2)
        self.kinetic = self.e_tot - self.mass
        self.gamma = self.e_tot / self.mass
        self.beta = self.mom_p / self.e_tot
        self.velocity = self.beta * 299792458.0

    def track_drift(self, l):
        """
        Track a Proton for a drift of length l using the exact hamiltonian (PTC)
        @param l: length of the drift
        @return: particle with updated coordinates
        """
        self.x = self.x + (self.x_p / self.norm_mom) * l
        self.y = self.y + (self.y_p / self.norm_mom) * l
        self.s = self.s + l

    def track_in_field(self, septum, s):
        self.x = (
            self.x
            + (
                self.x_p / self.norm_mom
                + (0.5 * (septum.kick / (self.dp_p + 1)) / septum.length) * s
            )
            * s
        )
        self.x_p = (
            self.x_p / self.norm_mom
            + ((septum.kick / (self.dp_p + 1)) / septum.length) * s
        ) * self.norm_mom

        self.y = self.y + (self.y_p / self.norm_mom) * s

        self.s = self.s + s

    def transform_t(self, t_mom):
        """
        In order to specify the cross section and the differential cross section  of different processes,
        it has been used the Lorents-invariants 'momentum transfer t' and 'CM energy squared s'.
        Here t is used as input parameter in order to update the self proton transverse momenta
        @param t_mom: t = (theta * p)**2
        @return: Update particle transverse momenta
        """
        theta = np.sqrt(t_mom) / self.mom_p
        while True:
            va = 2.0 * np.random.rand() - 1.0
            vb = np.random.rand()
            r2 = va ** 2 + vb ** 2
            # r2 = abs(random.gauss(0, 1))
            if r2 <= 1:
                tx = theta * (2.0 * va * vb) / r2
                ty = theta * (va ** 2 - vb ** 2) / r2
                self.x_p += tx
                self.y_p += ty
                break

    def make_interaction(self, interaction, collimator, cross_sections):
        """
        Function used to modify particle longitudinal and transverse momenta according with the chosen interaction
        @param interaction: string representing the chosen interaction
        @param collimator: Object collimator
        @param cross_sections: dictionary obtained from 'get_cross_sections()' function. It contains cross sections
                               values for the considered different phenomena
        """
        s_CM = 2.0 * self.mass * self.p0
        bpp = 7.156 + 1.439 * np.log(np.sqrt(s_CM))

        if interaction == "inelastic":
            # print self.id
            self.flag = "absorbed"
        elif interaction == "elastic_N":
            # print self.id
            bn = (
                collimator.bnref
                * cross_sections["tot_pN"]
                / collimator.cs_tot_ref
            )
            t_mom = -np.log(np.random.rand()) / bn
            self.transform_t(t_mom)
        elif interaction == "elastic_n":
            # print self.id
            t_mom = -np.log(np.random.rand()) / bpp
            self.transform_t(t_mom)
        elif interaction == "SD":
            # print 'SD event: ', self.id
            m2 = np.exp(np.random.rand() * np.log(0.15 * s_CM))
            # dpp = self.dp_p - self.mom_p * (m2 / s_CM) / self.p0
            # self.update_energy(self.beta0 ** 2 * self.e0 * dpp)
            # print m2, s_CM
            p_new = self.mom_p * (1 - m2 / s_CM)
            de_sd = np.sqrt(p_new ** 2 + self.mass ** 2) - self.e_tot
            # print 'SD = ', de_sd
            self.update_energy(de_sd)
            if m2 < 2:
                bsd = 2.0 * bpp
            elif 2 <= m2 <= 5:
                bsd = (106.0 - 17.0 * m2) * bpp / 26.0  # 26 is in sixTrack
            elif m2 > 5:
                bsd = 7.0 * bpp / 12.0
            t_mom = -np.log(np.random.rand()) / bsd
            self.transform_t(t_mom)
        elif interaction == "ruth":
            # print self.id
            _, tx, ty = ruth_six(collimator, self, 1)
            xpfac = self.x_p / self.norm_mom
            ypfac = self.y_p / self.norm_mom
            anglex = np.arctan(xpfac) + tx
            angley = np.arctan(ypfac) + ty
            self.x_p = np.tan(anglex) * self.norm_mom
            self.y_p = np.tan(angley) * self.norm_mom

    def mom_dir_motion(self):
        return np.sqrt(
            1.0
            + (self.x_p / self.norm_mom) ** 2
            + (self.y_p / self.norm_mom) ** 2
        )


class Collimator(object):
    """
    Class to create an object collimator. It includes all its physical info.
    unc_u: it's the precision of the upstream position of the jaw
    unc_d: it's the precision of the downstream position of the jaw
    """

    def __init__(
        self,
        material,
        s0,
        length,
        type,
        aper_x,
        aper_y,
        unc_p_u,
        unc_p_d,
        unc_n_u,
        unc_n_d,
        name,
        angle=0,
        eff_dens=1.0,
    ):
        self.s0 = s0
        self.length = length
        self.name = name
        self.type = type
        self.angle = angle * pi / 180.0
        self.aper_x = aper_x
        self.aper_y = aper_y
        self.unc_p_u = unc_p_u
        self.unc_p_d = unc_p_d
        self.unc_n_u = -1 * unc_n_u
        self.unc_n_d = -1 * unc_n_d
        self.material = material
        self.pptref = 0.04  # \sigma_{pp, ref}^{TOT}
        self.pperef = 0.007  # \sigma_{pp, ref}^{elastic}
        self.SDcoe = 0.68e-3  # coefficient of the Single Diffractive CS [mb]
        self.pref = 450.0  # reference momentum in GeV/c
        self.pptco = 0.05788  # Exponent of eq.(3.12) -> s_pp^TOT = s_ref^TOT*(p/p_ref)^pptco
        self.ppeco = 0.04792  # Exponent of eq.(3.7) -> s_pp^el = s_ref^el*(p/p_ref)^ppeco
        self.freeco = (
            1.618  # Coeff of last equation (3.14) -> n_eff = freeco*A^(1/3)
        )

        self.eff_dens = eff_dens

        if self.material == "Tungsten":
            self.z = 74.0
            self.a = 183.84  # [g mol-1]
            self.r = 0.52
            self.d = 19.3 * self.eff_dens  # 1.83  # [g/cm3]
            self.i_max = 727.0  # eV
            self.c_bar = 5.4059
            self.x0 = 0.2167
            self.x1 = 3.496
            self.a_small = 0.1551
            self.k = 2.8447
            self.delta0 = 0.14
            self.rad_length = 0.353e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = 2.765  # [b]
            self.cs_in_ref = 1.591  # [b]
            self.bnref = (
                455.89  # forward slope for coherent p-N scattering b_pN
            )
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.768e-2
        elif self.material == "Tantalum":
            self.z = 73.0
            self.a = 180.9  # [g mol-1]
            self.r = 0.52
            self.d = 16.654 * self.eff_dens  # [g/cm3]
            self.i_max = 718.0  # eV
            self.c_bar = 5.5262
            self.x0 = 0.2117
            self.x1 = 3.4805
            self.a_small = 0.1780
            self.k = 2.7623
            self.delta0 = 0.1400
            self.rad_length = 0.4094e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = 2.935  # [b]
            self.cs_in_ref = 1.707  # [b]
            self.bnref = (
                455.89  # forward slope for coherent p-N scattering b_pN
            )
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.768e-2
        elif self.material == "W75RE25":
            self.z = 74.0
            self.a = 183.84  # [g mol-1]
            self.r = 0.52
            self.d = 20.0 * self.eff_dens  # [g/cm3] 2080 wires, 3.15 m, 80 um
            self.i_max = 727.0  # eV
            self.c_bar = 5.4059
            self.x0 = 0.2167
            self.x1 = 3.496
            self.a_small = 0.1551
            self.k = 2.8447
            self.delta0 = 0.14
            self.rad_length = 0.353e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = 2.765  # [b]
            self.cs_in_ref = 1.591  # [b]
            self.bnref = (
                455.89  # forward slope for coherent p-N scattering b_pN
            )
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.768e-2
        elif self.material == "W75RE25_FULL":
            self.z = 74.0
            self.a = 183.84  # [g mol-1]
            self.r = 0.52
            self.d = 20.0 * self.eff_dens  # [g/cm3]
            self.i_max = 727.0  # eV
            self.c_bar = 5.4059
            self.x0 = 0.2167
            self.x1 = 3.496
            self.a_small = 0.1551
            self.k = 2.8447
            self.delta0 = 0.14
            self.rad_length = 0.353e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = 2.765  # [b]
            self.cs_in_ref = 1.591  # [b]
            self.bnref = (
                455.89  # forward slope for coherent p-N scattering b_pN
            )
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.768e-2
        elif self.material == "Graphite":
            self.z = 6.0
            self.a = 12.0107  # [g mol-1]
            self.r = 0.25
            self.d = 2.21 * self.eff_dens  # 1.83  # [g/cm3]
            self.i_max = 78.0  # eV
            self.c_bar = 3.155
            self.x0 = 0.048
            self.x1 = 2.5387
            self.a_small = 0.2076
            self.k = 2.9532
            self.delta0 = 0.14
            self.rad_length = 19.32e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = 0.337  # [b]
            self.cs_in_ref = 0.232  # [b]
            self.bnref = (
                70.0  # forward slope for coherent p-N scattering b_pN (eq.3.15)
            )
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.0076e-2
        elif self.material == "R4550":
            self.z = 6.0
            self.a = 12.0107  # [g mol-1]
            self.r = 0.25
            self.d = 1.83 * self.eff_dens  # 1.83  # [g/cm3]
            self.i_max = 78.0  # eV
            self.c_bar = 3.155
            self.x0 = 0.048
            self.x1 = 2.5387
            self.a_small = 0.2076
            self.k = 2.9532
            self.delta0 = 0.14
            self.rad_length = 0.188 / self.eff_dens  # [m]
            self.cs_tot_ref = 0.337  # [b]
            self.cs_in_ref = 0.232  # [b]
            self.bnref = (
                70.0  # forward slope for coherent p-N scattering b_pN (eq.3.15)
            )
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.0076e-2
        elif self.material == "BN5000":
            self.z = 6.0
            self.a = 12.0107  # [g mol-1]
            self.r = 0.25
            self.d = 1.925 * self.eff_dens  # 1.83  # [g/cm3]
            self.i_max = 78.0  # eVi
            self.c_bar = 3.155
            self.x0 = 0.048
            self.x1 = 2.5387
            self.a_small = 0.2076
            self.k = 2.9532
            self.delta0 = 0.14
            self.rad_length = 0.188 / self.eff_dens  # [m]
            self.cs_tot_ref = 0.337  # [b]
            self.cs_in_ref = 0.232  # [b]
            self.bnref = (
                70.0  # forward slope for coherent p-N scattering b_pN (eq.3.15)
            )
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.0076e-2
        elif self.material == "Aluminum":
            self.z = 13.0
            self.a = 26.892  # [g mol-1]
            self.r = 0.302
            self.d = 2.7 * self.eff_dens  # [g/cm3]
            self.i_max = 166.0  # eV
            self.c_bar = 4.2395
            self.x0 = 0.1708
            self.x1 = 3.0127
            self.a_small = 0.0802
            self.k = 3.6345
            self.delta0 = 0.12
            self.rad_length = 8.9e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = 0.634  # [b]
            self.cs_in_ref = 0.418  # [b]
            self.bnref = 120.3  # forward slope for coherent p-N scattering b_pN
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.034e-2
        elif self.material == "Copper":
            self.z = 29.0
            self.a = 63.546  # [g mol-1]
            self.r = 0.366
            self.d = 8.96 * self.eff_dens  # [g/cm3]
            self.i_max = 322.0  # eV
            self.c_bar = 4.419
            self.x0 = -0.0254
            self.x1 = 3.2792
            self.a_small = 0.1434
            self.k = 2.9044
            self.delta0 = 0.08
            self.rad_length = 1.43e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = 1.253  # [b]
            self.cs_in_ref = 0.769  # [b]
            self.bnref = 217.8  # forward slope for coherent p-N scattering b_pN
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.153e-2
        elif self.material == "Silicon":
            self.z = 14.0
            self.a = 28.0855  # [g mol-1]
            self.r = 0.325  # This is used only for rutherford scattering...is this correct? Number obtained scaling graphite by 30% as radius 30% larger
            self.d = 2.328 * self.eff_dens  # [g/cm3]
            self.i_max = 173.0  # eV => Mean exitation energy (dpgg)
            self.c_bar = (
                4.4351  # Energy loss in matter by heavy particles by Don Groom
            )
            self.x0 = (
                0.2014  # Energy loss in matter by heavy particles by Don Groom
            )
            self.x1 = (
                2.8715  # Energy loss in matter by heavy particles by Don Groom
            )
            self.a_small = (
                0.1492  # Energy loss in matter by heavy particles by Don Groom
            )
            self.k = (
                3.2546  # Energy loss in matter by heavy particles by Don Groom
            )
            self.delta0 = (
                0.14  # Energy loss in matter by heavy particles by Don Groom
            )
            self.rad_length = 9.37e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = (
                self.a / (avogadro * self.d * 30.16) * 1e24
            )  # [b] the numeric value is the nuclear collision length in cm from pdg
            self.cs_in_ref = (
                self.a / (avogadro * self.d * 46.52) * 1e24
            )  # [b] the numeric value is the nuclear interaction length in cm from pdg
            self.bnref = 124.14  # forward slope for coherent p-N scattering b_pN (eq.3.15)
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.034e-2  # copied from Aluminum...
        elif self.material == "Titanium":
            self.z = 22.0
            self.a = 47.9  # [g mol-1]
            self.r = 0.147  # This is used only for rutherford scattering
            self.d = 4.54 * self.eff_dens  # [g/cm3]
            self.i_max = 233.0  # eV => Mean exitation energy (dpgg)
            self.c_bar = (
                4.450  # Energy loss in matter by heavy particles by D Groom
            )
            self.x0 = (
                0.0957  # Energy loss in matter by heavy particles by Do D Groom
            )
            self.x1 = (
                3.0386  # Energy loss in matter by heavy particles by Do D Groom
            )
            self.a_small = (
                0.1566  # Energy loss in matter by heavy particles by Do D Groom
            )
            self.k = (
                3.0302  # Energy loss in matter by heavy particles by Do D Groom
            )
            self.delta0 = (
                0.12  # Energy loss in matter by heavy particles by Do D Groom
            )
            self.rad_length = 3.56e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = (
                self.a / (avogadro * self.d * 17.37) * 1e24
            )  # [b] the numeric value is the nuclear collision length in cm from pdg
            self.cs_in_ref = (
                self.a / (avogadro * self.d * 27.80) * 1e24
            )  # [b] the numeric value is the nuclear interaction length in cm from pdg
            self.bnref = 14.1 * self.a ** (
                0.65
            )  # forward slope for coherent p-N scattering b_pN (eq.3.15)
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.034e-2  # copied from Aluminum...not used
        elif self.material == "Iron":
            self.z = 26.0
            self.a = 55.847  # [g mol-1]
            self.r = 0.126  # This is used only for rutherford scattering
            self.d = 7.874 * self.eff_dens  # [g/cm3]
            self.i_max = 286.0  # eV => Mean exitation energy (dpgg)
            self.c_bar = (
                4.2911  # Energy loss in matter by heavy particles by D Groom
            )
            self.x0 = (
                -0.0012
            )  # Energy loss in matter by heavy particles by Do D Groom
            self.x1 = (
                3.1531  # Energy loss in matter by heavy particles by Do D Groom
            )
            self.a_small = 0.1468  # Ti Energy loss in matter by heavy particles by Do D Groom
            self.k = (
                2.9632  # Energy loss in matter by heavy particles by Do D Groom
            )
            self.delta0 = (
                0.12  # Energy loss in matter by heavy particles by Do D Groom
            )
            self.rad_length = 1.757e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = (
                self.a / (avogadro * self.d * 10.37) * 1e24
            )  # [b] the numeric value is the nuclear collision length in cm from pdg
            self.cs_in_ref = (
                self.a / (avogadro * self.d * 16.77) * 1e24
            )  # [b] the numeric value is the nuclear interaction length in cm from pdg
            self.bnref = 14.1 * self.a ** (
                0.65
            )  # forward slope for coherent p-N scattering b_pN (eq.3.15)
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.034e-2  # copied from Aluminum...not used

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __str__(self):
        attrs = vars(self)
        return ", ".join("%s: %s" % item for item in attrs.items())


class Septum(object):
    """
    Class to create an object collimator. It includes all its physical info.
    unc_u: it's the precision of the upstream position of the jaw
    unc_d: it's the precision of the downstream position of the jaw
    """

    def __init__(
        self,
        material,
        s0,
        length,
        aper_u,
        aper_d,
        thickness,
        unc_u,
        unc_d,
        name,
        side,
        angle=0,
        kick=0,
        eff_dens=1.0,
    ):
        self.s0 = s0
        self.length = length
        self.name = name
        self.angle = angle * pi / 180.0
        self.unc_u = unc_u
        self.unc_d = unc_d
        self.material = material

        self.aper_u = aper_u
        self.aper_d = aper_d
        self.thickness = thickness
        self.kick = kick
        self.side = side

        self.pptref = 0.04  # \sigma_{pp, ref}^{TOT}
        self.pperef = 0.007  # \sigma_{pp, ref}^{elastic}
        self.SDcoe = 0.68e-3  # coefficient of the Single Diffractive CS [mb]
        self.pref = 450.0  # reference momentum in GeV/c
        self.pptco = 0.05788  # Exponent of eq.(3.12) -> s_pp^TOT = s_ref^TOT*(p/p_ref)^pptco
        self.ppeco = 0.04792  # Exponent of eq.(3.7) -> s_pp^el = s_ref^el*(p/p_ref)^ppeco
        self.freeco = (
            1.618  # Coeff of last equation (3.14) -> n_eff = freeco*A^(1/3)
        )

        self.eff_dens = eff_dens

        if self.material == "Tungsten":
            self.z = 74.0
            self.a = 183.84  # [g mol-1]
            self.r = 0.52
            self.d = 19.3 * self.eff_dens  # 1.83  # [g/cm3]
            self.i_max = 727.0  # eV
            self.c_bar = 5.4059
            self.x0 = 0.2167
            self.x1 = 3.496
            self.a_small = 0.1551
            self.k = 2.8447
            self.delta0 = 0.14
            self.rad_length = 0.353e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = 2.765  # [b]
            self.cs_in_ref = 1.591  # [b]
            self.bnref = (
                455.89  # forward slope for coherent p-N scattering b_pN
            )
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.768e-2
        elif self.material == "Tantalum":
            self.z = 73.0
            self.a = 180.9  # [g mol-1]
            self.r = 0.52
            self.d = 16.654 * self.eff_dens  # [g/cm3]
            self.i_max = 718.0  # eV
            self.c_bar = 5.5262
            self.x0 = 0.2117
            self.x1 = 3.4805
            self.a_small = 0.1780
            self.k = 2.7623
            self.delta0 = 0.1400
            self.rad_length = 0.4094e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = 2.935  # [b]
            self.cs_in_ref = 1.707  # [b]
            self.bnref = (
                455.89  # forward slope for coherent p-N scattering b_pN
            )
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.768e-2
        elif self.material == "W75RE25":
            self.z = 74.0
            self.a = 183.84  # [g mol-1]
            self.r = 0.52
            self.d = 20.0 * self.eff_dens  # [g/cm3] 2080 wires, 3.15 m, 80 um
            self.i_max = 727.0  # eV
            self.c_bar = 5.4059
            self.x0 = 0.2167
            self.x1 = 3.496
            self.a_small = 0.1551
            self.k = 2.8447
            self.delta0 = 0.14
            self.rad_length = 0.353e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = 2.765  # [b]
            self.cs_in_ref = 1.591  # [b]
            self.bnref = (
                455.89  # forward slope for coherent p-N scattering b_pN
            )
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.768e-2
        elif self.material == "W75RE25_FULL":
            self.z = 74.0
            self.a = 183.84  # [g mol-1]
            self.r = 0.52
            self.d = 20.0 * self.eff_dens  # [g/cm3]
            self.i_max = 727.0  # eV
            self.c_bar = 5.4059
            self.x0 = 0.2167
            self.x1 = 3.496
            self.a_small = 0.1551
            self.k = 2.8447
            self.delta0 = 0.14
            self.rad_length = 0.353e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = 2.765  # [b]
            self.cs_in_ref = 1.591  # [b]
            self.bnref = (
                455.89  # forward slope for coherent p-N scattering b_pN
            )
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.768e-2
        elif self.material == "Graphite":
            self.z = 6.0
            self.a = 12.0107  # [g mol-1]
            self.r = 0.25
            self.d = 2.21 * self.eff_dens  # 1.83  # [g/cm3]
            self.i_max = 78.0  # eV
            self.c_bar = 3.155
            self.x0 = 0.048
            self.x1 = 2.5387
            self.a_small = 0.2076
            self.k = 2.9532
            self.delta0 = 0.14
            self.rad_length = 19.32e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = 0.337  # [b]
            self.cs_in_ref = 0.232  # [b]
            self.bnref = (
                70.0  # forward slope for coherent p-N scattering b_pN (eq.3.15)
            )
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.0076e-2
        elif self.material == "R4550":
            self.z = 6.0
            self.a = 12.0107  # [g mol-1]
            self.r = 0.25
            self.d = 1.83 * self.eff_dens  # 1.83  # [g/cm3]
            self.i_max = 78.0  # eV
            self.c_bar = 3.155
            self.x0 = 0.048
            self.x1 = 2.5387
            self.a_small = 0.2076
            self.k = 2.9532
            self.delta0 = 0.14
            self.rad_length = 0.188 / self.eff_dens  # [m]
            self.cs_tot_ref = 0.337  # [b]
            self.cs_in_ref = 0.232  # [b]
            self.bnref = (
                70.0  # forward slope for coherent p-N scattering b_pN (eq.3.15)
            )
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.0076e-2
        elif self.material == "BN5000":
            self.z = 6.0
            self.a = 12.0107  # [g mol-1]
            self.r = 0.25
            self.d = 1.925 * self.eff_dens  # 1.83  # [g/cm3]
            self.i_max = 78.0  # eVi
            self.c_bar = 3.155
            self.x0 = 0.048
            self.x1 = 2.5387
            self.a_small = 0.2076
            self.k = 2.9532
            self.delta0 = 0.14
            self.rad_length = 0.188 / self.eff_dens  # [m]
            self.cs_tot_ref = 0.337  # [b]
            self.cs_in_ref = 0.232  # [b]
            self.bnref = (
                70.0  # forward slope for coherent p-N scattering b_pN (eq.3.15)
            )
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.0076e-2
        elif self.material == "Aluminum":
            self.z = 13.0
            self.a = 26.892  # [g mol-1]
            self.r = 0.302
            self.d = 2.7 * self.eff_dens  # [g/cm3]
            self.i_max = 166.0  # eV
            self.c_bar = 4.2395
            self.x0 = 0.1708
            self.x1 = 3.0127
            self.a_small = 0.0802
            self.k = 3.6345
            self.delta0 = 0.12
            self.rad_length = 8.9e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = 0.634  # [b]
            self.cs_in_ref = 0.418  # [b]
            self.bnref = 120.3  # forward slope for coherent p-N scattering b_pN
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.034e-2
        elif self.material == "Copper":
            self.z = 29.0
            self.a = 63.546  # [g mol-1]
            self.r = 0.366
            self.d = 8.96 * self.eff_dens  # [g/cm3]
            self.i_max = 322.0  # eV
            self.c_bar = 4.419
            self.x0 = -0.0254
            self.x1 = 3.2792
            self.a_small = 0.1434
            self.k = 2.9044
            self.delta0 = 0.08
            self.rad_length = 1.43e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = 1.253  # [b]
            self.cs_in_ref = 0.769  # [b]
            self.bnref = 217.8  # forward slope for coherent p-N scattering b_pN
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.153e-2
        elif self.material == "Silicon":
            self.z = 14.0
            self.a = 28.0855  # [g mol-1]
            self.r = 0.325  # This is used only for rutherford scattering...is this correct? Number obtained scaling graphite by 30% as radius 30% larger
            self.d = 2.328 * self.eff_dens  # [g/cm3]
            self.i_max = 173.0  # eV => Mean exitation energy (dpgg)
            self.c_bar = (
                4.4351  # Energy loss in matter by heavy particles by Don Groom
            )
            self.x0 = (
                0.2014  # Energy loss in matter by heavy particles by Don Groom
            )
            self.x1 = (
                2.8715  # Energy loss in matter by heavy particles by Don Groom
            )
            self.a_small = (
                0.1492  # Energy loss in matter by heavy particles by Don Groom
            )
            self.k = (
                3.2546  # Energy loss in matter by heavy particles by Don Groom
            )
            self.delta0 = (
                0.14  # Energy loss in matter by heavy particles by Don Groom
            )
            self.rad_length = 9.37e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = (
                self.a / (avogadro * self.d * 30.16) * 1e24
            )  # [b] the numeric value is the nuclear collision length in cm from pdg
            self.cs_in_ref = (
                self.a / (avogadro * self.d * 46.52) * 1e24
            )  # [b] the numeric value is the nuclear interaction length in cm from pdg
            self.bnref = 124.14  # forward slope for coherent p-N scattering b_pN (eq.3.15)
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.034e-2  # copied from Aluminum...
        elif self.material == "Titanium":
            self.z = 22.0
            self.a = 47.9  # [g mol-1]
            self.r = 0.147  # This is used only for rutherford scattering
            self.d = 4.54 * self.eff_dens  # [g/cm3]
            self.i_max = 233.0  # eV => Mean exitation energy (dpgg)
            self.c_bar = (
                4.450  # Energy loss in matter by heavy particles by D Groom
            )
            self.x0 = (
                0.0957  # Energy loss in matter by heavy particles by Do D Groom
            )
            self.x1 = (
                3.0386  # Energy loss in matter by heavy particles by Do D Groom
            )
            self.a_small = (
                0.1566  # Energy loss in matter by heavy particles by Do D Groom
            )
            self.k = (
                3.0302  # Energy loss in matter by heavy particles by Do D Groom
            )
            self.delta0 = (
                0.12  # Energy loss in matter by heavy particles by Do D Groom
            )
            self.rad_length = 3.56e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = (
                self.a / (avogadro * self.d * 17.37) * 1e24
            )  # [b] the numeric value is the nuclear collision length in cm from pdg
            self.cs_in_ref = (
                self.a / (avogadro * self.d * 27.80) * 1e24
            )  # [b] the numeric value is the nuclear interaction length in cm from pdg
            self.bnref = 14.1 * self.a ** (
                0.65
            )  # forward slope for coherent p-N scattering b_pN (eq.3.15)
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.034e-2  # copied from Aluminum...not used
        elif self.material == "Iron":
            self.z = 26.0
            self.a = 55.847  # [g mol-1]
            self.r = 0.126  # This is used only for rutherford scattering
            self.d = 7.874 * self.eff_dens  # [g/cm3]
            self.i_max = 286.0  # eV => Mean exitation energy (dpgg)
            self.c_bar = (
                4.2911  # Energy loss in matter by heavy particles by D Groom
            )
            self.x0 = (
                -0.0012
            )  # Energy loss in matter by heavy particles by Do D Groom
            self.x1 = (
                3.1531  # Energy loss in matter by heavy particles by Do D Groom
            )
            self.a_small = 0.1468  # Ti Energy loss in matter by heavy particles by Do D Groom
            self.k = (
                2.9632  # Energy loss in matter by heavy particles by Do D Groom
            )
            self.delta0 = (
                0.12  # Energy loss in matter by heavy particles by Do D Groom
            )
            self.rad_length = 1.757e-2 / self.eff_dens  # [m]
            self.cs_tot_ref = (
                self.a / (avogadro * self.d * 10.37) * 1e24
            )  # [b] the numeric value is the nuclear collision length in cm from pdg
            self.cs_in_ref = (
                self.a / (avogadro * self.d * 16.77) * 1e24
            )  # [b] the numeric value is the nuclear interaction length in cm from pdg
            self.bnref = 14.1 * self.a ** (
                0.65
            )  # forward slope for coherent p-N scattering b_pN (eq.3.15)
            self.freep = self.freeco * self.a ** (
                1.0 / 3.0
            )  # number of nucleons in the external ring (3.14) -> n_eff
            self.cs_r = 0.034e-2  # copied from Aluminum...not used

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def get_points_hull(self):
        x1 = (self.s0, self.aper_u + self.unc_u)
        x2 = (self.s0 + self.length, self.aper_d + self.unc_d)
        x3 = (self.s0 + self.length, self.aper_d + self.unc_d + self.thickness)
        x4 = (self.s0, self.aper_u + self.unc_u + self.thickness)
        return np.array([x1, x2, x3, x4])

    def get_hull(self):
        points = self.get_points_hull()
        return ConvexHull(points)

    def get_inclination(self):
        a, b, c, d = self.get_points_hull()
        return (b[1] - a[1]) / self.length

    def get_points(self):
        x1_a = self.aper_u + self.unc_u
        x2_b = self.aper_d + self.unc_d
        x3_c = self.aper_d + self.unc_d + self.thickness
        x4_d = self.aper_u + self.unc_u + self.thickness
        return x1_a, x2_b, x3_c, x4_d

    def update_s0(self, s0_new):
        end_s = self.s0 + self.length
        self.s0 = s0_new
        sep_angle = self.get_inclination()
        self.aper_u += sep_angle * self.s0
        self.unc_u = 0.0
        temp_l = self.length
        self.length = end_s - self.s0
        self.kick *= self.length / temp_l

    def __str__(self):
        attrs = vars(self)
        return ", ".join("%s: %s" % item for item in attrs.items())


def get_cross_sections(proton, collimator):
    """
    Calculates relevant cross section for protons crossing collimator material
    @param proton: Object of class "Proton"
    @param collimator: Object of class "Collimator"
    @return: dictionary including all calculated cross sections:
             total -> Sum_i (sigma_i) sum of all relative cross sections
             ruth -> Rutherford cross section
             inelastic -> proton - Nucleus inelastic CS
             elastic_N -> proton - Nucleus elastic CS
             elastic_n -> proton - nucleon elastic CS
             SD -> proton - nucleon single diffractive CS
             tot_pN -> proton - Nucleus total CS, i.e. without considering Ruth scattering
    """
    en_CM_2 = (
        2.0 * proton.mass * proton.p0
    )  # energy available in the CM squared (s = 2*m_p*p)

    # eq.(3.12) -> s_pp^TOT = s_{pp, ref}^TOT*(p/p_ref)^pptco --> OLD!!!!! 41 - 2.33 * np.log(s) + 0.315 * np.log(s)**2 [mb]
    # pptot = collimator.pptref * (proton.mom_p / collimator.pref) ** collimator.pptco
    pptot = (
        41.084 - 2.3302 * np.log(en_CM_2) + 0.31514 * np.log(en_CM_2) ** 2
    ) * 1e-3

    # eq.(3.7) -> s_pp^el = s_{pp, ref}^el*(p/p_ref)^ppeco --> OLD!!!!! 11.7 - 1.59 * np.log(s) + 0.134 np.log(s)**2 [mb]
    # ppel = collimator.pperef * (proton.mom_p / collimator.pref) ** collimator.ppeco
    ppel = (11.7 - 1.59 * np.log(en_CM_2) + 0.134 * np.log(en_CM_2) ** 2) * 1e-3

    # eq.(3.10) -> sigma_pp^SD = A*ln(0.15*s) --> OLD!!!!! 4.3 + 0.3 np.log(s) [mb]
    # ppSD = collimator.SDcoe * np.log(0.15 * en_CM_2)
    ppSD = (4.3 + 0.3 * np.log(en_CM_2)) * 1e-3

    # eq.(3.15.1.a) -> s_pN^tot = s_{pN,ref}^tot+n_eff*(s_pp^tot-s_{pp,ref}^tot)
    CS_tot = collimator.cs_tot_ref + collimator.freep * (
        pptot - collimator.pptref
    )

    CS_inl = collimator.cs_in_ref * CS_tot / collimator.cs_tot_ref

    # eq.(3.12.1.a) -> s_pn^el = n_eff * s_pp^el
    CS_el = collimator.freep * ppel

    # eq.(3.12.1.b) -> s_pn^SD = n_eff * s_pp^SD
    CS_SD = collimator.freep * ppSD

    # s_pN^TOT = s_pN^inl + s_pN^el + (s_pn^el + s_pn^SD)
    CS_el_N = (CS_tot - CS_inl - CS_el) - CS_SD

    CS_ruth = ruth_six(collimator, proton)

    total_cross_section = CS_tot + CS_ruth

    # print total_cross_section

    cross_sections = dict(
        total=total_cross_section,
        ruth=CS_ruth,
        inelastic=CS_inl,
        elastic_N=CS_el_N,
        elastic_n=CS_el,
        SD=CS_SD,
        tot_pN=CS_tot,
    )
    return cross_sections


def which_interaction(cs):
    """
    Used to decide which interaction has to be considered of course using the given cross-sections
    @param cs: dictionary containing the values of point-like interaction cross-sections
    @return: string representing the chosen interaction
    """
    choice = np.random.rand()

    inelastic_frac = cs["inelastic"] / cs["total"]
    elastic_N_frac = cs["elastic_N"] / cs["total"]
    elastic_nn_frac = cs["elastic_n"] / cs["total"]
    sd_frac = cs["SD"] / cs["total"]
    ruth_frac = (
        1.0  # - (inelastic_frac + elastic_N_frac + elastic_nn_frac + sd_frac)
    )

    # rint 'SD: ', sd_frac
    # print 'ela', elastic_nn_frac
    # print 'inel: ', inelastic_frac
    # print 'elN: ', elastic_N_frac
    # print 'tot: ', inelastic_frac + elastic_N_frac + elastic_nn_frac + sd_frac + ruth_frac

    cs_names = ["inelastic", "elastic_N", "elastic_n", "SD", "ruth"]
    cs_frac = [
        inelastic_frac,
        elastic_N_frac,
        elastic_nn_frac,
        sd_frac,
        ruth_frac,
    ]

    prob_cs = [0]

    for i in range(0, len(cs_names) - 1):
        prob_cs.append(prob_cs[i] + cs_frac[i])
    prob_cs.append(1.0)

    # print choice, prob_cs

    # xk = (0, 1, 2, 3, 4)

    # custm = stats.rv_discrete(name='custm', values=(xk, cs_frac))
    # where = custm.rvs(size=1)
    where = len([1 for i in prob_cs if choice > i]) - 1
    # print where

    # print cs_names[where]

    return cs_names[where]


def read_ptc_track_out(file):
    """
    Read output file from PTC tracking - with this function it is possible to get the final
    distribution at the end of the line if no other observation points are called
    @param file: path of the PTC output file
    @return: the 6 particles coordinates divided in 6 lists
    """
    x = []
    px = []
    y = []
    py = []
    pt = []
    t = []
    s = []

    with open(file, "r") as fr:
        for line in fr:
            if re.search("end", line):
                break
        for line in fr:
            column = line.split()
            x.append(float(column[2]))
            px.append(float(column[3]))
            y.append(float(column[4]))
            py.append(float(column[5]))
            t.append(float(column[6]))
            pt.append(float(column[7]))
            s.append(float(column[8]))
    return x, px, y, py, t, pt, s


def write_init_distr(file, list):
    """
    Modify the MAD-X file for the next PTC tracking stage
    @param file:
    @param list: list of proton objects
    @return: it writes a initial_distribution.txt type file to be used as input for the next tracking
    """
    head = []

    with open(file, "r") as ff:
        for i in range(8):
            head.append(ff.readline())

    with open(file, "w") as fp:
        for i in range(len(head)):
            fp.write(head[i])
        for i in range(len(list)):
            line = (
                "            "
                + str(i + 1)
                + "        0"
                + "        "
                + "%.12f" % (list[i].x)
                + "    "
                + "%.12f" % (list[i].x_p)
                + "    "
                + "%.12f" % (list[i].y)
                + "    "
                + "%.12f" % (list[i].y_p)
                + "    "
                + "%.12f" % (list[i].t)
                + "    "
                + "%.12f" % (list[i].pt)
                + "    "
                + "0.000000000000"
                + "    "
                + "450.000978170460"
            )
            fp.write(line + "\n")


def get_impact_point_angle(collimator, proton):
    """
    In order to check if the trajectory of the argument particle intercepts the collimator jaw
    the 2 eq system to be solved is [x = x_coll ; x = x_0 + s_int * x'] if the displacement
    from the nominal orbit, i.e. x(s), and the conjugate momentum have same sign.
    Otherwise the system is [x = 2 * x_0 + x_coll ; x = x_0 + s * x']. So, looking for the
    solution of these systems, i.e. the common point between particle trajectory and transverse coordinate
    of the collimator jaw, one can calculate which is the impact point at the collimator, if it exists.

    The 2 eq system has to be solved for both positive and negative transverse coordinate. When an error of the
    jaw position is considered, it is needed to solve 4 systems of 2 eq each in order to cover all possible
    trajectories the particles can have.

    @param collimator: object of the class Collimator
    @param proton: object of the class Proton
    @return: updated particle coordinates
    """
    # print 'here: ', proton.flag, proton.x, proton.s, proton.x_p
    if not proton.flag == "absorbed":
        type_c = collimator.type
        if type_c == "h":
            jaw_pos = collimator.aper_x
            x = proton.x
            xp = proton.x_p
            m_p = (collimator.unc_p_d - collimator.unc_p_u) / collimator.length
            m_n = (collimator.unc_n_d - collimator.unc_n_u) / collimator.length

            # if x > 0.:
            #     if x < jaw_pos + collimator.unc_p_u:
            #         no_inside = True
            #     else:
            #         no_inside = False
            # else:
            #     if x > -jaw_pos + collimator.unc_n_u:
            #         no_inside = True
            #     else:
            #         no_inside = False
            check_flag(collimator, proton)
            # if no_inside:
            if proton.flag == "outside":
                if x > 0:
                    if xp < 0:
                        n = -jaw_pos + collimator.unc_n_u
                        s_int = (n - x) / (xp / proton.norm_mom - m_n)
                        if (
                            s_int > collimator.length + collimator.s0
                            or s_int < 0
                        ):
                            n = jaw_pos + collimator.unc_p_u
                            s_int = (n - x) / (xp / proton.norm_mom - m_p)
                    else:
                        n = jaw_pos + collimator.unc_p_u
                        s_int = (n - x) / (xp / proton.norm_mom - m_p)
                    if s_int > collimator.length + collimator.s0 or s_int < 0:
                        proton.track_drift(collimator.length)
                        proton.flag = "outside"
                    else:
                        proton.track_drift(s_int)
                        # print "track inside for l = l_coll - |s_int| -> %f" % s_int
                        proton.flag = "inside"
                else:
                    if xp > 0:
                        n = jaw_pos + collimator.unc_p_u
                        s_int = (n - x) / (xp / proton.norm_mom - m_p)
                        if (
                            s_int > collimator.length + collimator.s0
                            or s_int < 0
                        ):
                            n = -jaw_pos + collimator.unc_n_u
                            s_int = (n - x) / (xp / proton.norm_mom - m_n)
                    else:
                        n = -jaw_pos + collimator.unc_n_u
                        s_int = (n - x) / (xp / proton.norm_mom - m_n)
                    if s_int > collimator.length + collimator.s0 or s_int < 0:
                        proton.track_drift(collimator.length)
                        proton.flag = "outside"
                    else:
                        proton.track_drift(s_int)
                        # print "track inside for l = l_coll - |s_int| -> %f" % s_int
                        proton.flag = "inside"
            # elif no_inside and xp == 0.:
            #    proton.track_drift(collimator.length)
            else:
                # print "track inside jaw for l_coll"
                proton.flag = "inside"
        else:
            jaw_pos = collimator.aper_y
            y = proton.y
            yp = proton.y_p
            m_p = (collimator.unc_p_d - collimator.unc_p_u) / collimator.length
            m_n = (collimator.unc_n_d - collimator.unc_n_u) / collimator.length

            # if y > 0.:
            #     if y < jaw_pos + collimator.unc_p_u:
            #         no_inside = True
            #     else:
            #         no_inside = False
            # else:
            #     if y > -jaw_pos + collimator.unc_n_u:
            #         no_inside = True
            #     else:
            #         no_inside = False

            check_flag(collimator, proton)
            # if no_inside:
            if proton.flag == "outside":
                if y > 0:
                    if yp < 0:
                        n = -jaw_pos + collimator.unc_n_u
                        s_int = (n - y) / (yp / proton.norm_mom - m_n)
                        if (
                            s_int > collimator.length + collimator.s0
                            or s_int < 0
                        ):
                            n = jaw_pos + collimator.unc_p_u
                            s_int = (n - y) / (yp / proton.norm_mom - m_p)
                    else:
                        n = jaw_pos + collimator.unc_p_u
                        s_int = (n - y) / (yp / proton.norm_mom - m_p)
                    if s_int > collimator.length + collimator.s0 or s_int < 0:
                        proton.track_drift(collimator.length)
                        proton.flag = "outside"
                    else:
                        proton.track_drift(s_int)
                        # print "track inside for l = l_coll - |s_int| -> %f" % s_int
                        proton.flag = "inside"
                else:
                    if yp > 0:
                        n = jaw_pos + collimator.unc_p_u
                        s_int = (n - y) / (yp / proton.norm_mom - m_p)
                        if (
                            s_int > collimator.length + collimator.s0
                            or s_int < 0
                        ):
                            n = -jaw_pos + collimator.unc_n_u
                            s_int = (n - y) / (yp / proton.norm_mom - m_n)
                    else:
                        n = -jaw_pos + collimator.unc_n_u
                        s_int = (n - y) / (yp / proton.norm_mom - m_n)
                    if s_int > collimator.length + collimator.s0 or s_int < 0:
                        proton.track_drift(collimator.length)
                        proton.flag = "outside"
                    else:
                        proton.track_drift(s_int)
                        # print "track inside for l = l_coll - |s_int| -> %f" % s_int
                        proton.flag = "inside"
            # elif no_inside and yp == 0.:
            #    proton.track_drift(collimator.length)
            else:
                # print "track inside jaw for l_coll"
                proton.flag = "inside"


def get_impact_pos(septum, proton):
    """

    @param collimator: object of the class Collimator
    @param proton: object of the class Proton
    @return: updated particle coordinates
    """
    if not proton.flag == "absorbed":

        x = proton.x
        xp = proton.x_p

        # Points that represent the septum blade in the (s, x) space
        # D--------C
        # |        |
        # A--------B

        # TODO: Rewrite equation for region with field as function of s0

        a, b, c, d = septum.get_points_hull()

        sep_angle = septum.get_inclination()
        sep_kick = septum.kick * proton.p0 / proton.mom_p / septum.length

        check_flag_septum(septum, proton)
        # if no_inside:
        if proton.flag == "outside":
            if a[1] <= x <= d[1]:
                proton.flag = "inside"
            elif x < a[1]:
                # calculate lower hit
                s_int = (
                    a[1]
                    - x
                    - (septum.s0 * sep_angle)
                    + xp * septum.s0 / proton.norm_mom
                ) / (xp / proton.norm_mom - sep_angle)

                if septum.s0 <= s_int <= septum.s0 + septum.length:
                    proton.track_drift(s_int - septum.s0)
                    proton.flag = "inside"
                else:
                    proton.track_drift(septum.length)

            elif x > d[1]:
                # Calculate upper hit including field
                eq_a = 0.5 * sep_kick
                eq_b = xp / proton.norm_mom - sep_angle - septum.s0 * sep_kick
                eq_c = (
                    (x - d[1])
                    - (xp / proton.norm_mom - sep_angle) * septum.s0
                    + 0.5 * septum.s0 ** 2 * sep_kick
                )

                delta = eq_b ** 2 - 4 * eq_a * eq_c

                # Immaginary solutions, hence no impact with septum
                if delta < 0:
                    proton.track_in_field(septum, septum.length)

                # One solution, hence tangent with the septum
                elif delta == 0:
                    s_int = -1 * eq_b * 1 / (2 * eq_a)

                    if septum.s0 <= s_int <= septum.length + septum.s0:
                        proton.track_in_field(septum, s_int - septum.s0)
                        proton.flag = "inside"
                    else:
                        proton.track_in_field(septum, septum.length)

                # Two solutions, intersection. Check is this happened in the septum length
                else:

                    if sep_kick == 0:
                        s_int = -1 * eq_c / eq_b
                        isHit = septum.s0 <= s_int <= septum.length + septum.s0

                    else:
                        s_1 = (-1 * eq_b + np.sqrt(delta)) / (2 * eq_a)
                        s_2 = (-1 * eq_b - np.sqrt(delta)) / (2 * eq_a)

                        s_int, isHit = get_s_int(septum, s_1, s_2)

                    if isHit:
                        proton.track_in_field(septum, s_int - septum.s0)
                        proton.flag = "inside"
                    else:
                        proton.track_in_field(septum, septum.length)


def get_s_int(septum, s1, s2):

    isS1 = septum.s0 <= s1 <= septum.length + septum.s0
    isS2 = septum.s0 <= s2 <= septum.length + septum.s0

    min_s = min(s1, s2)

    if isS1 and isS2:
        return min_s, True
    elif isS1:
        return s1, True
    elif isS2:
        return s2, True
    else:
        return 0.0, False


def get_impact_point_rel(collimator, proton):
    """
    In order to check if the trajectory of the argument particle intercepts the collimator jaw
    the 2 eq system to be solved is [x = x_coll ; x = x_0 + s_int * x'] if the displacement
    from the nominal orbit, i.e. x(s), and the conjugate momentum have same sign.
    Otherwise the system is [x = 2 * x_0 + x_coll ; x = x_0 + s * x']. So, looking for the
    solution of these systems, i.e. the common point between particle trajectory and transverse coordinate
    of the collimator jaw, one can calculate which is the impact point at the collimator, if it exists.

    The 2 eq system has to be solved for both positive and negative transverse coordinate. When an error of the
    jaw position is considered, it is needed to solve 4 systems of 2 eq each in order to cover all possible
    trajectories the particles can have.

    @param collimator: object of the class Collimator
    @param proton: object of the class Proton
    @return: updated particle coordinates
    """
    type_c = collimator.type
    temp_l = collimator.length
    collimator.length = collimator.length + collimator.s0 - proton.s
    # print proton.flag
    if not proton.flag == "absorbed":
        if type_c == "h":
            jaw_pos = collimator.aper_x
            x = proton.x
            xp = proton.x_p
            m_p = (
                collimator.unc_p_d - collimator.unc_p_u
            ) / temp_l  # collimator.length
            m_n = (
                collimator.unc_n_d - collimator.unc_n_u
            ) / temp_l  # collimator.length
            # print 'proton:', proton.flag, proton.s, proton.x, proton.x_p
            # check_flag(collimator, proton)
            if proton.flag == "outside":
                if x > 0:
                    if xp < 0:
                        n = -jaw_pos + collimator.unc_n_u
                        s_int = (n - x) / (xp / proton.norm_mom - m_n)
                        if s_int > collimator.length or s_int < proton.s:
                            n = jaw_pos + collimator.unc_p_u
                            s_int = (n - x) / (xp / proton.norm_mom - m_p)
                            # print s_int
                    else:
                        n = jaw_pos + collimator.unc_p_u
                        s_int = (n - x) / (xp / proton.norm_mom - m_p)
                        # print m_p, n
                    if s_int > collimator.length or s_int < proton.s:
                        proton.track_drift(collimator.length)
                        proton.flag = "outside"
                    else:
                        proton.track_drift(s_int)
                        # print "track inside for l = l_coll - |s_int| -> %f" % s_int
                        proton.flag = "inside"
                else:
                    if xp > 0:
                        n = jaw_pos + collimator.unc_p_u
                        s_int = (n - x) / (xp / proton.norm_mom - m_p)
                        if s_int > collimator.length or s_int < proton.s:
                            n = -jaw_pos + collimator.unc_n_u
                            s_int = (n - x) / (xp / proton.norm_mom - m_n)
                    else:
                        n = -jaw_pos + collimator.unc_n_u
                        s_int = (n - x) / (xp / proton.norm_mom - m_n)
                    if s_int > collimator.length or s_int < proton.s:
                        proton.track_drift(collimator.length)
                        proton.flag = "outside"
                    else:
                        proton.track_drift(s_int)
                        # print "track inside for l = l_coll - |s_int| -> %f" % s_int
                        proton.flag = "inside"
            # elif no_inside and xp == 0.:
            #    proton.track_drift(collimator.length)
            else:
                # print "track inside jaw for l_coll"
                proton.flag = "inside"
        else:
            jaw_pos = collimator.aper_y
            y = proton.y
            yp = proton.y_p
            m_p = (
                collimator.unc_p_d - collimator.unc_p_u
            ) / temp_l  # collimator.length
            m_n = (
                collimator.unc_n_d - collimator.unc_n_u
            ) / temp_l  # collimator.length

            # check_flag(collimator, proton)
            if proton.flag == "outside":
                if y > 0:
                    if yp < 0:
                        n = -jaw_pos + collimator.unc_n_u
                        s_int = (n - y) / (yp / proton.norm_mom - m_n)
                        if s_int > collimator.length or s_int < proton.s:
                            n = jaw_pos + collimator.unc_p_u
                            s_int = (n - y) / (yp / proton.norm_mom - m_p)
                    else:
                        n = jaw_pos + collimator.unc_p_u
                        s_int = (n - y) / (yp / proton.norm_mom - m_p)
                    if s_int > collimator.length or s_int < proton.s:
                        proton.track_drift(collimator.length)
                        proton.flag = "outside"
                    else:
                        proton.track_drift(s_int)
                        # print "track inside for l = l_coll - |s_int| -> %f" % s_int
                        proton.flag = "inside"
                else:
                    if yp > 0:
                        n = jaw_pos + collimator.unc_p_u
                        s_int = (n - y) / (yp / proton.norm_mom - m_p)
                        if s_int > collimator.length or s_int < proton.s:
                            n = -jaw_pos + collimator.unc_n_u
                            s_int = (n - y) / (yp / proton.norm_mom - m_n)
                    else:
                        n = -jaw_pos + collimator.unc_n_u
                        s_int = (n - y) / (yp / proton.norm_mom - m_n)
                    if s_int > collimator.length or s_int < proton.s:
                        proton.track_drift(collimator.length)
                        proton.flag = "outside"
                    else:
                        proton.track_drift(s_int)
                        # print "track inside for l = l_coll - |s_int| -> %f" % s_int
                        proton.flag = "inside"
            # elif no_inside and yp == 0.:
            #    proton.track_drift(collimator.length)
            else:
                # print "track inside jaw for l_coll"
                proton.flag = "inside"
    collimator.length = temp_l


def bethe(collimator, proton, step_3d):
    """
    Calculates the energy loss due to ionization
    @param collimator: object of collimator class
    @param proton: object of Proton class
    @param step_3d: length og the path done inside the material in 3D [m]
    @return: the energy lost by the particle due to ionisation [GeV]
    """
    m_e = 0.511
    mp = proton.mass
    z = collimator.z
    a = collimator.a
    b = proton.beta
    i = (z ** 0.9) * 10.0
    d = collimator.d
    x1 = collimator.x1
    c_bar = collimator.c_bar
    a_par = collimator.a_small
    k_par = collimator.k
    delta_0 = collimator.delta0
    x0 = collimator.x0

    g = 1.0 / np.sqrt(1.0 - b ** 2)

    t = (
        1e6
        * (2.0 * m_e * b ** 2 * g ** 2)
        / (1.0 + (2.0 * g * m_e / mp) + (m_e / mp) ** 2)
    )
    arg = 2.0 * m_e * 1e6 * b ** 2 * g ** 2 * t / i ** 2
    x_delta = np.log10(b * g)
    if x_delta >= x1:
        delta = 2 * np.log(10.0) * x_delta - c_bar
    elif x0 <= x_delta < x1:
        delta = (
            2 * np.log(10.0) * x_delta - c_bar + a_par * (x1 - x_delta) ** k_par
        )
    elif x_delta > x0:
        delta = delta_0 * 1e2 * (x_delta - x0)
    else:
        delta = 0.0
    de = (
        0.307075
        * (z / a)
        * (1 / (b ** 2))
        * (np.log(arg) / 2.0 - (b ** 2) - (delta / 2.0))
    )
    de = de * d * 1e2 * 1e-3 * step_3d  # energy lost in the path in [GeV]
    return de


def mcs_old(proton, collimator, step_s):
    """
    Calculates the new particle transverse position and transverse momentum due to MCS while
    traversing a length=step inside the collimator
    @param proton: object of proton class
    @param collimator: object of collimator class
    @param step_s: length of material to be crossed - s coordinate [m]
    @return: updated position and angle of given particle due to MCS
    """
    theta_0 = (
        13.6e-3
        / (proton.beta * proton.mom_p)
        * 1.0
        / np.sqrt(collimator.rad_length)
        * (1.0 + 0.038 * np.log(step_s / collimator.rad_length))
    )
    n1 = np.random.randn()
    n2 = np.random.randn()
    proton.x += step_s * proton.x_p + theta_0 * step_s / 2 * (
        n1 / np.sqrt(3.0) + n2
    )
    proton.x_p += theta_0 * n2 * proton.norm_mom
    ny1 = np.random.randn()
    ny2 = np.random.randn()
    proton.y += step_s * proton.y_p + theta_0 * step_s / 2 * (
        ny1 / np.sqrt(3.0) + ny2
    )
    proton.y_p += theta_0 * ny2 * proton.norm_mom
    # proton.s += step_s


def mcs(proton, collimator, step_s):
    """
    Calculates the new particle transverse position and transverse momentum due to MCS while
    traversing a length=step inside the collimator
    @param proton: object of proton class
    @param collimator: object of collimator class
    @param step_s: length of material to be crossed - s coordinate [m]
    @return: updated position and angle of given particle due to MCS
    """
    theta_0 = (
        13.6e-3 / (proton.beta * proton.mom_p) / np.sqrt(collimator.rad_length)
    )

    theta = (
        theta_0
        * np.sqrt(step_s)
        * (1.0 + 0.038 * np.log(step_s / collimator.rad_length))
    )
    n1 = np.random.randn()
    n2 = np.random.randn()
    proton.x += step_s * proton.x_p + step_s * theta * (
        n1 / np.sqrt(12.0) + 0.5 * n2
    )
    proton.x_p += theta * n2
    ny1 = np.random.randn()
    ny2 = np.random.randn()
    proton.y += step_s * proton.y_p + theta * step_s * (
        ny1 / np.sqrt(12.0) + 0.5 * ny2
    )
    proton.y_p += theta * ny2
    # proton.s += step_s


def mcs_bk(proton, collimator, step_s):
    """
    Calculates the new particle transverse position and transverse momentum due to MCS while
    traversing a length=step inside the collimator
    @param proton: object of proton class
    @param collimator: object of collimator class
    @param step_s: length of material to be crossed - s coordinate [m]
    @return: updated position and angle of given particle due to MCS
    """
    theta_0 = (
        13.6e-3
        / (proton.mom_p)
        / np.sqrt(collimator.rad_length * 10)
        * (1.0 + 0.038 * np.log(step_s / collimator.rad_length))
    )

    theta = theta_0 * np.sqrt(step_s)
    n1 = np.random.randn()
    n2 = np.random.randn()

    proton.x = (
        proton.x
        + step_s * proton.x_p
        + n1 * step_s * theta * (n1 / np.sqrt(12.0) + 0.5 * n2)
    )
    proton.x_p += theta * n2
    ny1 = np.random.randn()
    ny2 = np.random.randn()
    proton.y = (
        proton.y
        + step_s * proton.y_p
        + n1 * step_s * theta * (ny1 / np.sqrt(12.0) + 0.5 * ny2)
    )
    proton.y_p += theta * ny2
    # proton.s += step_s


def mcs_k2(proton, collimator, step_s):
    """
    Calculates the new particle transverse position and transverse momentum due to MCS while
    traversing a length=step inside the collimator
    @param proton: object of proton class
    @param collimator: object of collimator class
    @param step_s: length of material to be crossed - s coordinate [m]
    @return: updated position and angle of given particle due to MCS
    """
    theta_0 = (
        13.6e-3
        / (proton.beta * proton.mom_p)
        * 1.0
        / np.sqrt(collimator.rad_length)
    )
    n1 = np.random.randn()
    n2 = np.random.randn()
    proton.x += step_s * proton.x_p + theta_0 * np.sqrt(step_s) / 2 * (
        n1 / np.sqrt(3.0) + n2
    )
    proton.x_p += theta_0 * np.sqrt(step_s) * n2 * proton.norm_mom
    ny1 = np.random.randn()
    ny2 = np.random.randn()
    proton.y += step_s * proton.y_p + theta_0 * np.sqrt(step_s) / 2 * (
        ny1 / np.sqrt(3.0) + ny2
    )
    proton.y_p += theta_0 * np.sqrt(step_s) * ny2 * proton.norm_mom
    # proton.s += step_s


def mcs_test(proton, collimator, step_s):
    """
    Calculates the new particle transverse position and transverse momentum due to MCS while
    traversing a length=step inside the collimator
    @param proton: object of proton class
    @param collimator: object of collimator class
    @param step_s: length of material to be crossed - s coordinate [m]
    @return: updated position and angle of given particle due to MCS
    """
    theta_0 = (
        13.6e-3
        / (proton.beta * proton.mom_p)
        * 1.0
        / np.sqrt(collimator.rad_length)
    )
    n1 = np.random.randn()
    n2 = np.random.randn()
    x_new = step_s * proton.x_p + theta_0 * step_s * np.sqrt(step_s) / 2 * (
        n1 / np.sqrt(3.0) + n2
    )
    x_p_new = theta_0 * np.sqrt(step_s) * n2  # angle
    # print 'theta_min = ', theta_0 * np.sqrt(step_s)
    return x_new, x_p_new


def ruth_jack(collimator, proton, t, *args):
    hbar = 1.05443e-27
    eesu = 4.80286e-10
    amu = 1.65979e-24
    abhor = 5.29172e-9
    ap = 1.007593

    axmax = 1.4 * abhor * collimator.z ** -0.3333
    axmin = 1.4e-13 * collimator.a ** 0.33333
    n_tot = collimator.d / (collimator.a * amu)
    p_cgs = proton.gamma * proton.velocity * 1e2 * ap * amu
    thmin = hbar / (p_cgs * axmax)
    thmax = hbar / (p_cgs * axmin)
    thmax = np.pi
    # thmin = 2.235 * 13.6e-3 / (proton.beta * proton.mom_p) * np.sqrt(t) / np.sqrt(collimator.rad_length * 1e2)

    th2s = 2.0 * thmin ** 2 * np.log(thmax / thmin)
    coeff = 2.0 * collimator.z * eesu ** 2 / (hbar * proton.velocity * 1e2)
    sig_tot = np.pi * axmax ** 2 * coeff ** 2

    ncoll = n_tot * t * sig_tot

    th2tot = ncoll * th2s

    if thmin < (2.0 * np.sqrt(th2tot)):
        thmin = 2.0 * np.sqrt(th2tot)

    coeff2 = collimator.z * eesu ** 2 / (p_cgs * proton.velocity * 1e2)
    smin = np.sin(thmin / 2.0)
    smax = np.sin(thmax / 2.0)
    rcross = 1.0e24 * coeff2 ** 2 * np.pi * (1.0 / smin ** 2 - 1.0 / smax ** 2)

    if len(args) != 0:
        # thmin *= 1.4
        # print 'theta_min = ', thmin
        if thmin > thmax:
            print("theta_min > theta_max!!")
            return rcross
        probrp = np.random.rand()
        probxy = 2.0 * np.pi * np.random.rand()
        denom1 = 1.0 / smin ** 2
        denom2 = 1.0 / smax ** 2
        denom = denom1 - denom2
        th = (thmin + thmax) / 2.0
        for i in range(0, 32):
            sinth = np.sin(th / 2.0)
            prob = (1.0 / sinth ** 2 - denom2) / denom
            if prob > probrp:
                thmin = th
            if prob < probrp:
                thmax = th
            th = (thmin + thmax) / 2.0
        thx = th * np.cos(probxy)
        thy = th * np.sin(probxy)
        return rcross, thx, thy
    return rcross


def ruth_six(collimator, proton, *args):
    # gamma = 0.5772157
    alpha = 1.0 / 137.0
    hbar_convert = 0.3893858
    theta_0 = 13.6e-3 / (proton.beta * proton.mom_p)
    # const = np.pi * collimator.z**2 * alpha**2 * hbar_convert * proton.e_tot**2 / (2. * proton.mom_p**4)
    const = 4 * np.pi * collimator.z ** 2 * alpha ** 2 * hbar_convert
    theta_cut = 2.325 * theta_0
    # sn = np.sin(theta_cut / 2.)
    # t_cut = 4. * proton.mom_p**2 * sn**2
    t_cut = theta_cut ** 2 * proton.mom_p ** 2
    t_up = np.pi ** 2 * proton.mom_p ** 2
    # r = 1.3e-1 * collimator.a**(1. / 3.)
    # fac1 = 1. / 3. * (r / 0.197)**2
    # a_cut = fac1 * t_cut
    # cnorm = np.exp(-a_cut) / a_cut + gamma + np.log(a_cut)

    f_x = lambda x: const * np.exp(-0.856e3 * collimator.r ** 2 * x) / x ** 2
    integg, err = quad(f_x, t_cut, 0.02)
    # integg, err = quad(f_x, 0.0009982, 0.02)
    rcross = integg / 1000.0
    # rcross = collimator.cs_r
    if len(args) != 0:
        # thmin *= 1.4
        # print 'theta_min = ', thmin
        probrp = np.random.rand()
        probxy = 2.0 * np.pi * np.random.rand()
        smin = np.sin(theta_cut / 2.0)
        smax = np.sin(1.0 / 2.0)
        denom1 = 1.0 / smin ** 2
        denom2 = 1.0 / smax ** 2
        denom = denom1 - denom2
        thmax = 1.0
        th = (theta_cut + thmax) / 2.0
        for i in range(0, 32):
            sinth = np.sin(th / 2.0)
            prob = (1.0 / sinth ** 2 - denom2) / denom
            if prob > probrp:
                theta_cut = th
            if prob < probrp:
                thmax = th
            th = (theta_cut + thmax) / 2.0
        thx = th * np.cos(probxy)
        thy = th * np.sin(probxy)
        return rcross, thx, thy
    return rcross


def ruth_k2(collimator, proton):
    theta_0 = 13.6e-3 / (proton.beta * proton.mom_p)
    theta_cut = 2.325 * theta_0
    theta_max = np.pi
    alfa = 1.0 / 137.0
    coeff = collimator.z * alfa / (2.0 * proton.mom_p)
    rcross = (
        np.pi
        * coeff ** 2
        * (
            (1.0 / np.sin(theta_cut / 2.0)) ** 2
            - (1.0 / np.sin(theta_max / 2.0)) ** 2
        )
    )
    return rcross * 0.389 * 1e-3  # [barn]


def get_step_size(collimator, cross_sections):
    """
    Calculates a random step size starting from jaw material and total cross-section knowledge
    @param collimator: Collimator object
    @param cross_sections: Cross-sections dictionary
    """
    tot_cross = cross_sections["total"]
    # A is in [g/mol], Na is in [mol^-1], d is in [g/cm^3], tot_cross is in [barn] -> mean_free_path is in [m]!!
    mean_free_path = (
        collimator.a / (6.02214129e23 * collimator.d * tot_cross * 1e-24) * 1e-2
    )
    step = -mean_free_path * np.log(np.random.rand())
    # print step
    return step


def check_flag(collimator, proton):
    """
    Check if the particle is inside or outside collimator jaws
    @param collimator: Collimator object
    @param proton: Proton object
    """
    if proton.flag != "absorbed":
        if proton.s - collimator.s0 >= collimator.length:
            proton.flag = "outside"
        elif collimator.type == "h":
            m = (collimator.unc_p_d - collimator.unc_p_u) / collimator.length
            n = collimator.aper_x + collimator.unc_p_u - m * collimator.s0
            m_n = (collimator.unc_n_d - collimator.unc_n_u) / collimator.length
            n_n = -collimator.aper_x + collimator.unc_n_u - m_n * collimator.s0

            x_coll_p = m * proton.s + n
            x_coll_n = m_n * proton.s + n_n
            s_limit_0 = collimator.s0
            s_limit_end = collimator.s0 + collimator.length

            if (
                x_coll_p > proton.x > x_coll_n
                and s_limit_end >= proton.s >= s_limit_0
            ):
                proton.flag = "outside"
            else:
                proton.flag = "inside"

        elif collimator.type == "v":
            m = (collimator.unc_p_d - collimator.unc_p_u) / collimator.length
            n = collimator.aper_y + collimator.unc_p_u - m * collimator.s0

            m_n = (collimator.unc_n_d - collimator.unc_n_u) / collimator.length
            n_n = -collimator.aper_y + collimator.unc_n_u - m_n * collimator.s0

            y_coll_p = m * proton.s + n
            y_coll_n = m_n * proton.s + n_n
            s_limit_0 = collimator.s0
            s_limit_end = collimator.s0 + collimator.length

            if (
                y_coll_p > proton.y > y_coll_n
                and s_limit_end >= proton.s >= s_limit_0
            ):
                proton.flag = "outside"
            else:
                proton.flag = "inside"


def point_in_hull(point, hull, tolerance=1e-12):

    return all(
        (np.dot(eq[:-1], point) + eq[-1] <= tolerance) for eq in hull.equations
    )


def check_flag_septum(septum, proton):
    """
    Check if the particle is inside or outside collimator jaws
    @param collimator: Collimator object
    @param proton: Proton object
    """

    septum_points = septum.get_points_hull()
    septum_hull = septum.get_hull()

    if proton.flag != "absorbed":
        particleIsInside = point_in_hull((proton.s, proton.x), septum_hull)
        proton.flag = "inside" if particleIsInside else "outside"

    return septum_hull


def check_mcs_step(collimator, proton, step_in):
    """
    From K2 scattering routine. It is basically treatment of edge effect inside the collimator jaws
    @param collimator: Collimator object
    @param proton: Proton object
    @param step_in: Random step chosen for this iteration
    """

    theta_0 = (
        13.6e-3
        / (proton.beta * proton.mom_p)
        * np.sqrt(step_in / collimator.rad_length)
        * (1.0 + 0.038 * np.log(step_in / collimator.rad_length))
    )
    if collimator.type == "h":
        if proton.x > 0:
            angle = np.arctan(
                (collimator.unc_p_d - collimator.unc_p_u) / collimator.length
            )
            if angle != 0.0:
                x_rel = proton.x - collimator.aper_x
                x_temp = np.sin(angle) * x_rel
            else:
                x_temp = proton.x - collimator.aper_x
            a = 9 * theta_0 ** 2 / 3.0
            b = -1 * proton.x_p ** 2
            c = -2 * proton.x_p * x_temp
            d = -(x_temp ** 2)
            # print 'a', a, 'b', b, 'c', c, 'd', d
            # test = np.linspace(-10, 10, 100)
            # test_y = a * test ** 3 + b * test ** 2 + test * c + d
            # plt.plot(test, test_y, 'lime')
            sol = np.roots([a, b, c, d])
            # print sol
            sol_re = [100000.0]
            for i in sol:
                if np.imag(i) == 0:
                    sol_re.append(float(np.real(i)))
            min_sol = min(sol_re)
            if min_sol < step_in:
                if min_sol < collimator.rad_length * 0.1e-2:
                    return collimator.rad_length * 0.1e-2
                else:
                    return min(sol_re)
            else:
                return step_in
        else:
            angle = np.arctan(
                (collimator.unc_n_d - collimator.unc_n_u) / collimator.length
            )
            if angle != 0.0:
                x_rel = proton.x + collimator.aper_x
                x_temp = np.sin(angle) * x_rel
            else:
                x_temp = proton.x + collimator.aper_x
            a = 9 * theta_0 ** 2 / 3.0
            b = -1 * proton.x_p ** 2
            c = -2 * proton.x_p * x_temp
            d = -(x_temp ** 2)
            # print 'a', a, 'b', b, 'c', c, 'd', d
            # test = np.linspace(-10, 10, 100)
            # test_y = a * test ** 3 + b * test ** 2 + test * c + d
            # plt.plot(test, test_y, 'lime')
            sol = np.roots([a, b, c, d])
            # print sol
            sol_re = [100000.0]
            for i in sol:
                if np.imag(i) == 0:
                    sol_re.append(float(np.real(i)))
            min_sol = min(sol_re)
            if min_sol < step_in:
                if min_sol < collimator.rad_length * 0.1e-2:
                    return collimator.rad_length * 0.1e-2
                else:
                    return min(sol_re)
            else:
                return step_in
    else:
        if proton.y > 0:
            angle = np.arctan(
                (collimator.unc_p_d - collimator.unc_p_u) / collimator.length
            )
            if angle != 0.0:
                y_rel = proton.y - collimator.aper_y
                y_temp = np.sin(angle) * y_rel
            else:
                y_temp = proton.y - collimator.aper_y
            a = 9 * theta_0 ** 2 / 3.0
            b = -1 * proton.x_p ** 2
            c = -2 * proton.x_p * y_temp
            d = -1 * y_temp ** 2
            # print 'a', a, 'b', b, 'c', c, 'd', d
            # test = np.linspace(-10, 10, 100)
            # test_y = a * test ** 3 + b * test ** 2 + test * c + d
            # plt.plot(test, test_y, 'lime')
            sol = np.roots([a, b, c, d])
            # print sol
            sol_re = [100000.0]
            for i in sol:
                if np.imag(i) == 0:
                    sol_re.append(float(np.real(i)))
            min_sol = min(sol_re)
            if min_sol < step_in:
                if min_sol < collimator.rad_length * 0.1e-2:
                    return collimator.rad_length * 0.1e-2
                else:
                    return min(sol_re)
            else:
                return step_in
        else:
            angle = np.arctan(
                (collimator.unc_n_d - collimator.unc_n_u) / collimator.length
            )
            if angle != 0.0:
                y_rel = proton.y + collimator.aper_y
                y_temp = np.sin(angle) * y_rel
            else:
                y_temp = proton.y + collimator.aper_y
            a = 9 * theta_0 ** 2 / 3.0
            b = -1 * proton.x_p ** 2
            c = -2 * proton.x_p * y_temp
            d = -1 * y_temp ** 2
            # print 'a', a, 'b', b, 'c', c, 'd', d
            # test = np.linspace(-10, 10, 100)
            # test_y = a * test ** 3 + b * test ** 2 + test * c + d
            # plt.plot(test, test_y, 'lime')
            sol = np.roots([a, b, c, d])
            # print sol
            sol_re = [100000.0]
            for i in sol:
                if np.imag(i) == 0:
                    sol_re.append(float(np.real(i)))
            min_sol = min(sol_re)
            if min_sol < step_in:
                if min_sol < collimator.rad_length * 0.1e-2:
                    return collimator.rad_length * 0.1e-2
                else:
                    return min(sol_re)
            else:
                return step_in


def check_mcs_step_septum(septum, proton, step_in):
    """
    From K2 scattering routine. It is basically treatment of edge effect inside the septum jaws
    @param septum: septum object
    @param proton: Proton object
    @param step_in: Random step chosen for this iteration
    """

    theta_0 = (
        13.6e-3
        / (proton.mom_p)
        / np.sqrt(septum.rad_length)
        * np.sqrt(step_in)
        * (1.0 + 0.038 * np.log(step_in / septum.rad_length))
    )

    x_temp = (
        proton.x - septum.aper_u
        if proton.x <= (septum.aper_u + septum.thickness / 2.0)
        else proton.x - septum.aper_u - septum.thickness
    )

    a = 9 * theta_0 ** 2 / 3.0
    b = -1 * proton.x_p ** 2
    c = -2 * proton.x_p * x_temp
    d = -(x_temp ** 2)
    # print 'a', a, 'b', b, 'c', c, 'd', d
    # test = np.linspace(-10, 10, 100)
    # test_y = a * test ** 3 + b * test ** 2 + test * c + d
    # plt.plot(test, test_y, 'lime')
    sol = np.roots([a, b, c, d])
    # print sol
    sol_re = [100000.0]
    for i in sol:
        if np.imag(i) == 0:
            sol_re.append(float(np.real(i)))
    min_sol = min(sol_re)
    if min_sol < step_in:
        if min_sol < septum.rad_length * 0.1e-2:
            return septum.rad_length * 0.1e-2
        else:
            return min(sol_re)
    else:
        return step_in


def contin_processes(collimator, proton, step_in, rem_s):
    """
    Treatment of continuous processes, i.e. MCS and ionization.
    @param collimator: Collimator object
    @param proton: Proton object
    @param step_in: Random step
    @param rem_s: Residual collimator length s-projection
    @return : updated residual collimator length and its s-projection
    """
    mcs(proton, collimator, step_in)
    de = -1 * bethe(collimator, proton, step_in)
    proton.update_energy(de)
    proton.s += step_in / proton.mom_dir_motion()
    if proton.mom_p < 0.02:
        proton.flag = "absorbed"
        # print 'absorbed - cont'
        rem_l_temp = 0.0
        rem_s_temp = 0.0
    else:
        check_flag(collimator, proton)
        rem_s_temp = rem_s - (step_in / proton.mom_dir_motion())
        rem_l_temp = rem_s_temp * proton.mom_dir_motion()

    return rem_s_temp, rem_l_temp


def contin_processes_septum(septum, proton, step_in, rem_s):
    """
    Treatment of continuous processes, i.e. MCS and ionization.
    @param septum: septum object
    @param proton: Proton object
    @param step_in: Random step
    @param rem_s: Residual septum length s-projection
    @return : updated residual septum length and its s-projection
    """
    mcs(proton, septum, step_in)
    de = -1 * bethe(septum, proton, step_in)
    proton.update_energy(de)
    proton.s += step_in / proton.mom_dir_motion()
    if proton.mom_p < 0.02:
        proton.flag = "absorbed"
        # print 'absorbed - cont'
        rem_l_temp = 0.0
        rem_s_temp = 0.0
    else:
        check_flag_septum(septum, proton)
        rem_s_temp = rem_s - (step_in / proton.mom_dir_motion())
        rem_l_temp = rem_s_temp * proton.mom_dir_motion()

    return rem_s_temp, rem_l_temp


def write_files(p_list, file_survived, file_absorbed, s_ini, collimator):
    file_head = "initial_distribution_1.txt"
    head = []
    with open(file_head, "r") as ff:
        for i in range(6):
            head.append(ff.readline())

    x_sur = [ppp.x for ppp in p_list if ppp.flag == "outside"]
    px_sur = [ppp.x_p for ppp in p_list if ppp.flag == "outside"]

    y_sur = [ppp.y for ppp in p_list if ppp.flag == "outside"]
    py_sur = [ppp.y_p for ppp in p_list if ppp.flag == "outside"]

    t_sur = [ppp.t for ppp in p_list if ppp.flag == "outside"]
    pt_sur = [ppp.pt for ppp in p_list if ppp.flag == "outside"]

    s_sur = [ppp.s + s_ini for ppp in p_list if ppp.flag == "outside"]

    normal = PrettyTable(
        ["*", "NUMBER", "TURN", "X", "PX", "Y", "PY", "T", "PT", "S", "E"]
    )
    normal.align = "r"
    normal.border = False
    normal.add_row(
        [
            "$",
            "%d",
            "%d",
            "%le",
            "%le",
            "%le",
            "%le",
            "%le",
            "%le",
            "%le",
            "%le",
        ]
    )

    with open(file_survived, "w") as fp:
        for i in range(len(head)):
            fp.write(head[i])
        for i in range(len(x_sur)):
            normal.add_row(
                [
                    " ",
                    i + 1,
                    0,
                    x_sur[i],
                    px_sur[i],
                    y_sur[i],
                    py_sur[i],
                    t_sur[i],
                    pt_sur[i],
                    s_sur[i],
                    450.000978170460,
                ]
            )
        fp.write(normal.get_string())

    x_abs = [ppp.x for ppp in p_list if ppp.flag == "absorbed"]
    px_abs = [ppp.x_p for ppp in p_list if ppp.flag == "absorbed"]

    y_abs = [ppp.y for ppp in p_list if ppp.flag == "absorbed"]
    py_abs = [ppp.y_p for ppp in p_list if ppp.flag == "absorbed"]

    mom_abs = []
    for ppp in p_list:
        if ppp.flag == "absorbed":
            if ppp.mom_p <= 0.02:
                mom_abs.append(0.02)
            else:
                mom_abs.append(ppp.mom_p)

    s_abs = [ppp.s + s_ini for ppp in p_list if ppp.flag == "absorbed"]

    abs_table = PrettyTable(["X", "PX", "Y", "PY", "S", "P[GeV]"])
    abs_table.align = "r"
    abs_table.border = False

    # abs_table.add_column("X", x_abs)
    # abs_table.add_column("PX", px_abs)
    # abs_table.add_column("Y", y_abs)
    # abs_table.add_column("PY", py_abs)
    # abs_table.add_column("S", s_abs)
    # abs_table.add_column("P[GeV]", mom_abs)

    for i in range(len(x_abs)):
        abs_table.add_row(
            [x_abs[i], px_abs[i], y_abs[i], py_abs[i], s_abs[i], mom_abs[i]]
        )

    with open(file_absorbed, "w") as fp:
        fp.write(collimator.name + "\n")
        if len(s_abs) == 0:
            fp.write("X    PX    Y    PY    S    P[GeV]")
        else:
            fp.write(abs_table.get_string())


def write_files_rot(p_list, file_survived, file_absorbed, s_ini, collimator):
    file_head = "initial_distribution_1.txt"
    head = []
    with open(file_head, "r") as ff:
        for i in range(6):
            head.append(ff.readline())

    x_sur0 = [ppp.x for ppp in p_list if ppp.flag == "outside"]
    px_sur0 = [ppp.x_p for ppp in p_list if ppp.flag == "outside"]

    y_sur0 = [ppp.y for ppp in p_list if ppp.flag == "outside"]
    py_sur0 = [ppp.y_p for ppp in p_list if ppp.flag == "outside"]

    t_sur = [ppp.t for ppp in p_list if ppp.flag == "outside"]
    pt_sur = [ppp.pt for ppp in p_list if ppp.flag == "outside"]

    s_sur = [ppp.s + s_ini for ppp in p_list if ppp.flag == "outside"]

    x_sur = np.array(x_sur0) * cos(-1 * collimator.angle) + np.array(
        y_sur0
    ) * sin(-1 * collimator.angle)
    px_sur = np.array(px_sur0) * cos(-1 * collimator.angle) + np.array(
        py_sur0
    ) * sin(-1 * collimator.angle)

    y_sur = -1 * np.array(x_sur0) * sin(-1 * collimator.angle) + np.array(
        y_sur0
    ) * cos(-1 * collimator.angle)
    py_sur = -1 * np.array(px_sur0) * sin(-1 * collimator.angle) + np.array(
        py_sur0
    ) * cos(-1 * collimator.angle)

    normal = PrettyTable(
        ["*", "NUMBER", "TURN", "X", "PX", "Y", "PY", "T", "PT", "S", "E"]
    )
    normal.align = "r"
    normal.border = False
    normal.add_row(
        [
            "$",
            "%d",
            "%d",
            "%le",
            "%le",
            "%le",
            "%le",
            "%le",
            "%le",
            "%le",
            "%le",
        ]
    )

    with open(file_survived, "w") as fp:
        for i in range(len(head)):
            fp.write(head[i])
        for i in range(len(x_sur)):
            normal.add_row(
                [
                    " ",
                    i + 1,
                    0,
                    x_sur[i],
                    px_sur[i],
                    y_sur[i],
                    py_sur[i],
                    t_sur[i],
                    pt_sur[i],
                    s_sur[i],
                    450.000978170460,
                ]
            )
        fp.write(normal.get_string())

    x_abs0 = [ppp.x for ppp in p_list if ppp.flag == "absorbed"]
    px_abs0 = [ppp.x_p for ppp in p_list if ppp.flag == "absorbed"]

    y_abs0 = [ppp.y for ppp in p_list if ppp.flag == "absorbed"]
    py_abs0 = [ppp.y_p for ppp in p_list if ppp.flag == "absorbed"]

    x_abs = np.array(x_abs0) * cos(-1 * collimator.angle) + np.array(
        y_abs0
    ) * sin(-1 * collimator.angle)
    px_abs = np.array(px_abs0) * cos(-1 * collimator.angle) + np.array(
        py_abs0
    ) * sin(-1 * collimator.angle)

    y_abs = -1 * np.array(x_abs0) * sin(-1 * collimator.angle) + np.array(
        y_abs0
    ) * cos(-1 * collimator.angle)
    py_abs = -1 * np.array(px_abs0) * sin(-1 * collimator.angle) + np.array(
        py_abs0
    ) * cos(-1 * collimator.angle)

    mom_abs = []
    for ppp in p_list:
        if ppp.flag == "absorbed":
            if ppp.mom_p <= 0.02:
                mom_abs.append(0.02)
            else:
                mom_abs.append(ppp.mom_p)

    s_abs = [ppp.s + s_ini for ppp in p_list if ppp.flag == "absorbed"]

    abs_table = PrettyTable(["X", "PX", "Y", "PY", "S", "P[GeV]"])
    abs_table.align = "r"
    abs_table.border = False

    # abs_table.add_column("X", x_abs)
    # abs_table.add_column("PX", px_abs)
    # abs_table.add_column("Y", y_abs)
    # abs_table.add_column("PY", py_abs)
    # abs_table.add_column("S", s_abs)
    # abs_table.add_column("P[GeV]", mom_abs)

    for i in range(len(x_abs)):
        abs_table.add_row(
            [x_abs[i], px_abs[i], y_abs[i], py_abs[i], s_abs[i], mom_abs[i]]
        )

    with open(file_absorbed, "w") as fp:
        fp.write(collimator.name + "\n")
        if len(s_abs) == 0:
            fp.write("X    PX    Y    PY    S    P[GeV]")
        else:
            fp.write(abs_table.get_string())


def rotate(x, x_p, y, y_p, coll):
    x_new = np.array(x) * cos(coll.angle) + np.array(y) * sin(coll.angle)
    px_new = np.array(x_p) * cos(coll.angle) + np.array(y_p) * sin(coll.angle)

    y_new = -1 * np.array(x) * sin(coll.angle) + np.array(y) * cos(coll.angle)
    py_new = -1 * np.array(x_p) * sin(coll.angle) + np.array(y_p) * cos(
        coll.angle
    )

    return x_new, px_new, y_new, py_new


# from mpl_toolkits.mplot3d import Axes3D


def plot2d_proton(proton, coll, mar):
    # plt.figure()
    if coll.type == "h":
        plt.plot(
            [coll.s0, coll.s0 + coll.length],
            [coll.aper_x + coll.unc_p_u, coll.aper_x + coll.unc_p_d],
        )
        plt.plot(
            [coll.s0, coll.s0 + coll.length],
            [-coll.aper_x + coll.unc_n_u, -coll.aper_x + coll.unc_n_d],
        )
        # plt.xlim(-.1, coll.length + 0.1)
        plt.ylim(-coll.aper_x - 2e-3, coll.aper_x + 2e-3)
        plt.plot(proton.s, proton.x, mar)
        plt.ylabel("x (m)")
    else:
        plt.plot(
            [coll.s0, coll.s0 + coll.length],
            [coll.aper_y + coll.unc_p_u, coll.aper_y + coll.unc_p_d],
        )
        plt.plot(
            [coll.s0, coll.s0 + coll.length],
            [-coll.aper_y + coll.unc_n_u, -coll.aper_y + coll.unc_n_d],
        )
        # plt.xlim(-.1, coll.length + 0.1)
        plt.ylim(-coll.aper_y - 2e-3, coll.aper_y + 2e-3)
        plt.plot(proton.s, proton.y, mar)
        plt.ylabel("y (m)")

    global fig_num
    fig = "fig_" + str(fig_num) + ".png"
    plt.xlabel("s (m)")
    plt.minorticks_on()
    # plt.title(fig)
    # plt.savefig(fig)
    # plt.clf()
    fig_num += 1


def plot_coll(coll):
    # plt.figure()
    if coll.type == "h":
        plt.axvline(coll.aper_x + coll.unc_p_u)
        plt.axvline(-coll.aper_x + coll.unc_n_u)
        # plt.xlim(-.1, coll.length + 0.1)
        # plt.ylim(-coll.aper_x - 2e-3, coll.aper_x + 2e-3)
        plt.xlabel("x (m)")
        plt.ylabel("y (m)")
    else:
        plt.axhline(coll.aper_y + coll.unc_p_u)
        plt.axhline(-coll.aper_y + coll.unc_n_u)
        # plt.xlim(-.1, coll.length + 0.1)
        # plt.ylim(-coll.aper_y - 2e-3, coll.aper_y + 2e-3)
        plt.xlabel("x (m)")
        plt.ylabel("y (m)")

    plt.minorticks_on()


def plot_proton(proton, coll, mar):
    fig2 = plt.figure()
    ax2 = fig2.gca(projection="3d")

    if coll.type == "h":
        ax2.plot(
            [coll.s0, coll.s0 + coll.length],
            [coll.aper_x + coll.unc_p_u, coll.aper_x + coll.unc_p_d],
            [0, 0],
        )
        ax2.plot(
            [coll.s0, coll.s0 + coll.length],
            [-coll.aper_x + coll.unc_n_u, -coll.aper_x + coll.unc_n_d],
            [0, 0],
        )
        ax2.auto_scale_xyz(
            [-0.1, coll.length + 0.1],
            [-coll.aper_x - 2e-3, coll.aper_x + 2e-3],
            [-0.5e-3, 0.5e-3],
        )
    else:
        ax2.plot(
            [coll.s0, coll.s0 + coll.length],
            [0, 0],
            [coll.aper_y + coll.unc_p_u, coll.aper_y + coll.unc_p_d],
        )
        ax2.plot(
            [coll.s0, coll.s0 + coll.length],
            [0, 0],
            [-coll.aper_y + coll.unc_n_u, -coll.aper_y + coll.unc_n_d],
        )
        ax2.auto_scale_xyz(
            [-0.1, coll.length + 0.1],
            [-0.5e-3, 0.5e-3],
            [-coll.aper_y - 20e-3, coll.aper_y + 20e-3],
        )

    # ax2.set_xlim()
    # ax2.set_ylim()

    ax2.plot([proton.s], [proton.x], [proton.y], mar)
    ax2.view_init(elev=10.0, azim=0)
    # ax2.set_xlabel('s (mm)')
    # ax2.set_ylabel('x (mm)')
    # ax2.set_zlabel('y (mm)')

    global fig_num3d
    fig = "fig_3d_" + str(fig_num3d) + ".png"
    plt.title(fig)
    plt.savefig(fig)
    plt.clf()
    fig_num3d += 1


def plot_proton_new(proton, coll, mar):
    fig2 = plt.figure()
    ax2 = fig2.gca(projection="3d")

    if coll.type == "h":
        ax2.plot(
            [coll.s0, coll.s0 + coll.length],
            [coll.aper_x + coll.unc_p_u, coll.aper_x + coll.unc_p_d],
            [0, 0],
        )
        ax2.plot(
            [coll.s0, coll.s0 + coll.length],
            [-coll.aper_x + coll.unc_n_u, -coll.aper_x + coll.unc_n_d],
            [0, 0],
        )
        ax2.auto_scale_xyz(
            [-0.1, coll.length + 0.1],
            [-coll.aper_x - 2e-3, coll.aper_x + 2e-3],
            [-0.5e-3, 0.5e-3],
        )
    else:
        ax2.plot(
            [coll.s0, coll.s0 + coll.length],
            [0, 0],
            [coll.aper_y + coll.unc_p_u, coll.aper_y + coll.unc_p_d],
        )
        ax2.plot(
            [coll.s0, coll.s0 + coll.length],
            [0, 0],
            [-coll.aper_y + coll.unc_n_u, -coll.aper_y + coll.unc_n_d],
        )
        ax2.auto_scale_xyz(
            [-0.1, coll.length + 0.1],
            [-0.5e-3, 0.5e-3],
            [-coll.aper_y - 20e-3, coll.aper_y + 20e-3],
        )

    # ax2.set_xlim()
    # ax2.set_ylim()

    ax2.plot([proton.s], [proton.x], [proton.y], mar)
    ax2.view_init(elev=10.0, azim=0)
    fig = "fig_3d_" + str(fig_num3d) + ".png"
    plt.title(fig)
    # plt.savefig(fig)
    # plt.clf()
    # global fig_num3d
    # fig_num3d += 1


def black_absorber(p_list):
    for ppp in p_list:
        if ppp.flag == "inside":
            ppp.flag = "absorbed"


def do_scattering_routine(collimator, p_universe):
    """
    SCATTERING ROUTINE!!  PyCollimate
    """
    for pp_in in p_universe:
        # print 'New proton'
        rem_coll_s = collimator.length + collimator.s0 - pp_in.s
        check_flag(collimator, pp_in)
        if not pp_in.flag == "absorbed":
            while pp_in.flag == "inside":
                rem_coll_len = rem_coll_s * pp_in.mom_dir_motion()
                cs = get_cross_sections(pp_in, collimator)
                step = get_step_size(collimator, cs)

                if step > rem_coll_len:
                    while pp_in.flag == "inside":
                        if np.abs(rem_coll_len) < 1e-15:
                            pp_in.flag = "outside"
                            break
                        step = check_mcs_step(collimator, pp_in, rem_coll_len)
                        if step > 0:
                            rem_coll_s, rem_coll_len = contin_processes(
                                collimator, pp_in, step, rem_coll_s
                            )
                            if pp_in.flag == "outside" and rem_coll_s > 0.0:
                                pp_in.track_drift(rem_coll_s)
                                rem_coll_len = 0.0
                                rem_coll_s = 0.0
                            if rem_coll_s < 0 and abs(rem_coll_s) < 1e6:
                                pp_in.s += rem_coll_s
                                pp_in.flag = "outside"
                                # plot_proton(pp_in, collimator, '+')
                                # plot2d_proton(pp_in, collimator, '+')
                                # plt.plot(pp_in.s, pp_in.y, '+')
                        else:
                            pp_in.flag = "outside"

                else:
                    rem_coll_s, rem_coll_len = contin_processes(
                        collimator, pp_in, step, rem_coll_s
                    )
                    if pp_in.flag == "outside":
                        if pp_in.s == collimator.s0 + collimator.length:
                            break
                        else:
                            # pp_in.track_drift(rem_coll_s)
                            # print 'beg:', pp_in.flag, pp_in.y, pp_in.s, pp_in.id
                            while (
                                pp_in.s
                                < (collimator.s0 + collimator.length) - 1e-7
                            ):
                                if pp_in.flag == "absorbed":
                                    break
                                check_flag(collimator, pp_in)
                                get_impact_point_rel(collimator, pp_in)
                                # break
                                do_scattering_routine(collimator, [pp_in])
                                # break

                    else:
                        cs = get_cross_sections(pp_in, collimator)
                        interaction = which_interaction(cs)
                        pp_in.make_interaction(interaction, collimator, cs)
                        # plot_proton(pp_in, collimator, '*')
                        # plot2d_proton(pp_in, collimator, '*')
                        # plt.plot(pp_in.s, pp_in.y, '*')
                        if pp_in.mom_p < 0.02:
                            pp_in.flag = "absorbed"
                            # plot_proton(pp_in, collimator, 'd')
                            # plot2d_proton(pp_in, collimator, 'd')
                            # plt.plot(pp_in.s, pp_in.y, 'd')


def do_scattering_routine_septum(septum, p_universe):
    """
    SCATTERING ROUTINE!!  PyCollimate
    """
    for pp_in in p_universe:
        # print 'New proton'
        a, _, _, d = septum.get_points()
        rem_coll_s = septum.length + septum.s0 - pp_in.s
        check_flag_septum(septum, pp_in)
        if not pp_in.flag == "absorbed":
            while pp_in.flag == "inside":
                rem_coll_len = rem_coll_s * pp_in.mom_dir_motion()
                cs = get_cross_sections(pp_in, septum)
                step = get_step_size(septum, cs)

                if step > rem_coll_len:
                    while pp_in.flag == "inside":
                        if np.abs(rem_coll_len) < 1e-15:
                            pp_in.flag = "outside"
                            break
                        step = check_mcs_step_septum(
                            septum, pp_in, rem_coll_len
                        )
                        if step > 0:
                            rem_coll_s, rem_coll_len = contin_processes_septum(
                                septum, pp_in, step, rem_coll_s
                            )
                            if pp_in.flag == "outside" and rem_coll_s > 0.0:
                                # print pp_in.x, pp_in.s, septum.get_inclination(), a, d, septum.get_inclination() * pp_in.s + a, septum.get_inclination() * pp_in.s + d
                                if pp_in.x <= (
                                    septum.get_inclination()
                                    * (pp_in.s - septum.s0)
                                    + a
                                ):
                                    pp_in.track_drift(rem_coll_s)
                                elif pp_in.x >= (
                                    septum.get_inclination()
                                    * (pp_in.s - septum.s0)
                                    + d
                                ):
                                    pp_in.track_in_field(septum, rem_coll_s)
                                else:
                                    raise ValueError("Particle still inside")
                                rem_coll_len = 0.0
                                rem_coll_s = 0.0
                            if rem_coll_s < 0 and abs(rem_coll_s) < 1e-6:
                                pp_in.s += rem_coll_s
                                pp_in.flag = "outside"
                                # plot_proton(pp_in, septum, '+')
                                # plot2d_proton(pp_in, septum, '+')
                                # plt.plot(pp_in.s, pp_in.y, '+')
                        else:
                            pp_in.flag = "outside"

                else:
                    rem_coll_s, rem_coll_len = contin_processes_septum(
                        septum, pp_in, step, rem_coll_s
                    )
                    if pp_in.flag == "outside":
                        if pp_in.s == septum.s0 + septum.length:
                            break
                        else:
                            # pp_in.track_drift(rem_coll_s)
                            # print 'beg:', pp_in.flag, pp_in.y, pp_in.s, pp_in.id

                            ## quick and dirty infinite loop fix ##
                            count = 0
                            while pp_in.s < (septum.s0 + septum.length) - 1e-7:
                                count += 1
                                if count > 1000:
                                    print(
                                        "Particle {} got stuck in an infinite loop! Assuming absorbed.".format(
                                            pp_in.id
                                        )
                                    )
                                    pp_in.flag = "absorbed"
                                    break
                                ##/ quick and dirty infinite loop fix ##
                                if pp_in.flag == "absorbed":
                                    break

                                septum_part = copy.copy(septum)

                                septum_part.update_s0(pp_in.s)

                                # print pp_in.s, septum.s0, septum.length, pp_in.id
                                # print septum_part.s0, septum_part.length
                                check_flag_septum(septum_part, pp_in)
                                get_impact_pos(septum_part, pp_in)
                                # break
                                do_scattering_routine_septum(
                                    septum_part, [pp_in]
                                )
                                # break
                                # septum.update_s0(temp_len)

                    else:
                        cs = get_cross_sections(pp_in, septum)
                        interaction = which_interaction(cs)
                        pp_in.make_interaction(interaction, septum, cs)
                        # plot_proton(pp_in, septum, '*')
                        # plot2d_proton(pp_in, septum, '*')
                        # plt.plot(pp_in.s, pp_in.y, '*')
                        if pp_in.mom_p < 0.02:
                            pp_in.flag = "absorbed"
                            # plot_proton(pp_in, septum, 'd')
                            # plot2d_proton(pp_in, septum, 'd')
                            # plt.plot(pp_in.s, pp_in.y, 'd')


def read_coll(file_db, line_n):
    with open(file_db, "r") as ff:
        for i, line in enumerate(ff):
            if i == line_n - 1:
                coll_descr = line.split()
                break
    coll_type = coll_descr[0][5].lower()
    if coll_type == "h":
        aperx = float(coll_descr[1])
        apery = 0.04
    elif coll_type == "v":
        aperx = 0.04
        apery = float(coll_descr[1])
    name = coll_descr[0][1:-1]
    new_coll = Collimator(
        "Graphite", 0, 1.34, coll_type, aperx, apery, 0.0, 0.0, 0.0, 0.0, name
    )
    s0 = float(coll_descr[2])
    return new_coll, s0


def read_coll_lhc(file_db, line_n):
    with open(file_db, "r") as ff:
        for i, line in enumerate(ff):
            if i == line_n:
                coll_descr = line.split()
                break
    print(coll_descr)
    name = coll_descr[0][1:-1]
    coll_type = coll_descr[1]

    aper_up_p = float(coll_descr[2])
    aper_up_n = float(coll_descr[3])

    aper_down_p = float(coll_descr[4])
    aper_down_n = float(coll_descr[5])

    s0 = float(coll_descr[6])
    length = float(coll_descr[7])
    material = coll_descr[8]

    error_up_on = coll_descr[9]
    if error_up_on:
        std_up = float(coll_descr[10])
    else:
        std_up = 0.0

    error_do_on = coll_descr[11]
    if error_do_on:
        std_do = float(coll_descr[12])
    else:
        std_do = 0.0

    if coll_type == "v":
        aperx = 0.04
        apery = abs(aper_up_p - aper_down_p) / 2.0

        unc_p_u = (aper_up_p - apery) + random.gauss(0, std_up)
        unc_n_u = -1 * (apery + aper_up_n) + random.gauss(0, std_up)

        unc_p_d = (aper_down_p - apery) + random.gauss(0, std_do)
        unc_n_d = -1 * (apery + aper_down_n) + random.gauss(0, std_do)
    else:
        apery = 0.04
        aperx = abs(aper_up_p - aper_down_p) / 2.0

        unc_p_u = (aper_up_p - aperx) + random.gauss(0, std_up)
        unc_n_u = -1 * (aperx + aper_up_n) + random.gauss(0, std_up)

        unc_p_d = (aper_down_p - aperx) + random.gauss(0, std_do)
        unc_n_d = -1 * (aperx + aper_down_n) + random.gauss(0, std_do)
    angle = float(coll_descr[13])
    new_coll = Collimator(
        material,
        0,
        length,
        coll_type,
        aperx,
        apery,
        unc_p_u,
        unc_p_d,
        unc_n_u,
        unc_n_d,
        name,
        angle,
    )

    return new_coll, s0


def make_pencil_beam(n_particles, momentum, h_offset=0.0, v_offset=0.0):
    particles = []
    for i in range(1, n_particles + 1):
        pp = Proton(
            0.0,
            0.0 + h_offset,
            0e-2,
            0.0 + v_offset,
            0.0,
            0.0,
            momentum,
            0.0,
            "outside",
            i,
        )
        particles.append(pp)
    return particles


def convert_collDB(coll_DB6t):
    with open(coll_DB6t, "rb") as fc:

        fc.readline()
        fc.readline()

        names = []
        aper_n = []
        mat = []
        length = []
        angle = []

        while True:
            line = fc.readline()
            if line == "":
                break
            if line.strip()[0] == "#":
                names.append(fc.readline().split()[0])
                fc.readline()
                aper_n.append(float(fc.readline().split()[0]))
                mat.append(fc.readline().split()[0])
                length.append(float(fc.readline().split()[0]))
                angle.append(float(fc.readline().split()[0]))

    mat_pc = []
    for i in mat:
        if i == "C":
            mat_pc.append("Graphite")
        elif i == "CU":
            mat_pc.append("Copper")
        elif i == "W":
            mat_pc.append("Tungsten")
        elif i == "AL":
            mat_pc.append("Aluminum")

    return names, aper_n, mat_pc, length, angle


def coll_markers_and_DB_inj(
    twiss_file,
    collDB_6t,
    inj_file,
    inj_aper=(6.8, 6.8, 6.8),
    errors=("False 0.0", "False 0.0"),
):

    names, aper_n, mat, length, angle = convert_collDB(collDB_6t)

    name_inj, aper_inj = np.loadtxt(
        inj_file,
        usecols=(0, 7),
        dtype=str,
        unpack=True,
        delimiter=",",
        skiprows=1,
    )

    names_new, aper_n_new, mat_new, length_new, angle_new = [], [], [], [], []

    if "TDI.4L2.B1" in names:
        tdi_index = names.index("TDI.4L2.B1")

    elif "TDI.4R8.B2" in names:
        tdi_index = names.index("TDI.4R8.B2")

    del length[tdi_index]
    del aper_n[tdi_index]
    del mat[tdi_index]
    del angle[tdi_index]
    del names[tdi_index]

    for i in range(len(names)):
        if names[i] in name_inj:
            aper_n[i] = float(aper_inj[list(name_inj).index(names[i])])

    patterns_list = [r"\"%s\"" % re.escape(s.strip()) for s in names]
    patterns = re.compile("|".join(patterns_list))

    line1_tot = []
    line2_tot = []

    with open(twiss_file, "r") as f:
        for i, s in enumerate(f):
            if i == 2:
                sequence = s.split()[3].strip('"')

            if patterns.search(s):
                col = s.split()
                pos = float(col[1])
                l = float(col[2])
                l_colDB = length[names.index(col[0].strip('"'))]

                if l != l_colDB:
                    if l_colDB != 0:
                        l_colDB = max(l, l_colDB)
                        print(
                            "Lengths of "
                            + col[0].strip('"')
                            + " are not the same, used the biggest (l = ",
                            l,
                            ", " "l_collDB = ",
                            l_colDB,
                            ")",
                        )
                    else:
                        print(
                            "Collimator "
                            + col[0].strip('"')
                            + " has length zero in collDB - not used! (l = ",
                            l,
                            ", l_collDB = ",
                            l_colDB,
                            ")",
                        )

                if l_colDB == 0:
                    print("Length 0.0 for " + col[0].strip('"'))

                    index_todelete = names.index(col[0].strip('"'))

                    del length[index_todelete]
                    del aper_n[index_todelete]
                    del mat[index_todelete]
                    del angle[index_todelete]
                    del names[index_todelete]
                else:
                    index_tosave = names.index(col[0].strip('"'))
                    length_new.append(length[index_tosave])
                    aper_n_new.append(aper_n[index_tosave])
                    mat_new.append(mat[index_tosave])
                    angle_new.append(angle[index_tosave])
                    names_new.append(names[index_tosave])
                    line1 = (
                        "install, element = aper."
                        + s.split()[0].strip('"')
                        + ".a, class = marker, at = -"
                        + str(l_colDB / 2.0)
                        + ", from = "
                        + col[0].strip('"')
                        + ";\n"
                    )
                    line2 = (
                        "install, element = aper."
                        + s.split()[0].strip('"')
                        + ".b, class = marker, at = "
                        + str(l_colDB / 2.0)
                        + ", from = "
                        + col[0].strip('"')
                        + ";\n"
                    )

                    line1_tot.append(line1)
                    line2_tot.append(line2)

    with open("install_coll_markers.madx", "w") as fw:
        fw.write("seqedit, sequence = " + sequence + ";\n")
        for l1, l2 in zip(line1_tot, line2_tot):
            fw.write(l1)
            fw.write(l2)
        fw.write("flatten;\n endedit;")

    lhc_beam = Beam("LHC")
    emit = lhc_beam.emit_nom / (lhc_beam.gamma * lhc_beam.beta)

    variables = ["name", "s", "x", "y", "betx", "bety", "dx", "dy", "l"]
    name, s, x, y, betx, bety, dx, dy, l = get_twiss_var(
        twiss_file, "", variables
    )

    sigma_x = np.sqrt(emit * np.array(betx))
    sigma_y = np.sqrt(emit * np.array(bety))

    coll_DB = PrettyTable(
        [
            "Element",
            "Type",
            "Aper_up_p [m]",
            "Aper_up_n [m]",
            "Aper_down_p [m]",
            "Aper_down_n [m]",
            "S_0 [m]",
            "Length [m]",
            "Material",
            "Error_up (Bool, std [m])",
            "Error_down (Bool, std[m])",
            "Angle [deg]",
        ]
    )
    coll_DB.align = "r"
    coll_DB.border = False

    tdi_aper = inj_aper[0]
    tclia_aper = inj_aper[1]
    tclib_aper = inj_aper[2]

    patterns_collDB = [
        r"\"%s\"" % re.escape(string.strip()) for string in names_new
    ]
    patterns_install = re.compile("|".join(patterns_collDB))

    out_file = "coll_DB_pyCollimate_lhc_" + sequence[3:].lower() + ".txt"

    with open(out_file, "w") as fp:

        for i in range(len(name)):

            if re.search(r"\"TDI\.1", name[i]):
                coll_DB.add_row(
                    [
                        name[i],
                        "v",
                        y[i - 1] + tdi_aper * sigma_y[i - 1],
                        y[i - 1] - tdi_aper * sigma_y[i - 1],
                        y[i] + tdi_aper * sigma_y[i],
                        y[i] - tdi_aper * sigma_y[i],
                        s[i - 1],
                        l[i],
                        "BN5000",
                        errors[0],
                        errors[1],
                        0.0,
                    ]
                )
            if re.search(r"\"TDI\.2", name[i]):
                coll_DB.add_row(
                    [
                        name[i],
                        "v",
                        y[i - 1] + 2e-3 + tdi_aper * sigma_y[i - 1],
                        y[i - 1] - 2e-3 - tdi_aper * sigma_y[i - 1],
                        y[i] + tdi_aper * sigma_y[i],
                        y[i] - tdi_aper * sigma_y[i],
                        s[i - 1],
                        l[i],
                        "Aluminum",
                        errors[0],
                        errors[1],
                        0.0,
                    ]
                )

            elif re.search(r"\"TDI\.3", name[i]):
                coll_DB.add_row(
                    [
                        name[i],
                        "v",
                        y[i - 1] + 2e-3 + tdi_aper * sigma_y[i - 1],
                        y[i - 1] - 2e-3 - tdi_aper * sigma_y[i - 1],
                        y[i] + tdi_aper * sigma_y[i],
                        y[i] - tdi_aper * sigma_y[i],
                        s[i - 1],
                        l[i],
                        "Copper",
                        errors[0],
                        errors[1],
                        0.0,
                    ]
                )

            elif re.search(r"\"TCLIA", name[i]):
                if (sequence == "LHCB1" and re.search("4R2", name[i])) or (
                    sequence == "LHCB2" and re.search("4L8", name[i])
                ):
                    coll_DB.add_row(
                        [
                            name[i],
                            "v",
                            y[i - 1] + tclia_aper * sigma_y[i - 1],
                            y[i - 1] - tclia_aper * sigma_y[i - 1],
                            y[i - 1] + tclia_aper * sigma_y[i - 1],
                            y[i - 1] - tclia_aper * sigma_y[i - 1],
                            s[i - 1],
                            l[i],
                            "R4550",
                            errors[0],
                            errors[1],
                            0.0,
                        ]
                    )
            elif re.search(r"\"TCLIB", name[i]):
                coll_DB.add_row(
                    [
                        name[i],
                        "v",
                        y[i - 1] + tclib_aper * sigma_y[i - 1],
                        y[i - 1] - tclib_aper * sigma_y[i - 1],
                        y[i - 1] + tclib_aper * sigma_y[i - 1],
                        y[i - 1] - tclib_aper * sigma_y[i - 1],
                        s[i - 1],
                        l[i],
                        "Graphite",
                        errors[0],
                        errors[1],
                        0.0,
                    ]
                )
            elif patterns_install.search(name[i]):
                coll_index = names_new.index(name[i].strip('"'))
                aper_loc = aper_n_new[coll_index]
                mat_loc = mat_new[coll_index]
                angle_loc = floor(degrees(angle_new[coll_index]))
                length_loc = length_new[coll_index]

                if angle_loc == 90.0:
                    coll_type = "v"
                    aper_pos = y[i - 1] + aper_loc * sigma_y[i - 1]
                    aper_neg = y[i - 1] - aper_loc * sigma_y[i - 1]
                elif angle_loc == 0.0:
                    coll_type = "h"
                    aper_pos = x[i - 1] + aper_loc * sigma_x[i - 1]
                    aper_neg = x[i - 1] - aper_loc * sigma_x[i - 1]
                else:
                    coll_type = "h"
                    sigma_skew = sqrt(
                        sigma_x[i - 1] ** 2 * cos(radians(angle_loc)) ** 2
                        + sigma_y[i - 1] ** 2 * sin(radians(angle_loc)) ** 2
                    )
                    aper_pos = aper_loc * sigma_skew
                    aper_neg = -1 * aper_loc * sigma_skew

                coll_DB.add_row(
                    [
                        name[i],
                        coll_type,
                        aper_pos,
                        aper_neg,
                        aper_pos,
                        aper_neg,
                        s[i - 1],
                        length_loc,
                        mat_loc,
                        errors[0],
                        errors[1],
                        angle_loc,
                    ]
                )

        fp.write(coll_DB.get_string())

    return names_new, aper_n_new, mat_new, length_new, angle_new


def read_collDB_inj(twiss_file, collDB_6t, inj_file):

    names, aper_n, mat, length, angle = convert_collDB(collDB_6t)

    name_inj, aper_inj = np.loadtxt(
        inj_file,
        usecols=(0, 7),
        dtype=str,
        unpack=True,
        delimiter=",",
        skiprows=1,
    )

    names_new, aper_n_new, mat_new, length_new, angle_new = [], [], [], [], []

    # if 'TDI.4L2.B1' in names:
    #     tdi_index = names.index('TDI.4L2.B1')
    #
    # elif 'TDI.4R8.B2' in names:
    #     tdi_index = names.index('TDI.4R8.B2')
    #
    # del length[tdi_index]
    # del aper_n[tdi_index]
    # del mat[tdi_index]
    # del angle[tdi_index]
    # del names[tdi_index]

    for i in range(len(names)):
        if names[i] in name_inj:
            aper_n[i] = float(aper_inj[list(name_inj).index(names[i])])

    patterns_list = [r"\"%s\"" % re.escape(s.strip()) for s in names]
    patterns = re.compile("|".join(patterns_list))

    line1_tot = []
    line2_tot = []

    with open(twiss_file, "r") as f:
        for i, s in enumerate(f):
            if i == 2:
                sequence = s.split()[3].strip('"')
                madx_input_delete = ["seqedit, sequence = " + sequence + ";\n"]

            if patterns.search(s):
                col = s.split()
                pos = float(col[1])
                l = float(col[2])
                l_colDB = length[names.index(col[0].strip('"'))]

                if l != l_colDB:
                    if l_colDB != 0:
                        l_colDB = max(l, l_colDB)
                        print(
                            "Lengths of "
                            + col[0].strip('"')
                            + " are not the same, used the biggest (l = ",
                            l,
                            ", " "l_collDB = ",
                            l_colDB,
                            ")",
                        )
                    else:
                        print(
                            "Collimator "
                            + col[0].strip('"')
                            + " has length zero in collDB - not used! (l = ",
                            l,
                            ", l_collDB = ",
                            l_colDB,
                            ")",
                        )
                        madx_input_delete.append(
                            "remove, element = " + col[0].strip('"') + ";\n"
                        )

                if l_colDB == 0:
                    print("Length 0.0 for " + col[0].strip('"'))

                    madx_input_delete.append(
                        "remove, element = " + col[0].strip('"') + ";\n"
                    )
                    index_todelete = names.index(col[0].strip('"'))

                    del length[index_todelete]
                    del aper_n[index_todelete]
                    del mat[index_todelete]
                    del angle[index_todelete]
                    del names[index_todelete]
                else:
                    index_tosave = names.index(col[0].strip('"'))
                    length_new.append(length[index_tosave])
                    aper_n_new.append(aper_n[index_tosave])
                    mat_new.append(mat[index_tosave])
                    angle_new.append(angle[index_tosave])
                    names_new.append(names[index_tosave])
                    line1 = (
                        "install, element = aper."
                        + s.split()[0].strip('"')
                        + ".a, class = marker, at = -"
                        + str(l_colDB / 2.0)
                        + ", from = "
                        + col[0].strip('"')
                        + ";\n"
                    )
                    line2 = (
                        "install, element = aper."
                        + s.split()[0].strip('"')
                        + ".b, class = marker, at = "
                        + str(l_colDB / 2.0)
                        + ", from = "
                        + col[0].strip('"')
                        + ";\n"
                    )

                    line1_tot.append(line1)
                    line2_tot.append(line2)

    with open("coll_to_remove.madx", "w") as fw:
        for line in madx_input_delete:
            fw.write(line)
        fw.write("endedit;")

    return names_new, aper_n_new, mat_new, length_new, angle_new


def coll_markers_and_DB(
    twiss_file,
    collDB_6t,
    inj_aper=(6.8, 6.8, 6.8),
    errors=("False 0.0", "False 0.0"),
):

    names, aper_n, mat, length, angle = convert_collDB(collDB_6t)

    names_new, aper_n_new, mat_new, length_new, angle_new = [], [], [], [], []

    if "TDI.4L2.B1" in names:
        tdi_index = names.index("TDI.4L2.B1")

    elif "TDI.4R8.B2" in names:
        tdi_index = names.index("TDI.4R8.B2")

    del length[tdi_index]
    del aper_n[tdi_index]
    del mat[tdi_index]
    del angle[tdi_index]
    del names[tdi_index]

    patterns_list = [r"\"%s\"" % re.escape(s.strip()) for s in names]
    patterns = re.compile("|".join(patterns_list))

    line1_tot = []
    line2_tot = []

    with open(twiss_file, "r") as f:
        for i, s in enumerate(f):
            if i == 2:
                sequence = s.split()[3].strip('"')

            if patterns.search(s):
                col = s.split()
                pos = float(col[1])
                l = float(col[2])
                l_colDB = length[names.index(col[0].strip('"'))]

                if l != l_colDB:
                    if l_colDB != 0:
                        l_colDB = max(l, l_colDB)
                        print(
                            "Lengths of "
                            + col[0].strip('"')
                            + " are not the same, used the biggest (l = ",
                            l,
                            ", " "l_collDB = ",
                            l_colDB,
                            ")",
                        )
                    else:
                        print(
                            "Collimator "
                            + col[0].strip('"')
                            + " has length zero in collDB - not used! (l = ",
                            l,
                            ", l_collDB = ",
                            l_colDB,
                            ")",
                        )

                if l_colDB == 0:
                    print("Length 0.0 for " + col[0].strip('"'))

                    index_todelete = names.index(col[0].strip('"'))

                    del length[index_todelete]
                    del aper_n[index_todelete]
                    del mat[index_todelete]
                    del angle[index_todelete]
                    del names[index_todelete]
                else:
                    index_tosave = names.index(col[0].strip('"'))
                    length_new.append(length[index_tosave])
                    aper_n_new.append(aper_n[index_tosave])
                    mat_new.append(mat[index_tosave])
                    angle_new.append(angle[index_tosave])
                    names_new.append(names[index_tosave])
                    line1 = (
                        "install, element = aper."
                        + s.split()[0].strip('"')
                        + ".a, class = marker, at = -"
                        + str(l_colDB / 2.0)
                        + ", from = "
                        + col[0].strip('"')
                        + ";\n"
                    )
                    line2 = (
                        "install, element = aper."
                        + s.split()[0].strip('"')
                        + ".b, class = marker, at = "
                        + str(l_colDB / 2.0)
                        + ", from = "
                        + col[0].strip('"')
                        + ";\n"
                    )

                    line1_tot.append(line1)
                    line2_tot.append(line2)

    with open("install_coll_markers.madx", "w") as fw:
        fw.write("seqedit, sequence = " + sequence + ";\n")
        for l1, l2 in zip(line1_tot, line2_tot):
            fw.write(l1)
            fw.write(l2)
        fw.write("flatten;\n endedit;")

    lhc_beam = Beam("LHC")
    emit = lhc_beam.emit_nom / (lhc_beam.gamma * lhc_beam.beta)

    variables = ["name", "s", "x", "y", "betx", "bety", "dx", "dy", "l"]
    name, s, x, y, betx, bety, dx, dy, l = get_twiss_var(
        twiss_file, "", variables
    )

    sigma_x = np.sqrt(emit * np.array(betx))
    sigma_y = np.sqrt(emit * np.array(bety))

    coll_DB = PrettyTable(
        [
            "Element",
            "Type",
            "Aper_up_p [m]",
            "Aper_up_n [m]",
            "Aper_down_p [m]",
            "Aper_down_n [m]",
            "S_0 [m]",
            "Length [m]",
            "Material",
            "Error_up (Bool, std [m])",
            "Error_down (Bool, std[m])",
            "Angle [deg]",
        ]
    )
    coll_DB.align = "r"
    coll_DB.border = False

    tdi_aper = inj_aper[0]
    tclia_aper = inj_aper[1]
    tclib_aper = inj_aper[2]

    patterns_collDB = [
        r"\"%s\"" % re.escape(string.strip()) for string in names_new
    ]
    patterns_install = re.compile("|".join(patterns_collDB))

    out_file = "coll_DB_pyCollimate_lhc_" + sequence[3:].lower() + ".txt"

    with open(out_file, "w") as fp:

        for i in range(len(name)):

            if re.search(r"\"TDI\.1", name[i]):
                coll_DB.add_row(
                    [
                        name[i],
                        "v",
                        y[i - 1] + tdi_aper * sigma_y[i - 1],
                        y[i - 1] - tdi_aper * sigma_y[i - 1],
                        y[i] + tdi_aper * sigma_y[i],
                        y[i] - tdi_aper * sigma_y[i],
                        s[i - 1],
                        l[i],
                        "BN5000",
                        errors[0],
                        errors[1],
                        0.0,
                    ]
                )
            if re.search(r"\"TDI\.2", name[i]):
                coll_DB.add_row(
                    [
                        name[i],
                        "v",
                        y[i - 1] + tdi_aper * sigma_y[i - 1],
                        y[i - 1] - tdi_aper * sigma_y[i - 1],
                        y[i] + tdi_aper * sigma_y[i],
                        y[i] - tdi_aper * sigma_y[i],
                        s[i - 1],
                        l[i],
                        "Aluminum",
                        errors[0],
                        errors[1],
                        0.0,
                    ]
                )

            elif re.search(r"\"TDI\.3", name[i]):
                coll_DB.add_row(
                    [
                        name[i],
                        "v",
                        y[i - 1] + 2e-3 + tdi_aper * sigma_y[i - 1],
                        y[i - 1] - 2e-3 - tdi_aper * sigma_y[i - 1],
                        y[i] + tdi_aper * sigma_y[i],
                        y[i] - tdi_aper * sigma_y[i],
                        s[i - 1],
                        l[i],
                        "Copper",
                        errors[0],
                        errors[1],
                        0.0,
                    ]
                )

            elif re.search(r"\"TCLIA", name[i]):
                if (sequence == "LHCB1" and re.search("4R2", name[i])) or (
                    sequence == "LHCB2" and re.search("4L8", name[i])
                ):
                    coll_DB.add_row(
                        [
                            name[i],
                            "v",
                            y[i - 1] + tclia_aper * sigma_y[i - 1],
                            y[i - 1] - tclia_aper * sigma_y[i - 1],
                            y[i - 1] + tclia_aper * sigma_y[i - 1],
                            y[i - 1] - tclia_aper * sigma_y[i - 1],
                            s[i - 1],
                            l[i],
                            "R4550",
                            errors[0],
                            errors[1],
                            0.0,
                        ]
                    )
            elif re.search(r"\"TCLIB", name[i]):
                coll_DB.add_row(
                    [
                        name[i],
                        "v",
                        y[i - 1] + tclib_aper * sigma_y[i - 1],
                        y[i - 1] - tclib_aper * sigma_y[i - 1],
                        y[i - 1] + tclib_aper * sigma_y[i - 1],
                        y[i - 1] - tclib_aper * sigma_y[i - 1],
                        s[i - 1],
                        l[i],
                        "Graphite",
                        errors[0],
                        errors[1],
                        0.0,
                    ]
                )
            elif patterns_install.search(name[i]):
                coll_index = names_new.index(name[i].strip('"'))
                aper_loc = aper_n_new[coll_index]
                mat_loc = mat_new[coll_index]
                angle_loc = floor(degrees(angle_new[coll_index]))
                length_loc = length_new[coll_index]

                if angle_loc == 90.0:
                    coll_type = "v"
                    aper_pos = y[i - 1] + aper_loc * sigma_y[i - 1]
                    aper_neg = y[i - 1] - aper_loc * sigma_y[i - 1]
                elif angle_loc == 0.0:
                    coll_type = "h"
                    aper_pos = x[i - 1] + aper_loc * sigma_x[i - 1]
                    aper_neg = x[i - 1] - aper_loc * sigma_x[i - 1]
                else:
                    coll_type = "h"
                    sigma_skew = sqrt(
                        sigma_x[i - 1] ** 2 * cos(radians(angle_loc)) ** 2
                        + sigma_y[i - 1] ** 2 * sin(radians(angle_loc)) ** 2
                    )
                    aper_pos = aper_loc * sigma_skew
                    aper_neg = -1 * aper_loc * sigma_skew

                coll_DB.add_row(
                    [
                        name[i],
                        coll_type,
                        aper_pos,
                        aper_neg,
                        aper_pos,
                        aper_neg,
                        s[i - 1],
                        length_loc,
                        mat_loc,
                        errors[0],
                        errors[1],
                        angle_loc,
                    ]
                )

        fp.write(coll_DB.get_string())

    return names_new, aper_n_new, mat_new, length_new, angle_new


def create_uselist(input_file, output_file, coll_names):
    """

    @param input_file: Twiss with markers at start and end of each collimator already installed
    @param output_file: File with use commands for tracking
    @param coll_names: names with collimators of interests
    @return:
    """
    uselist = []

    sequence = get_sequencetwiss(input_file)

    if sequence == "LHCB1":
        tdis = ["TDI.1.4L2.B1", "TDI.2.4L2.B1", "TDI.3.4L2.B1"]
    elif sequence == "LHCB2":
        tdis = ["TDI.1.4R8.B2", "TDI.2.4R8.B2", "TDI.3.4R8.B2"]

    for tdi in tdis:
        coll_names.append(tdi)

    patterns_temp = [r"\"%s\"" % re.escape(string) for string in coll_names]
    patterns = re.compile("|".join(patterns_temp))

    start = "#s"

    variables = ["name", "x"]
    elements, x = get_twiss_var(input_file, "", variables)

    for i in range(0, len(elements)):
        if patterns.search(elements[i]):
            if not re.search("DRIFT", elements[i - 1]):
                end = elements[i - 1][1:-1]
            else:
                end = elements[i - 2][1:-1]

            use_command = (
                "use, sequence = "
                + sequence
                + ", range = "
                + start
                + "/"
                + end
                + ";\n"
            )
            uselist.append(use_command)
            if not re.search("DRIFT", elements[i + 1]):
                start = elements[i + 1][1:-1]
            else:
                start = elements[i + 2][1:-1]
    end = "#e;\n"
    use_command = (
        "use, sequence = " + sequence + ", range = " + start + "/" + end
    )
    uselist.append(use_command)

    with open(output_file, "w") as fw:
        for line in uselist:
            fw.write(line)


def create_6tDB_from_twiss(collDB_6t, twiss_file, new_collDB):

    names, aper_n, mat, length, angle = convert_collDB(collDB_6t)
    variables = ["name", "betx", "bety"]
    name_t, betx, bety = get_twiss_var(twiss_file, "", variables)

    patterns_temp = [r"\b%s\b" % re.escape(string) for string in names]
    patterns = re.compile("|".join(patterns_temp))

    file_temp = []
    with open(collDB_6t, "r") as fr:
        for line in fr:
            file_temp.append(line)

    for i in range(len(file_temp)):
        if (
            patterns.search(file_temp[i])
            and '"' + file_temp[i].strip() + '"' in name_t
        ):
            file_temp[i + 7] = (
                "%.16f\n"
                % betx[name_t.index('"' + file_temp[i].strip() + '"') - 1]
            )
            file_temp[i + 8] = (
                "%.16f\n"
                % bety[name_t.index('"' + file_temp[i].strip() + '"') - 1]
            )
        elif file_temp[i].strip() == "TDI.4L2.B1":
            file_temp[i + 7] = (
                "%.16f\n" % betx[name_t.index('"TDI.1.4L2.B1"') - 1]
            )
            file_temp[i + 8] = (
                "%.16f\n" % bety[name_t.index('"TDI.1.4L2.B1"') - 1]
            )
        elif file_temp[i].strip() == "TDI.4R8.B2":
            file_temp[i + 7] = (
                "%.16f\n" % betx[name_t.index('"TDI.1.4R8.B2"') - 1]
            )
            file_temp[i + 8] = (
                "%.16f\n" % bety[name_t.index('"TDI.1.4R8.B2"') - 1]
            )

    with open(new_collDB, "wb") as fw:
        for line in file_temp:
            fw.write(line)


def make_coll_dbmadx_table():
    """
    Creates a prettytable object in order to produce a nice looking collimator database
    @return: Prettytable object
    """
    coll_DB = PrettyTable(
        [
            "Element",
            "Aper_up_p [m]",
            "Aper_up_n [m]",
            "Aper_down_p [m]",
            "Aper_down_n [m]",
            "Length [m]",
            "Material",
            "Error_up (Bool, std [m])",
            "Error_down (Bool, std[m])",
        ]
    )
    coll_DB.align = "r"
    coll_DB.border = False
    return coll_DB


def write_coll_dbmadx_table_to_file(output_file, coll_DB):
    """
    Write Prettytable object ot file
    @param output_file: name of the file given as output
    @param coll_DB: Prettytable object to be written
    @return:
    """
    with open(output_file, "w") as fp:
        fp.write(coll_DB.get_string())


def add_coll_from_list(
    twiss_file,
    coll_DB,
    beam_type,
    material,
    aper_list,
    name_coll,
    errors=("False 0.0", "False 0.0"),
):
    """
    Add all the collimators in the twiss file passed as input to the coll_DB object. The coll_DB object has
    to be created before, for example with make_coll_dbmadx_table() function.
    @param twiss_file: twiss file with the collimators to be put in the database. It needs the following columns:
    ['name', 'keyword', 'tilt', 's', 'x', 'y', 'betx', 'bety', 'dx', 'dy', 'l']
    @param coll_DB: prettytable object at which the collimators will be append
    @param beam_type: type of beam (LHC, FT, HLLHC...)
    @param material: list of materials of the collimators. if only one given, all collimators will be
    made of the same material
    @param aper_list: list of apertures in betatron sigma. if one value given, all collimators will be made with the
    same aperture in sigma
    @param errors: flag to switch on a gaussian error with a given standard deviation for both entrance and exit of
    the collimator
    @param names_coll names of collimators at which the aperture and material lists correspond to
    @return:
    """

    lhc_beam = Beam(beam_type)
    emit = lhc_beam.emit_nom / (lhc_beam.gamma * lhc_beam.beta)

    variables = [
        "name",
        "keyword",
        "tilt",
        "s",
        "x",
        "y",
        "betx",
        "bety",
        "dx",
        "dy",
        "l",
    ]
    name, keyword, tilt, s, x, y, betx, bety, dx, dy, l = get_twiss_var(
        twiss_file, "", variables
    )

    sigma_x = np.sqrt(emit * np.array(betx))
    sigma_y = np.sqrt(emit * np.array(bety))

    for i in range(len(name)):
        if "COLLIMATOR" in keyword[i]:

            if name[i].strip('"') in name_coll:
                coll_num = name_coll.index(name[i].strip('"'))

                aper_current = aper_list[coll_num]
                mat_current = material[coll_num]
                angle_current = floor(degrees(tilt[i]))
                length_current = l[i]

                if 88.0 <= angle_current <= 92.0:
                    coll_type = "v"
                    print(name[i], coll_type, angle_current)
                    aper_pos = y[i - 1] + aper_current * sigma_y[i - 1]
                    aper_neg = y[i - 1] - aper_current * sigma_y[i - 1]
                elif angle_current == 0.0:
                    coll_type = "h"
                    print(name[i], coll_type, angle_current)
                    aper_pos = x[i - 1] + aper_current * sigma_x[i - 1]
                    aper_neg = x[i - 1] - aper_current * sigma_x[i - 1]
                else:
                    coll_type = "h"
                    print(name[i], coll_type, angle_current)
                    sigma_skew = sqrt(
                        sigma_x[i - 1] ** 2 * cos(radians(angle_current)) ** 2
                        + sigma_y[i - 1] ** 2 * sin(radians(angle_current)) ** 2
                    )
                    aper_pos = aper_current * sigma_skew
                    aper_neg = -1 * aper_current * sigma_skew

                coll_DB.add_row(
                    [
                        name[i],
                        aper_pos,
                        aper_neg,
                        aper_pos,
                        aper_neg,
                        length_current,
                        mat_current,
                        errors[0],
                        errors[1],
                    ]
                )


def get_rand_num_trunc(std, limit):
    return float(trandn.rvs(-limit / std, limit / std, scale=std, size=1))


def add_coll_from_twiss(
    twiss_file,
    coll_DB,
    beam_type,
    material,
    aper_list,
    errors=("False 0.0", "False 0.0"),
):
    """
    Add all the collimators in the twiss file passed as input to the coll_DB object. The coll_DB object has
    to be created before, for example with make_coll_dbmadx_table() function.
    @param twiss_file: twiss file with the collimators to be put in the database. It needs the following columns:
    ['name', 'keyword', 'tilt', 's', 'x', 'y', 'betx', 'bety', 'dx', 'dy', 'l']
    @param coll_DB: prettytable object at which the collimators will be append
    @param beam_type: type of beam (LHC, FT, HLLHC...)
    @param material: list of materials of the collimators. if only one given, all collimators will be
    made of the same material
    @param aper_list: list of apertures in betatron sigma. if one value given, all collimators will be made with the
    same aperture in sigma
    @param errors: flag to switch on a gaussian error with a given standard deviation (in betatron sigma). The first refers to the entrance
    the second to the exit of the collimator
    @param names_coll names of collimators at which the aperture and material lists correspond to
    @return: list of errors assigned in betatron sigma
    """

    lhc_beam = Beam(beam_type)
    emit = lhc_beam.emit_nom / (lhc_beam.gamma * lhc_beam.beta)

    variables = [
        "name",
        "keyword",
        "tilt",
        "s",
        "x",
        "y",
        "betx",
        "bety",
        "dx",
        "dy",
        "l",
    ]
    name, keyword, tilt, s, x, y, betx, bety, dx, dy, l = get_twiss_var(
        twiss_file, "", variables
    )

    name_counter = collections.Counter(keyword)
    n_coll = (
        name_counter['"RCOLLIMATOR"']
        + name_counter['"COLLIMATOR"']
        + name_counter['"ECOLLIMATOR"']
    )

    if not isinstance(material, list):
        mat_old = material
        material = []
        for i in range(n_coll):
            material.append(mat_old)

    if not isinstance(aper_list, list):
        aper_nominal = aper_list
        aper_list = []
        for i in range(n_coll):
            aper_list.append(aper_nominal)

    sigma_x = np.sqrt(emit * np.array(betx))
    sigma_y = np.sqrt(emit * np.array(bety))

    # list of errors assigned to collimator. It is returned for statistic
    error_list = []

    coll_num = 0
    print(
        "name   aper_pos    aper_neg   aper[sigma]   sigma[m]   (x, y)[m]   anlge[deg]"
    )
    for i in range(len(name)):
        if "COLLIMATOR" in keyword[i]:

            aper_current = aper_list[coll_num]
            mat_current = material[coll_num]
            angle_current = floor(degrees(tilt[i]))
            length_current = l[i]

            errors_up, errors_do = errors[0], errors[1]

            if angle_current == 90.0:
                coll_type = "v"
                aper_pos = y[i - 1] + aper_current * sigma_y[i - 1]
                aper_neg = y[i - 1] - aper_current * sigma_y[i - 1]

                if len(errors[0].split()) == 2:
                    if errors[0].split()[0] == "True":
                        temp_value = (
                            float(errors[0].split()[1]) * sigma_y[i - 1]
                        )
                        errors_up = "True " + str(temp_value)
                    if errors[1].split()[0] == "True":
                        temp_value = (
                            float(errors[1].split()[1]) * sigma_y[i - 1]
                        )
                        errors_do = "True " + str(temp_value)
                else:
                    if errors[0].split()[0] == "True":
                        temp_value = get_rand_num_trunc(
                            std=float(errors[0].split()[1]), limit=1.4
                        ), get_rand_num_trunc(
                            std=float(errors[0].split()[2]), limit=1.4
                        )

                        error_list.append(temp_value[0])
                        error_list.append(temp_value[1])

                        errors_up = (
                            "True "
                            + str(temp_value[0] * sigma_y[i - 1])
                            + " "
                            + str(temp_value[1] * sigma_y[i - 1])
                        )
                    if errors[1].split()[0] == "True":

                        temp_value = get_rand_num_trunc(
                            std=float(errors[1].split()[1]), limit=1.4
                        ), get_rand_num_trunc(
                            std=float(errors[1].split()[2]), limit=1.4
                        )

                        error_list.append(temp_value[0])
                        error_list.append(temp_value[1])

                        errors_do = (
                            "True "
                            + str(temp_value[0] * sigma_y[i - 1])
                            + " "
                            + str(temp_value[1] * sigma_y[i - 1])
                        )

                print(
                    name[i],
                    "  ",
                    aper_pos,
                    "   ",
                    aper_neg,
                    "   ",
                    aper_current,
                    "   ",
                    sigma_y[i - 1],
                    "   ",
                    y[i - 1],
                    "   ",
                    angle_current,
                )
            elif angle_current == 0.0:
                coll_type = "h"
                aper_pos = x[i - 1] + aper_current * sigma_x[i - 1]
                aper_neg = x[i - 1] - aper_current * sigma_x[i - 1]

                if len(errors[0].split()) == 2:
                    if errors[0].split()[0] == "True":
                        temp_value = (
                            float(errors[0].split()[1]) * sigma_x[i - 1]
                        )
                        errors_up = "True " + str(temp_value)
                    if errors[1].split()[0] == "True":
                        temp_value = (
                            float(errors[0].split()[1]) * sigma_x[i - 1]
                        )
                        errors_do = "True " + str(temp_value)
                else:
                    if errors[0].split()[0] == "True":

                        temp_value = get_rand_num_trunc(
                            std=float(errors[0].split()[1]), limit=1.4
                        ), get_rand_num_trunc(
                            std=float(errors[0].split()[2]), limit=1.4
                        )

                        error_list.append(temp_value[0])
                        error_list.append(temp_value[1])

                        errors_up = (
                            "True "
                            + str(temp_value[0] * sigma_x[i - 1])
                            + " "
                            + str(temp_value[1] * sigma_x[i - 1])
                        )

                    if errors[1].split()[0] == "True":

                        temp_value = get_rand_num_trunc(
                            std=float(errors[1].split()[1]), limit=1.4
                        ), get_rand_num_trunc(
                            std=float(errors[1].split()[2]), limit=1.4
                        )

                        error_list.append(temp_value[0])
                        error_list.append(temp_value[1])

                        errors_do = (
                            "True "
                            + str(temp_value[0] * sigma_x[i - 1])
                            + " "
                            + str(temp_value[1] * sigma_x[i - 1])
                        )

                print(
                    name[i],
                    "  ",
                    aper_pos,
                    "   ",
                    aper_neg,
                    "   ",
                    aper_current,
                    "   ",
                    sigma_x[i - 1],
                    "   ",
                    x[i - 1],
                    "   ",
                    angle_current,
                )
            else:
                coll_type = "h"
                sigma_skew = sqrt(
                    sigma_x[i - 1] ** 2 * cos(radians(angle_current)) ** 2
                    + sigma_y[i - 1] ** 2 * sin(radians(angle_current)) ** 2
                )
                aper_pos = aper_current * sigma_skew
                aper_neg = -1 * aper_current * sigma_skew

                if len(errors[0].split()) == 2:
                    if errors[0].split()[0] == "True":
                        temp_value = (
                            float(errors[0].split()[1]) * sigma_skew[i - 1]
                        )
                        errors_up = "True " + str(temp_value)
                    if errors[1].split()[0] == "True":
                        temp_value = (
                            float(errors[0].split()[1]) * sigma_skew[i - 1]
                        )
                        errors_do = "True " + str(temp_value)
                else:
                    if errors[0].split()[0] == "True":

                        temp_value = get_rand_num_trunc(
                            std=float(errors[0].split()[1]), limit=1.4
                        ), get_rand_num_trunc(
                            std=float(errors[0].split()[2]), limit=1.4
                        )

                        error_list.append(temp_value[0])
                        error_list.append(temp_value[1])

                        errors_up = (
                            "True "
                            + str(temp_value[0] * sigma_skew[i - 1])
                            + " "
                            + str(temp_value[1] * sigma_skew[i - 1])
                        )
                    if errors[1].split()[0] == "True":

                        temp_value = get_rand_num_trunc(
                            std=float(errors[1].split()[1]), limit=1.4
                        ), get_rand_num_trunc(
                            std=float(errors[1].split()[2]), limit=1.4
                        )

                        error_list.append(temp_value[0])
                        error_list.append(temp_value[1])

                        errors_do = (
                            "True "
                            + str(temp_value[0] * sigma_skew[i - 1])
                            + " "
                            + str(temp_value[1] * sigma_skew[i - 1])
                        )

                print(
                    name[i],
                    "  ",
                    aper_pos,
                    "   ",
                    aper_neg,
                    "   ",
                    aper_current,
                    "   ",
                    sigma_skew,
                    "   ",
                    "   ",
                    angle_current,
                )

            coll_DB.add_row(
                [
                    name[i],
                    aper_pos,
                    aper_neg,
                    aper_pos,
                    aper_neg,
                    length_current,
                    mat_current,
                    errors_up,
                    errors_do,
                ]
            )

            coll_num += 1

    return error_list


def add_coll_to_dbmadx(
    coll_DB,
    name,
    aper_up_pos,
    aper_up_neg,
    aper_do_pos,
    aper_do_neg,
    length,
    material,
    error_up="False 0.0",
    error_down="False 0.0",
):
    """
    Add a collimator to database with input parameter freely.
    @param coll_DB: Prettytable object of collimator database
    @param name:
    @param aper_up_pos:
    @param aper_up_neg:
    @param aper_do_pos:
    @param aper_do_neg:
    @param length:
    @param material:
    @param error_up:
    @param error_down:
    @return:
    """
    coll_DB.add_row(
        [
            '"' + name.upper() + '"',
            aper_up_pos,
            aper_up_neg,
            aper_do_pos,
            aper_do_neg,
            length,
            material,
            error_up,
            error_down,
        ]
    )


def make_coll_for_madx(file_db, coll_name):
    """
    Given a collimator database file and a collimator name as input, this function
    creates a collimator object with the parameters indicated in the database
    @param file_db: collimator database file
    @param coll_name: MADX name of the collimator as passed by the dialogue file
    @return: Collimator object
    """
    with open(file_db, "r") as ff:
        for line in ff:
            if coll_name.lower() in line.split()[0].lower():
                coll_descr = line.split()
                break
    name = coll_descr[0].strip('"')

    aper_up_p = float(coll_descr[1])
    aper_up_n = float(coll_descr[2])

    aper_down_p = float(coll_descr[3])
    aper_down_n = float(coll_descr[4])

    length = float(coll_descr[5])
    material = coll_descr[6]

    if len(coll_descr) == 11:
        error_up_on = coll_descr[7]
        if error_up_on == "True":
            std_up = float(coll_descr[8])
            err_pos_up = float(trandn(-2.4, 2.4, scale=std_up).rvs(1))
            err_neg_up = float(trandn(-2.4, 2.4, scale=std_up).rvs(1))
        else:
            err_pos_up = 0.0
            err_neg_up = 0.0

        error_do_on = coll_descr[9]
        if error_do_on == "True":
            std_do = float(coll_descr[10])
            err_pos_do = float(trandn(-2.4, 2.4, scale=std_do).rvs(1))
            err_neg_do = float(trandn(-2.4, 2.4, scale=std_do).rvs(1))
        else:
            err_pos_do = 0.0
            err_neg_do = 0.0

        apery = 0.04
        aperx = abs(aper_up_p - aper_down_p) / 2.0

        unc_p_u = (aper_up_p - aperx) + err_pos_up
        unc_n_u = -1 * (aperx + aper_up_n) + err_neg_up

        unc_p_d = (aper_down_p - aperx) + err_pos_do
        unc_n_d = -1 * (aperx + aper_down_n) + err_neg_do

    elif len(coll_descr) == 13:
        error_up_on = coll_descr[7]
        if error_up_on == "True":
            err_pos_up = float(coll_descr[8])
            err_neg_up = float(coll_descr[9])
        else:
            err_pos_up = 0.0
            err_neg_up = 0.0

        error_do_on = coll_descr[10]
        if error_do_on == "True":
            err_pos_do = float(coll_descr[11])
            err_neg_do = float(coll_descr[12])
        else:
            err_pos_do = 0.0
            err_neg_do = 0.0

        apery = 0.04
        aperx = abs(aper_up_p - aper_down_p) / 2.0

        unc_p_u = (aper_up_p - aperx) + err_pos_up
        unc_n_u = -1 * (aperx + aper_up_n) + err_neg_up

        unc_p_d = (aper_down_p - aperx) + err_pos_do
        unc_n_d = -1 * (aperx + aper_down_n) + err_neg_do

    new_coll = Collimator(
        material,
        0,
        length,
        "h",
        aperx,
        apery,
        unc_p_u,
        unc_p_d,
        unc_n_u,
        unc_n_d,
        name,
        0.0,
    )

    return new_coll


def make_septum_for_madx(sept_db, sept_name):
    """
    Creates a collimator object with septum properties
    @param coll_name: MADX name of the collimator as passed by the dialogue file
    @return: Collimator object
    """
    with open(sept_db, "r") as ff:
        for line in ff:
            if sept_name.lower() in line.split()[0].lower():
                sept_desc = line.split()
                break

    (
        name,
        aper_u,
        aper_d,
        thickness,
        length,
        material,
        eff_dens,
        isErrorUp,
        std_up,
        isErrorDown,
        std_do,
        kick,
        side,
    ) = sept_desc

    name = name.strip('"')

    if isErrorUp == "True":
        unc_u = float(trandn(-2.4, 2.4, scale=float(std_up)).rvs(1))
    else:
        unc_u = 0.0

    if isErrorDown == "True":
        unc_d = float(trandn(-2.4, 2.4, scale=float(std_do)).rvs(1))
    else:
        unc_d = 0.0

    new_septum = Septum(
        material,
        0,
        float(length),
        float(aper_u),
        float(aper_d),
        float(thickness),
        float(unc_u),
        float(unc_d),
        name,
        side,
        0.0,
        float(kick),
        float(eff_dens),
    )

    return new_septum


def get_twiss_dic(file_name):

    """
    Needs a twiss file as input and gives 2 dictionaries back.

    @param file_name: twiss file name
    @return: dict of twiss columns (data) and dict of twiss summary table (beam_info)
    """

    beam_info = {}

    with open(file_name, "r") as ff:
        count = 0
        for i in range(1, 100):
            line = ff.readline().split()
            if line[0] == "@":
                count += 1
                try:
                    beam_info[line[1]] = float(line[3])
                except:
                    beam_info[line[1]] = line[3]
            elif line[0] == "*":
                names = line[1:]

    data = np.genfromtxt(
        file_name, skip_header=count + 2, names=names, dtype=None
    )

    return data, beam_info


def get_gauss_distribution(
    input="sequence_totrack.tfs", sigmas=3, beam_t="LHC", n_part=100, seed=123
):
    twiss_file = input

    twiss, _ = get_twiss_dic(twiss_file)

    x0 = twiss["X"][0]
    y0 = twiss["Y"][0]
    px0 = twiss["PX"][0]
    py0 = twiss["PY"][0]

    betx = twiss["BETX"][0]
    bety = twiss["BETY"][0]

    alfx = twiss["ALFX"][0]
    alfy = twiss["ALFY"][0]

    dx = twiss["DX"][0]
    dpx = twiss["DPX"][0]
    dy = twiss["DY"][0]
    dpy = twiss["DPY"][0]

    # ========================================
    # Seed
    # ========================================

    np.random.seed(seed)

    beam_type = beam_t

    if beam_type == "LHC":
        beam = dict(
            type="LHC",
            energy=450,
            emit_nom=3.5e-6,
            emit_n=3.5e-6,
            intensity=288 * 1.15e11,
            dpp_t=3e-4,
            n_particles=10000,
            n_sigma=5,
        )
    elif beam_type == "LIU":
        beam = dict(
            type="LIU",
            energy=450,
            emit_nom=3.5e-6,
            emit_n=1.37e-6,
            intensity=288 * 2.0e11,
            dpp_t=3e-4,
            n_particles=1000,
            n_sigma=5,
        )
    elif beam_type == "LHC-meas":
        beam = dict(
            type="LHC-meas",
            energy=450,
            emit_nom=3.5e-6,
            emit_n=2.0e-6,
            intensity=288 * 1.2e11,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=6.8,
        )
    elif beam_type == "HL-LHC":
        beam = dict(
            type="HL-LHC",
            energy=450,
            emit_nom=3.5e-6,
            emit_n=2.08e-6,
            intensity=288 * 2.32e11,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=5,
        )
    elif beam_type == "coast":
        beam = dict(
            type="coast",
            energy=270,
            emit_nom=3.5e-6,
            emit_n=3.5e-6,
            intensity=1e12,
            dpp_t=3e-4,
            n_particles=1e6,
            n_sigma=5,
        )

    n_sigma = sigmas

    emit_n = beam["emit_n"]  # [mm.mrad]
    dpp_t = beam["dpp_t"]

    n = n_part

    m_p = 0.938

    # ========================================
    # Beam parameters
    # ========================================

    p = beam["energy"]  # momentum in GeV

    gamma = np.sqrt(m_p ** 2 + p ** 2) / m_p
    beta0 = np.sqrt(gamma ** 2 - 1) / gamma
    beta_r = np.sqrt(1.0 - 1.0 / gamma ** 2)
    emit_x = emit_n / (beta_r * gamma)
    emit_y = emit_n / (beta_r * gamma)

    e0 = np.sqrt(p ** 2 + m_p ** 2)

    # ========================================
    # Delta p / p
    # ========================================

    ddp = (np.random.rand(n) * 2.0 * dpp_t) - dpp_t

    de = ddp * e0 * beta0 ** 2
    pt = de / p

    # ==============================================
    # Bivariate normal distributions
    # ==============================================

    sx = np.sqrt(emit_x * betx)
    n_x = trandn(-1 * n_sigma, n_sigma, scale=sx).rvs(n)
    x = x0 + n_x + (dx * ddp)
    px = (
        px0
        + (trandn(-1 * n_sigma, n_sigma, scale=sx).rvs(n) - alfx * n_x) / betx
        + (dpx * ddp)
    )

    sy = np.sqrt(emit_y * bety)
    n_y = trandn(-1 * n_sigma, n_sigma, scale=sy).rvs(n)
    y = y0 + n_y + (dy * ddp)
    py = (
        py0
        + (trandn(-1 * n_sigma, n_sigma, scale=sy).rvs(n) - alfy * n_y) / bety
        + (dpy * ddp)
    )

    t = np.zeros(n)

    return x, px, y, py, t, pt


def read_madx_dialogue_file(file_name, p0):
    """
    Read file produce from MADX and use as input for tracking inside matter
    @param file_name: name of the file => outputf.dat
    @param p0: momentum of the synchronous particle
    @return: name of the collimator, its angle (tilt) and a particle universe
    """
    with open(file_name, "r") as fr:
        name = fr.readline().split()[0].strip('"')
        angle = float(fr.readline().split()[0])

        x = map(float, fr.readline().split())
        x_p = map(float, fr.readline().split())
        y = map(float, fr.readline().split())
        y_p = map(float, fr.readline().split())
        t = map(float, fr.readline().split())
        pt = map(float, fr.readline().split())
        id_part = map(int, fr.readline().split())

    particle_universe = []
    for x0, xp0, y0, yp0, pt0, t0, id_n in zip(x, x_p, y, y_p, pt, t, id_part):
        pp = Proton(0, x0, xp0, y0, yp0, pt0, p0, t0, "outside", id_n)
        particle_universe.append(pp)

    return name, angle, particle_universe


def get_list_from_pu(particle_universe, var):
    """
    Return a list for a given variable "var" taken from the particle that compose the particle universe.
    @param particle_universe: list of Proton objects
    @param var: variable of interest as string
    @return: list of var for the particles composing the particle universe
    """
    vec = []
    for pp in particle_universe:
        vec.append(getattr(pp, var))
    return vec


def make_particle_universe_from_vector(x, x_p, y, y_p, t, pt, p0):
    particle_universe = []
    count = 0
    for x0, xp0, y0, yp0, pt0, t0 in zip(x, x_p, y, y_p, pt, t):
        pp = Proton(0, x0, xp0, y0, yp0, pt0, 400, t0, "outside", count)
        particle_universe.append(pp)
        count += 1

    return particle_universe


def transport(particle_universe, matrix):

    for pp in particle_universe:
        z_vec = np.array((pp.x, pp.x_p, pp.y, pp.y_p, pp.t, pp.pt))
        pp.x, pp.x_p, pp.y, pp.y_p, pp.t, pp.pt = np.dot(matrix, z_vec)


def write_madx_dialogue_file(file_name, particle_universe):
    """
    Write file to send the particle distribution back to MADX.
    @param file_name: name of the file => outputp.dat
    @param particle_universe: list of Proton objects
    """
    with open(file_name, "w") as fw:
        x = get_list_from_pu(particle_universe, "x")
        x_p = get_list_from_pu(particle_universe, "x_p")
        y = get_list_from_pu(particle_universe, "y")
        y_p = get_list_from_pu(particle_universe, "y_p")
        t = get_list_from_pu(particle_universe, "t")
        pt = get_list_from_pu(particle_universe, "pt")
        id_part = get_list_from_pu(particle_universe, "id")

        fw.write(str(len(particle_universe)) + "\n")
        fw.write("    ".join(["%10.16f" % ele for ele in x]))
        fw.write("\n")
        fw.write("    ".join(["%10.16f" % ele for ele in x_p]))
        fw.write("\n")
        fw.write("    ".join(["%10.16f" % ele for ele in y]))
        fw.write("\n")
        fw.write("    ".join(["%10.16f" % ele for ele in y_p]))
        fw.write("\n")
        fw.write("    ".join(["%10.16f" % ele for ele in t]))
        fw.write("\n")
        fw.write("    ".join(["%10.16f" % ele for ele in pt]))
        fw.write("\n")
        fw.write("    ".join(["%10d" % ele for ele in id_part]))


def write_absfile_madx(file_name, particle_universe):
    """
    Write file to be read by MADX with information about absorbed particles.
    @param file_name: name of the file => outabsf.dat
    @param particle_universe: list of Proton objects
    """
    s_abs = [str(ppp.s) for ppp in particle_universe if ppp.flag == "absorbed"]
    id_abs = [
        str(ppp.id) for ppp in particle_universe if ppp.flag == "absorbed"
    ]

    with open(file_name, "w") as fw:
        fw.write(str(len(s_abs)) + "\n")

        fw.write("    ".join(id_abs))
        fw.write("\n")
        fw.write("    ".join(s_abs))


def write_cryst_interaction_madx(file_name, interacted):
    """
    Write file to record the particles that have interacted with the crystal.
    @param file_name: name of the file => cryst_inter.tfs
    @param interacted: list of particles ID that have interacted with the crystal
    """

    interacted = [str(ele) for ele in interacted]

    with open(file_name, "a") as fw:

        fw.write("    ".join(interacted))
        fw.write("\n")


def read_crystal_from_db(file_name, name_cryst):
    """
    Read a crystal db file (columns = name, x_min, x_max, kick
    @param file_name: name of the file
    @param name_cryst: name of the crystal as used in madx
    @return:
    """
    crys_db = np.genfromtxt(file_name, names=True, dtype=None)
    crys_db = pd.DataFrame(crys_db)
    crys_db["Name"] = [ele.decode("utf-8") for ele in crys_db["Name"]]

    for i in range(len(crys_db["Name"])):
        crys_db["Name"][i] = crys_db["Name"][i].lower()
    try:
        ind_cry = list(crys_db["Name"]).index(name_cryst.lower())
    except ValueError:
        print("++++Warning, no crystal found")
        return 0.0, 0.0, 0.0, 0.0, 0.0
    else:

        return (
            crys_db["x_min"][ind_cry],
            crys_db["x_max"][ind_cry],
            crys_db["kick"][ind_cry],
            crys_db["kickSD"][ind_cry],
            crys_db["angle_c"][ind_cry],
        )


def make_crystal_db(file_name, name, x_min, x_max, kick, kick_std):

    crys_DB = PrettyTable(["# Name", "x_min", "x_max", "kick", "kickSD"])
    crys_DB.align = "r"
    crys_DB.border = False

    if type(name) == list:
        for i in range(len(name)):
            crys_DB.add_row([name[i], x_min[i], x_max[i], kick[i], kick_std[i]])
    else:
        crys_DB.add_row([name, x_min, x_max, kick, kick_std])

    with open(file_name, "w") as fp:
        fp.write(crys_DB.get_string())


def make_loss_map(losses_file, twiss_file, particles_tracked, color="red"):
    """
    Plots the number of particles lost at each element normalised by the tracked particles.
    @param losses_file: file of 'trackloss' table from MADX thin tracking
    @param twiss_file: twiss file of the sequence tracked
    @param particles_tracked: number of initial particles ( / 100. in case want the plot in percent)
    @param color: color of the histogram line
    @return: the s location of all particles lost
    """

    s = np.genfromtxt(
        losses_file, usecols=8, unpack=True, skip_header=8, invalid_raise=False
    )

    twiss, _ = get_twiss_dic(twiss_file)

    hist_cool(
        s, bins=twiss["S"], density=False, norm=particles_tracked, color=color
    )

    return s


def process_command_line_old(argv):
    """
    Process command line arguments
    """

    help_text = "Usage: python pycollimate.py 1 ti2_track_out_1.tfsone initial_distribution\n"

    if argv is None:

        argv = sys.argv[1:]

    # initialize the parser object:
    parser = optparse.OptionParser(
        formatter=optparse.TitledHelpFormatter(width=78), add_help_option=None
    )

    # define options here:
    parser.add_option(  # customized description; put --help last
        "-h", "--help", action="help", help=help_text
    )

    parser.set_defaults(
        coll_index=1,
        file_input="twiss_input.tfs",
        file_out="initial_distribution",
        black=False,
    )

    parser.add_option("-o", "--output", dest="file_out")

    parser.add_option("-i", "--input", dest="file_input")

    parser.add_option(
        "-b",
        "--black",
        action="store_true",
        dest="black",
        help="Black absorber switch",
    )

    parser.add_option(
        "-n",
        "--number",
        type="int",
        dest="coll_index",
        help="Collimator index and seed",
    )

    options, args = parser.parse_args(argv)

    return options, args


def main_old(argv):

    options, args = process_command_line_old(argv)

    ii = options.coll_index
    file_input = options.file_input
    file_out = options.file_out + "_" + str(ii + 1) + ".txt"

    seed = ii

    # np.random.seed(seed)

    files = os.listdir("")
    coll_db_file = [a for a in files if "coll_DB_pyCollimate" in a]

    coll, s0 = read_coll_lhc(coll_db_file[0], ii)

    x, x_p, y, y_p, t, pt, s = read_ptc_track_out(file_input)

    if coll.angle != 0 and coll.type == "h":
        x, x_p, y, y_p = rotate(x, x_p, y, y_p, coll)

    particle_universe = []
    count = 0
    for x0, xp0, y0, yp0, pt0, t0, s0_ptc in zip(x, x_p, y, y_p, pt, t, s):
        pp = Proton(0, x0, xp0, y0, yp0, pt0, 450, t0, "outside", count)
        particle_universe.append(pp)
        count += 1

    for pp in particle_universe:
        get_impact_point_angle(coll, pp)

    if options.black:
        black_absorber(particle_universe)
    else:
        do_scattering_routine(coll, particle_universe)

    if coll.angle != 0 and coll.type == "h":
        write_files_rot(
            particle_universe,
            file_out,
            "absorbed_" + str(ii) + ".txt",
            s0,
            coll,
        )
    else:
        write_files(
            particle_universe,
            file_out,
            "absorbed_" + str(ii) + ".txt",
            s0,
            coll,
        )
