"""
© Copyright F.M. Velotti, Y. Dutheil 2017-2020. All rights not expressly granted are reserved’
"""
from __future__ import division, print_function

from pycollimate.pycollimate import (sys, optparse, os, read_madx_dialogue_file,
                         make_septum_for_madx, get_impact_pos,
                         do_scattering_routine, black_absorber,
                         do_scattering_routine_septum, make_coll_for_madx,
                         write_madx_dialogue_file,
                         write_absfile_madx, get_impact_point_angle,
                         read_crystal_from_db, write_cryst_interaction_madx, 
                         np)
import pycollimate

from .track_inside_coll import (rvs_double, rvs_double_one, crystal_pdf, crystal_thin_kick,
                               crystal_rand_kick, crystal_real_kick, crystal_real_kick_2d,
                               process_command_line)

import socket
import time
import numpy as np
import struct
from .recv_size import recv_size



def track_inside(name, angle, ktrack, energy, 
                 track, part_id,
                 argv, db_files
):
    
    options, args = process_command_line(argv)
    coll_db_file, septa_db_file, crystal_db_file = db_files

    particle_universe = []
    for ipart, part in enumerate(track):
        pp = pycollimate.Proton(0, part[0], part[1], part[2], part[3],
                                part[5], energy, part[4], 'outside', part_id[ipart])
        particle_universe.append(pp)



    if name[0].upper() in ['M', 'Z', 'D']:

        septa = []
        with open(septa_db_file[0], 'r') as fc:
            next(fc)
            for line in fc:
                septa.append(line.split()[0].strip('"'))

        if name.upper() in septa:

            try:
                septum = make_septum_for_madx(septa_db_file[0], name)
            except IndexError:
                raise IndexError("Forgot somehting? No collimator database found!")

            t1_impact = time.time()

            for pp in particle_universe:
                pp.track_drift(-1 * septum.length/2.)
                pp.s = 0.0
                get_impact_pos(septum, pp)

            t2_impact = time.time()
            print('impact = ', t2_impact - t1_impact)

            t1_track = time.time()

            if options.black:
                black_absorber(particle_universe)
            else:
                do_scattering_routine_septum(septum, particle_universe)

            t2_track = time.time()
            print('track = ', t2_track - t1_track)

            for pp in particle_universe:
                if not pp.flag == 'absorbed':
                    pp.track_drift(-1 * septum.length/2.)
                else:
                    pp.s -= septum.length/2.

    elif 'CRYS' in name.upper():

        x_min, x_max, kick, kick_sd, angle_c = read_crystal_from_db(crystal_db_file[0], name)

        if kick != 0:

            interacted, kicked = crystal_real_kick_2d(x_min, x_max, angle_c, kick, particle_universe)
            if len(interacted) > 0:
                write_cryst_interaction_madx('cryst_inter.tfs', interacted)
                write_cryst_interaction_madx('cryst_kick.tfs', kicked)

        write_madx_dialogue_file('outputp.dat', particle_universe)
        write_absfile_madx('outabsf.dat', particle_universe)

    else:

        collimators = []
        with open(coll_db_file[0], 'r') as fc:
            next(fc)
            for line in fc:
                collimators.append(line.split()[0].strip('"'))

        if name.upper() in collimators:

            try:
                coll = make_coll_for_madx(coll_db_file[0], name)
            except IndexError:
                raise IndexError("Forgot somehting? No collimator database found!")

            for pp in particle_universe:
                pp.track_drift(-1 * coll.length/2.)
                pp.s = 0.0
                get_impact_point_angle(coll, pp)

            if options.black:
                black_absorber(particle_universe)
            else:
                do_scattering_routine(coll, particle_universe)

            for pp in particle_universe:
                if not pp.flag == 'absorbed':
                    pp.track_drift(-1 * coll.length/2.)
                else:
                    pp.s -= coll.length/2.

        
    track_out = []
    for pp in particle_universe:
        track_out += [pp.x, pp.x_p, pp.y, pp.y_p, pp.t, pp.pt]
   
    id_lost = [ppp.id for ppp in particle_universe if ppp.flag == 'absorbed']
    s_lost = [ppp.s for ppp in particle_universe if ppp.flag == 'absorbed']

    n_lost = len(id_lost)


    if(os.path.isfile('./interactions_elems')):
        finter = open('interactions_elems', 'a+')
        for pp in particle_universe:
            if(pp.pt != pp.X0[5]):
                # print('particle interacted', pp.id, coll.name, (pp.pt-pp.X0[5])*pp.p0, pp.p0)
                finter.write('{:5d} {:10s} {:15.5e}  {:10s} '.format(pp.id, pp.flag, (pp.pt-pp.X0[5])*pp.p0, coll.name)
                             + '{:15.5e} {:15.5e} {:15.5e} {:15.5e} {:15.5e} {:15.5e}'.format(*pp.X0)
                             + '{:15.5e} {:15.5e} {:15.5e} {:15.5e} {:15.5e} {:15.5e}'.format(pp.s, pp.x, pp.x_p, pp.y, pp.y_p, pp.pt)
                             + '\n')
        finter.close()
    
   
    return n_lost, id_lost, s_lost, track_out
    

    
def main(argv):
    
    files = os.listdir('.')
    coll_db_file = [a for a in files if 'coll_DB' in a or 'colldb' in a]
    septa_db_file = [a for a in files if 'septa_db' in a or 'septa_DB' in a]
    crystal_db_file = [a for a in files if 'crystal_db' in a or 'crystal_DB' in a]
    db_files = [coll_db_file, septa_db_file, crystal_db_file]

    # using ip address
    # HOST = ''                 # Symbolic name meaning all available interfaces
    # PORT = 50007              # Arbitrary non-privileged port
    # s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # s.bind((HOST, PORT))

    # using socket file descriptor
    s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    s.bind("./socket.s")

    s.listen(1)

    while 1:
        conn, addr = s.accept()
        t_start = time.time()

        el_name = recv_size(conn, 48).strip()

        theta = recv_size(conn, 8)
        theta_raw = theta
        theta = struct.unpack('d', theta)[0]
        
        ktrack = recv_size(conn, 4)
        ktrack_raw = ktrack
        ktrack = struct.unpack('i', ktrack)[0]

        energy = recv_size(conn, 8)
        energy_raw = energy
        energy = struct.unpack('d', energy)[0]
        
        track = recv_size(conn, 8*6*ktrack)
        track = struct.unpack('d'*(ktrack*6), track)

        
        part_id = recv_size(conn, 4*ktrack)
        part_id = struct.unpack('i'*ktrack, part_id)

        t_received = time.time()
        n_lost, id_lost, s_lost, track_out = track_inside(el_name, theta, ktrack, energy, 
                                                          np.array(track).reshape(ktrack,6), part_id,
                                                          argv, db_files)
        t_treated = time.time()
        conn.send(struct.pack('i', n_lost))

        if(n_lost>0):
            # id_lost = np.random.randint(-100, 100, n_lost)
            conn.send(struct.pack('i'*n_lost, *id_lost))

            # s_lost = np.linspace(0,1,n_lost)
            conn.send(struct.pack('d'*n_lost, *s_lost))


        # track_out = np.linspace(0,1, (ktrack)*6)
        conn.send(struct.pack('d'*(ktrack)*6, *track_out))

        t_sent = time.time()
        
        print('done with pycollimate, in {:5.3f} ({:3.1f} {:3.1f} {:3.1f}) at {:s} and lost {:5d}/{:5d}'.format(
            t_sent-t_start, t_received-t_start, t_treated-t_received, t_sent-t_treated,
            el_name, n_lost, ktrack))
        # time.sleep(3)
        # conn.send("MrC all done in python")


if __name__ == '__main__':
    main(sys.argv)
