"""
© Copyright F.M. Velotti 2017-2020. All rights not expressly granted are reserved’
"""
from __future__ import division

import time
from pycollimate.pycollimate import (sys, optparse, os, read_madx_dialogue_file,
                                     make_septum_for_madx, get_impact_pos,
                                     do_scattering_routine, black_absorber,
                                     do_scattering_routine_septum, make_coll_for_madx,
                                     write_madx_dialogue_file,
                                     write_absfile_madx, get_impact_point_angle,
                                     read_crystal_from_db, write_cryst_interaction_madx,
                                     np)

from pycollimate import pycollimate
from scipy.stats import norm, expon
import pickle

pyc_path = os.path.dirname(pycollimate.__file__)

t1_imp = time.time()


def rvs_double(n, mu1, mu2, s1, s2, c2):

    tot_len = int(n * (1 - c2)) + int(n * c2)

    if not tot_len == n:
        add_one = 1
    else:
        add_one = 0

    res1 = norm(loc=mu1, scale=s1).rvs(int(n * (1 - c2)))

    res2 = norm(loc=mu2, scale=s2).rvs(int(n * c2) + add_one)

    return np.r_[res1, res2]


def rvs_double_one(mu1, mu2, s1, s2, c2):

    choice = np.random.choice([1, 2], p=[1 - c2, c2])

    if choice == 1:
        return norm(loc=mu1, scale=s1).rvs(1)

    else:
        return norm(loc=mu2, scale=s2).rvs(1)


def crystal_pdf(n=1):

    n_tot = {1: 0, 2: 0, 3: 0}

    if n == 1:

        choice = np.random.choice([1, 2, 3], p=[0.4, 0.06, 0.54])
        n_tot[choice] = 1

        if n_tot[1] == 1:
            x1 = rvs_double_one(-14, 0.1, 8.03, 7.3, 0.5)
        else:
            x1 = []

    else:
        n_tot[1] = int(n * 0.4)
        n_tot[2] = int(n * 0.06)
        n_tot[3] = int(n * 0.54)

        if n_tot[1] + n_tot[2] + n_tot[3] < n:
            n_tot[3] += 1
        elif n_tot[3] + n_tot[2] + n_tot[3] > n:
            n_tot[3] -= 1

        x1 = rvs_double(int(n_tot[1]), -14, 0.1, 8.03, 7.3, 0.5)

    s, l = 88.3, 3.0044

    x2 = []
    while(len(x2) != n_tot[2]):
        temp = float(expon.rvs(size=1, loc=l, scale=s))
        if temp < 130:
            x2.append(temp)

    x3 = norm.rvs(size=n_tot[3], loc=143.8, scale=8.2)

    return np.r_[x1, x2, x3]


def crystal_thin_kick(x_min, x_max, kick, particle_universe):

    kicked = []
    for pp in particle_universe:

        if x_min < pp.x < x_max:
            pp.x_p += kick
            kicked.append(pp.id)

    return kicked


def crystal_rand_kick(x_min, x_max, kick, kick_std, particle_universe):

    kicked = []
    for pp in particle_universe:
        if x_min <= pp.x <= x_max:
            actual_kick = np.random.randn() * kick_std + kick
            pp.x_p += actual_kick
            kicked.append(pp.id)

    return kicked


def crystal_real_kick(x_min, x_max, kick, kick_std, particle_universe):

    kicked = []
    kick_given = []
    # the kick from crystal is now computed from measured
    # pdf from UA9 - the one from the DB is ignored!
    for pp in particle_universe:
        if x_min <= pp.x <= x_max:
            actual_kick = crystal_pdf() * 1e-6 * (kick / np.abs(kick))
            pp.x_p += actual_kick
            pp.update_energy(-2e-3)
            kicked.append(pp.id)
            kick_given.append(actual_kick)

    return kicked, kick_given


def crystal_real_kick_2d(x_min, x_max, angle_c, kick, particle_universe):
    """
    Load 2D pdf from mesured data in H8 from UA9 collaboration
    """
    p = pickle.load(open(pyc_path + '/cry_2d_pdf.p', 'r'))
    kicked = []
    kick_given = []
    # the kick from crystal is now computed from measured
    # pdf from UA9 - the one from the DB is ignored!
    for pp in particle_universe:
        if x_min <= pp.x <= x_max:
            try:
                actual_kick = p.sampleX(n_sample=1, Yini=pp.x_p * 1e6 - angle_c * 1e6) * 1e-6 * np.sign(kick)
            except ValueError:
                inp_angle = pp.x_p * 1e6 - angle_c * 1e6
                if 29 < inp_angle < 189:
                    actual_kick = p.sampleX(n_sample=1, Yini=29) * 1e-6 * np.sign(kick)
                else:
                    actual_kick = p.sampleX(n_sample=1, Yini=-29) * 1e-6 * np.sign(kick)
            pp.x_p += actual_kick
            pp.update_energy(-2e-3)
            kicked.append(pp.id)
            kick_given.append(actual_kick)

    return kicked, kick_given


def process_command_line(argv):
    """
    Process command line arguments
    """

    help_text = 'Script to use MADX tracking for active elements and the pycollimate module' \
                'for tracking inside matter.\n'

    if argv is None:

        argv = sys.argv[1:]

    # initialize the parser object:
    parser = optparse.OptionParser(
        formatter=optparse.TitledHelpFormatter(width=78),
        add_help_option=None)

    # define options here:
    parser.add_option(      # customized description; put --help last
                            '-h', '--help', action='help',
                            help=help_text)

    parser.set_defaults(black=False)

    parser.add_option('-b', '--black', action='store_true', dest='black', help='Black absorber switch')

    options, args = parser.parse_args(argv)

    return options, args


def main(argv):

    options, args = process_command_line(argv)

    # seed = 100
    np.random.seed(int(t1_imp))

    files = os.listdir('..')
    coll_db_file = [a for a in files if 'coll_DB' in a or 'colldb' in a]
    septa_db_file = [a for a in files if 'septa_db' in a or 'septa_DB' in a]
    crystal_db_file = [a for a in files if 'crystal_db' in a or 'crystal_DB' in a]

    name, angle, particle_universe = read_madx_dialogue_file('outputf.dat', 400.)

    t2_imp = time.time()
    print('import = ', t2_imp - t1_imp)

    if name[0].upper() in ['M', 'Z', 'D']:

        septa = []
        with open(septa_db_file[0], 'r') as fc:
            next(fc)
            for line in fc:
                septa.append(line.split()[0].strip('"'))

        if name.upper() in septa:

            try:
                septum = make_septum_for_madx(septa_db_file[0], name)
            except IndexError:
                raise IndexError("Forgot somehting? No collimator database found!")

            t1_impact = time.time()

            for pp in particle_universe:
                pp.track_drift(-1 * septum.length/2.)
                pp.s = 0.0
                get_impact_pos(septum, pp)

            t2_impact = time.time()
            print('impact = ', t2_impact - t1_impact)

            t1_track = time.time()

            if options.black:
                black_absorber(particle_universe)
            else:
                do_scattering_routine_septum(septum, particle_universe)

            t2_track = time.time()
            print('track = ', t2_track - t1_track)

            for pp in particle_universe:
                if not pp.flag == 'absorbed':
                    pp.track_drift(-1 * septum.length/2.)
                else:
                    pp.s -= septum.length/2.

    elif 'CRYS' in name.upper():

        x_min, x_max, kick, kick_sd, angle_c = read_crystal_from_db(crystal_db_file[0], name)

        if kick != 0:

            interacted, kicked = crystal_real_kick_2d(x_min, x_max, angle_c, kick, particle_universe)
            if len(interacted) > 0:
                write_cryst_interaction_madx('cryst_inter.tfs', interacted)
                write_cryst_interaction_madx('cryst_kick.tfs', kicked)

        write_madx_dialogue_file('outputp.dat', particle_universe)
        write_absfile_madx('outabsf.dat', particle_universe)

    else:

        collimators = []
        with open(coll_db_file[0], 'r') as fc:
            next(fc)
            for line in fc:
                collimators.append(line.split()[0].strip('"'))

        if name.upper() in collimators:

            try:
                coll = make_coll_for_madx(coll_db_file[0], name)
            except IndexError:
                raise IndexError("Forgot somehting? No collimator database found!")

            for pp in particle_universe:
                pp.track_drift(-1 * coll.length/2.)
                pp.s = 0.0
                get_impact_point_angle(coll, pp)

            if options.black:
                black_absorber(particle_universe)
            else:
                do_scattering_routine(coll, particle_universe)

            for pp in particle_universe:
                if not pp.flag == 'absorbed':
                    pp.track_drift(-1 * coll.length/2.)
                else:
                    pp.s -= coll.length/2.

    write_madx_dialogue_file('outputp.dat', particle_universe)
    write_absfile_madx('outabsf.dat', particle_universe)


if __name__ == '__main__':
    main(sys.argv)
