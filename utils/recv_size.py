"""
© Copyright F.M. Velotti 2017-2020. All rights not expressly granted are reserved’
"""
# http://code.activestate.com/recipes/408859-socketrecv-three-ways-to-turn-it-into-recvall/    
def recv_size(the_socket, msg_len):
    total_len=0
    msg=''
    while total_len<msg_len:
        data = the_socket.recv(
            min( 2^15, msg_len-total_len))
        # print '**** received ', len(data)
        msg += data
        total_len=len(msg)
    return msg
    

    
    
    

