"""
© Copyright F.M. Velotti 2017-2020. All rights not expressly granted are reserved’
"""
import argparse
from pycollimate.pycollimate import *
import acc_library as al

parser = argparse.ArgumentParser(description='Process some integers.')

parser.add_argument('beam', type=str, choices=['b1', 'b2'], default='b1',
                    help='Beam under analysis')

parser.add_argument('-a', '--aperture_inj', type=float, nargs='*', default=[6.8, 6.8, 6.8],
                    help='Aperture of tdi, tclia and tclib in sigma')

args = parser.parse_args()
beam = args.beam

tdi_aper = args.aperture_inj[0]
tclia_aper = args.aperture_inj[1]
tclib_aper = args.aperture_inj[2]


path = '/afs/cern.ch/work/f/fvelotti/private/lhc/'

twiss_nominal = {'b1': 'lhc_track_pycollimate_b1/madx/twiss_lhc_2015_b1_nominal.tfs',
                 'b2': 'lhc_track_pycollimate_b2/madx/twiss_lhc_2015_b2_nominal.tfs'}

colldb_6t = {'b1': 'lhc_track_pycollimate_b1/CollDB_V6.503_lowb_st.b1.data',
             'b2': 'lhc_track_pycollimate_b2/CollDB_V6.503_lowb_st.b2.data'}

settings = {'b1': 'lhc_track_pycollimate_b1/start_ramp_B1_mm_kept_meas_beam_size_inj_FT_2015--2015_5_18_21_59_46.680732.csv',
            'b2': 'lhc_track_pycollimate_b2/start_ramp_B2_mm_kept_meas_beam_size_inj_FT_2015--2015_5_18_21_59_46.680732.csv'}

coll_twiss = {'b1': 'lhc_track_pycollimate_b1/madx/twiss_lhc_b1_forcoll.tfs',
              'b2': 'lhc_track_pycollimate_b2/madx/twiss_lhc_b2_forcoll.tfs'}

tdis_twiss = {'b1': 'lhc_track_pycollimate_b1/madx/twiss_lhc_b1_tdi.tfs',
              'b2': 'lhc_track_pycollimate_b2/madx/twiss_lhc_b2_tdi.tfs'}

tdi_names = {'b1': '.4L2.B1"',
             'b2': '.4R8.B2"'}

colldb_name = {'b1': 'colldbmadx_lhc.tfs',
               'b2': 'colldbmadx_lhc_b2.tfs'}

names_new, aper_n_new, mat_new, length_new, angle_new = \
    read_collDB_inj(path + twiss_nominal[beam], path + colldb_6t[beam], path + settings[beam])

for i, name in enumerate(names_new):
    if "TCLIB" in name:
        mat_new[i] = 'Graphite'
        aper_n_new[i] = tclib_aper
    if "TCLIA" in name:
        mat_new[i] = 'R4550'
        aper_n_new[i] = tclia_aper
for i, name in enumerate(names_new):
    if "TDI" in name:
        del names_new[i]
        del aper_n_new[i]
        del mat_new[i]
        del length_new[i]
        del angle_new[i]


with open('check_coll_aper_' + beam + '.txt', 'w') as fw:
    for i in range(len(names_new)):
        fw.write(names_new[i] + '   ' + str(aper_n_new[i]) + '   ' + str(length_new[i]) +  str(angle_new[i]) + '\n')

coll_table = make_coll_dbmadx_table()
add_coll_from_list(twiss_file=path + coll_twiss[beam], coll_DB=coll_table,
                    beam_type='LHC', material=mat_new, aper_list=aper_n_new, name_coll=names_new)

variables = ['name', 'y', 'bety']
names_twiss, y, bety = al.get_twiss_numpy(tdis_twiss[beam], path, variables)
lhc = al.Beam('LHC')
emit = lhc.emit_nom / (lhc.beta * lhc.gamma)
sigma = np.sqrt(emit * np.array(bety))
y = np.array(y)


for i in range(1, 4):
    tdi_name = '"TDI.' + str(i) + tdi_names[beam]
    index_tdi = names_twiss.index(tdi_name) - 1
    material = ['R4550', 'Aluminum', 'Copper']

    if i == 2:
        add_coll_to_dbmadx(coll_table, name=tdi_name.strip('"'),
                           aper_up_pos=y[index_tdi] + tdi_aper * sigma[index_tdi] + 2e-3,
                           aper_up_neg=y[index_tdi] - tdi_aper * sigma[index_tdi] - 2e-3,
                           aper_do_pos=y[index_tdi + 1] + tdi_aper * sigma[index_tdi] + 2e-3,
                           aper_do_neg=y[index_tdi + 1] - tdi_aper * sigma[index_tdi] - 2e-3,
                           length=0.6, material=material[i - 1])
    elif i == 3:

        add_coll_to_dbmadx(coll_table, name=tdi_name.strip('"'),
                           aper_up_pos=y[index_tdi] + tdi_aper * sigma[index_tdi] + 2e-3,
                           aper_up_neg=y[index_tdi] - tdi_aper * sigma[index_tdi] - 2e-3,
                           aper_do_pos=y[index_tdi + 1] + tdi_aper * sigma[index_tdi] + 2e-3,
                           aper_do_neg=y[index_tdi + 1] - tdi_aper * sigma[index_tdi] - 2e-3,
                           length=0.7, material=material[i - 1])

    elif i == 1:
        add_coll_to_dbmadx(coll_table, name=tdi_name.strip('"'),
                           aper_up_pos=y[index_tdi] + tdi_aper * sigma[index_tdi],
                           aper_up_neg=y[index_tdi] - tdi_aper * sigma[index_tdi],
                           aper_do_pos=y[index_tdi + 1] + tdi_aper * sigma[index_tdi + 1],
                           aper_do_neg=y[index_tdi + 1] - tdi_aper * sigma[index_tdi + 1],
                           length=2.85, material=material[i - 1])

write_coll_dbmadx_table_to_file(colldb_name[beam], coll_table)